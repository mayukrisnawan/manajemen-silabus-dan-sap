<?php
require_once 'vendors/PHP Router/PHPRouter.php';
require_once 'vendors/PHP Router/PHPRoute.php';

class Router {
  public static $_router = null; // PHPRouter instance
  public static $_route = null; // PHPRoute instance
  
  public static function set($from, $to, array $args=array()) {
    /* DEBUG
    echo "$from $to<br/>";
    var_dump($args);
    echo "<br/>";
    echo "<br/>";*/
    if (Router::$_router === null) {
      Router::$_router = new PHPRouter();
      Router::$_router->setBasePath(getBasePath());
    }
    if (!isset($args["name"])) $args["name"] = $to;
    Router::$_router->map($from, $to, $args);
  }

  public static function accept($controllerName, array $options=array()) {
    $className = ucfirst($controllerName) . "Controller";
    $except = isset($options["except"]) ? $options["except"] : array();
    $filters = isset($options["filters"]) ? $options["filters"] : array();
    foreach (get_class_methods($className) as $method) {
      if ($method[0] == "_" || in_array($method, $except)) continue;
      $refMethod = new ReflectionMethod($className, $method);
      $params = $refMethod->getParameters();
      $paramsCount = count($params);
      $optionalCount = $paramsCount - $refMethod->getNumberOfRequiredParameters();

      $filter = isset($filters["*"]) ? $filters["*"] : array();
      $filter = isset($filters[$method]) ? $filters[$method] : $filter;
      for ($i=0; $i<=$optionalCount; $i++) {
        $url = "";
        for ($j=0;$j<$paramsCount-$i;$j++) {
          $url .= ":" . $params[$j]->name . "/";
        }
        Router::set("/$controllerName/$method/$url", "$controllerName#$method",
          array("filters"=>$filter)
        );
      }
    }
  }

  public static function rest($name, array $options=array(), array $childs=array(), $parent=null) {
    if ($parent !== null) $parent = ActiveRecord\Utils::singularize($parent);
    $preUrl = $parent === null ? "" : "/$parent/:".$parent."_id";
    $preName =  $parent === null ? "" : $parent . "_";
    $className = ucfirst($name) . "Controller";
    $except = isset($options["except"]) ? $options["except"] : array();
    foreach (get_class_methods($className) as $method) {
      if ($method[0] == "_" || in_array($method, $except)) continue;
      switch ($method) {
        case 'index':
          Router::set("$preUrl/$name", "$name#index", array("name"=>$preName.$name."#index", "methods"=>"GET"));
          break;
        case 'show':
          Router::set("$preUrl/$name/:id", "$name#show", array("name"=>$preName.$name."#show", "methods"=>"GET",
            "filters"=>array("id"=>'(\d+)')
          ));
          break;
        case 'add':
          Router::set("$preUrl/$name/add", "$name#add", array("name"=>$preName.$name."#add", "methods"=>"GET"));
          break;
        case 'edit':
          Router::set("$preUrl/$name/:id/edit", "$name#edit", array("name"=>$preName.$name."#edit", "methods"=>"GET",
            "filters"=>array("id"=>'(\d+)')
          ));
          break;
        case 'create':
          Router::set("$preUrl/$name", "$name#create", array("name"=>$preName.$name."#create", "methods"=>"POST"));
          break;
        case 'update':
          Router::set("/$name/:id", "$name#update", array("name"=>$name."#update", "methods"=>"PATCH", 
            "filters"=>array("id"=>'(\d+)')
          ));
          break;
        case 'destroy':
          Router::set("/$name/:id", "$name#destroy", array("name"=>$name."#destroy", "methods"=>"DELETE",
            "filters"=>array("id"=>'(\d+)')
          ));
          break;
      }
    }
    foreach ($childs as $key=>$value) {
      if (is_array($value)) {
        Router::rest($key, $options, $value, $name);
      } else {
        Router::rest($value, $options, array(), $name);
      }
    }
  }

  public static function run() {
    Router::$_route = Router::$_router->matchCurrentRequest();
  }

  public static function target() {
    return Router::$_route ? Router::$_route->getTarget() : false;
  }

  public static function params() {
    $params = Router::$_route->getParameters();
    $params = array_merge($params, Router::httpParams());
    return Router::$_route ? $params : false;
  }

  public static function httpParams() {
    $params = array();
    static $buffer = false;
    switch ($_SERVER["REQUEST_METHOD"]) {
      case "GET":
        $buffer = $_GET;
        break;
      case "POST":
        $buffer = $_POST;
        break;
      default:
        if ($buffer === false) parse_str(file_get_contents("php://input"), $buffer);
        break;
    }
    foreach ($buffer as $key => $value) {
      $params[$key] = $value;
    }
    return $params;
  }

  public static function create($name, array $params = array()) {
    return Router::$_router->generate($name, $params);
  }

  public static function generate($routeName, array $params = array()) {
    return Router::$_router->generate($routeName, $params);
  }
}