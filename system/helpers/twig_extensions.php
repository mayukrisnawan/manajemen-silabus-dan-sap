<?php

return function($engine){
  // url function
  $u = new Twig_SimpleFunction('u', function($name, array $params=array()){
    return u($name, $params);
  });

  // anchor function
  $a = new Twig_SimpleFunction('a', function($label, $routeName, array $routeParams=array(), array $anchorAttributes=array()){
    try {
      $url = u($routeName, $routeParams);
    } catch (Exception $e) {
      $url = $routeName;
    }
    $attr = "";
    foreach ($anchorAttributes as $key => $value) {
      $attr .= " " . $key . "='" . $value . "'";
    }
    return "<a href='$url'".$attr.">$label</a>";
  }, array(
    'is_safe'=>array('html')
  ));

  // img function
  $img = new Twig_SimpleFunction('img', function($name, array $attributes=array()){
    $url = url("public/img/$name");
    $attr = "";
    foreach ($attributes as $key => $value) {
      $attr .= " " . $key . "='" . $value . "'";
    }
    return "<img src='$url'".$attr."/>";
  }, array(
    'is_safe'=>array('html')
  ));

  // var_dump function
  $var_dump = new Twig_SimpleFunction('var_dump', function($value){
    return var_dump($value);
  }, array(
    'is_safe'=>array('html')
  ));

  // md5 function
  $md5 = new Twig_SimpleFunction('md5', function($input){
    return md5($input);
  });

  // User Management
  $current_user_level = new Twig_SimpleFunction('current_user_level', function(){
    $level_id = UM::currentLevel();
    return UM::levelName($level_id);
  });

  $has_level = new Twig_SimpleFunction('has_level', function($level){
    $level_id = UM::currentLevel();
    return UM::levelName($level_id) == $level;
  });

  // extending the engine
  $engine->addFunction($u);
  $engine->addFunction($a);
  $engine->addFunction($img);
  $engine->addFunction($var_dump);
  $engine->addFunction($md5);
  $engine->addFunction($current_user_level);
  $engine->addFunction($has_level);
}

?>