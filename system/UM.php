<?php

session_start();
class UM {
  protected static $userIdKey = "_user_id";
  protected static $userLevelKey = "_user_level";
  protected static $requiredLevels = array("guest");
  protected static $modelName = "User";
  public static function login($username, $password) {
    if ($username == "" || $password == "") return false;
    $password = md5($password);
    $user = User::find_by_username_and_password($username, $password);
    if ($user) {
      $_SESSION[UM::$userIdKey] = $user->id;
      $_SESSION[UM::$userLevelKey] = $user->level;
      return true;
    }
    return false;
  }

  public static function isLogin() {
    return isset($_SESSION[UM::$userIdKey]);    
  }

  public static function logout() {
    if (isset($_SESSION[UM::$userIdKey])) {
      unset($_SESSION[UM::$userIdKey]);
    }
  }

  public static function info($key=null) {
    if (!UM::isLogin()) return false;
    $user = User::find($_SESSION[UM::$userIdKey]);
    if ($key == null) return $user->toArray();
    return $user->$key;
  }

  public static function levelId($userLevelName) {
    $config = App::config("user_level");
    foreach ($config as $id => $name) {
      if ($userLevelName == $name) return $id;
    }
    return false;
  }

  public static function levelName($levelId) {
    $config = App::config("user_level");
    return isset($config[$levelId]) ? $config[$levelId] : false;
  }

  public static function currentLevel() {
    if (!UM::isLogin()) return UM::levelId("guest");
    return $_SESSION[UM::$userLevelKey];
  }

  public static function currentUser() {
    return User::find($_SESSION[UM::$userIdKey]);
  }

  public static function requiredLevel($userLevel=null) {
    return UM::requiredLevels($userLevel);
  }   

  public static function requiredLevels($userLevels=null) {
    if ($userLevels === null) return UM::$requiredLevels;
    if (!is_array($userLevels)) $userLevels = array($userLevels);
    $levelList = array();
    foreach ($userLevels as $userLevel) {
      if (UM::levelId($userLevel) !== false) $levelList[] = $userLevel;
    }
    UM::$requiredLevels = $levelList;
  }

  public static function levelCheck() {
    $currentLevel = UM::levelName(UM::currentLevel());
    $requiredLevels = UM::requiredLevels();
    $access_granted = in_array($currentLevel, $requiredLevels);
    if (!$access_granted) {
      $callback = App::config("user_level_callback");
      header("Location:". u($callback));
      exit();
    }
  }
}