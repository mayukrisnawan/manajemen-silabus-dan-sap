<?php

class TwitterBootstrap {
  private static $cachePath = "system/tmp/bootstrap.css";
  private static $definitionPath = "public/css/bootstrap.css.less";
  private static $targets = array("public/css/bootstrap.css.less", "public/css/bootstrap.css");

  public static function initLessCompiler() {
    require_once "system/vendors/less.php-master/lib/Less/Visitor/visitor.php";
    require_once "system/vendors/less.php-master/lib/Less/Visitor/extend-visitor.php";
    require_once "system/vendors/less.php-master/lib/Less/Visitor/import-visitor.php";
    require_once "system/vendors/less.php-master/lib/Less/Visitor/join-selector-visitor.php";
    require_once "system/vendors/less.php-master/lib/Less/Visitor/process-extends-visitor.php";

    function lessAutoload($name) {
      $paths = array(
        "system/vendors/less.php-master/lib/Less/".$name.".php",
        "system/vendors/less.php-master/lib/Less/".str_replace("Less_", "", $name).".php",
        "system/vendors/less.php-master/lib/Less/Exception/".str_replace("Less_", "", $name).".php",
        "system/vendors/less.php-master/lib/Less/Node/".str_replace("Less_", "", $name).".php",
        "system/vendors/less.php-master/lib/Less/Node/".str_replace("Less_Tree_", "", $name).".php",
        "system/vendors/less.php-master/lib/Less/Node/Mixin/".str_replace("Less_Tree_Mixin", "", $name).".php",
        "system/vendors/less.php-master/lib/Less/Visitor/".str_replace("Less_", "", $name).".php"
      );
      foreach ($paths as $path) {
        if (file_exists($path)) {
          require_once($path);
        }
      }
    }
    spl_autoload_register("lessAutoload");
  }

  public static function checkedCompile($path) {
    if (TwitterBootstrap::requestedBy($path)) {
      if (!file_exists(TwitterBootstrap::$cachePath) || 
          (
            filemtime(TwitterBootstrap::currentTarget()) 
            > filemtime(TwitterBootstrap::$cachePath)
          ) 
         ) 
      {
        TwitterBootstrap::compile();
      }
    }
  }

  public static function catchJsBy($path) {
    
  }

  public static function currentTarget() {
    foreach (TwitterBootstrap::$targets as $target) {
      if (file_exists($target)) return $target;
    }
    return false;
  }

  public static function requestedBy($path) {
    return in_array($path, TwitterBootstrap::$targets);
  }

  public static function compile() {
    $parser = new Less_Parser();
    $parser->parseFile(TwitterBootstrap::currentTarget());
    $contents = $parser->getCss();
    file_put_contents(TwitterBootstrap::$cachePath, $contents, LOCK_EX);
  }

  public static function fetch() {
    return file_get_contents(TwitterBootstrap::$cachePath);
  }
}