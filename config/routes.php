<?php

Router::set("/", "guest#index");
Router::accept("guest",array(
  "except"=>array("tampilkan_berita")
));
Router::set("/berita/:id", "guest#tampilkan_berita", array(
  "methods"=>"GET", 
  "name" => "tampilkan_berita",
  "filters" => array("id" => "(\d+)")
));

Router::accept("guest");
Router::accept("dosen");

Router::set("/admin", "admin#index");
Router::accept("admin");

Router::rest("curriculums", array(), array("programs"));
Router::set("/curriculum/:curriculum_id/program/options/", "programs#options", array(
  "methods"=>"GET", 
  "name" => "program_options",
  "filters" => array("id" => "(\d+)")
));
Router::set("/curriculums/:curriculum_id/program/:id/search", "programs#search_matkul", array(
  "methods"=>"GET", 
  "name" => "programs#search_matkul",
  "filters" => array("curriculum_id" => "(\d+)", "id" => "(\d+)")
));

Router::rest("news");
Router::rest("resources");
Router::rest("stages");
Router::rest("references");
Router::rest("users");

Router::set("/users/:id/reset/password", "users#reset_password", array(
  "methods"=>"PATCH", 
  "name" => "reset_password",
  "filters" => array("id" => "(\d+)")
));

Router::rest("lecturers");
Router::set("/lecturers/json", "lecturers#json");
Router::set("/lecturers/search", "lecturers#search");

Router::set("/references/json", "references#json");
Router::set("/references/search", "references#search");

Router::rest("subjects", array(), array("meetings"));
Router::set("/subjects/search", "subjects#search");
Router::set("/subjects/:id/silabus", "subjects#silabus", array(
  "methods"=>"GET", 
  "name" => "download_silabus",
  "filters" => array("id" => "(\d+)")
));
Router::set("/subjects/:id/sap", "subjects#sap", array(
  "methods"=>"GET", 
  "name" => "download_sap",
  "filters" => array("id" => "(\d+)")
));

/*
Router::set("/subjects/:id/silabus/html", "subjects#silabus_html", array(
  "methods"=>"GET", 
  "name" => "silabus_html",
  "filters" => array("id" => "(\d+)")
));*/

Router::rest("meetings", array(), array("meetingstages"));
Router::rest("meetingreferences");
Router::rest("meetingstageresources");
Router::rest("achievementindicators");
Router::rest("standardcompetencies");
Router::rest("configs");

// Tabel Pertemuan
Router::set("/meeting/:id/references/table", "meetings#references_table", array(
  "methods"=>"GET", 
  "name" => "meeting_references_table",
  "filters" => array("id" => "(\d+)")
));

// Tabel Indikator Pencapaian
Router::set("/meeting/:id/achievement/indicators/table", "meetings#achievement_indicators_table", array(
  "methods"=>"GET", 
  "name" => "achievement_indicators_table",
  "filters" => array("id" => "(\d+)")
));

// Tabel Standar Kompetensi
Router::set("/subject/:id/standard/competency/table", "subjects#standard_competencies_table", array(
  "methods"=>"GET",
  "name" => "standard_competencies_table",
  "filters" => array("id" => "(\d+)")
));

// Tabel Alat dan Bahan
Router::set("/meeting/stage/:id/resources/table", "meetingstages#resources_table", array(
  "methods"=>"GET", 
  "name" => "meeting_stage_resources_table",
  "filters" => array("id" => "(\d+)")
));

// Mata Kuliah Umum #index
Router::set("/curriculum/:curriculum_id/matkul/umum", "subjects#matkul_umum", array(
  "methods"=>"GET", 
  "name" => "matkul_umum",
  "filters" => array("curriculum_id" => "(\d+)")
));

// Mata Kuliah Umum #add
Router::set("/curriculum/:curriculum_id/matkul/umum/add", "subjects#matkul_umum_add", array(
  "methods"=>"GET", 
  "name" => "matkul_umum_add",
  "filters" => array("curriculum_id" => "(\d+)")
));

// Mata Kuliah Umum #edit
Router::set("/curriculum/:curriculum_id/matkul/umum/:id/edit", "subjects#matkul_umum_edit", array(
  "methods"=>"GET", 
  "name" => "matkul_umum_edit",
  "filters" => array("curriculum_id" => "(\d+)", "id"=> "(\d+)")
));

// Mata Kuliah #add
Router::set("/program/:program_id/matkul/add", "subjects#matkul_add", array(
  "methods"=>"GET", 
  "name" => "matkul_add",
  "filters" => array("program_id" => "(\d+)")
));

// Mata Kuliah #edit
Router::set("/program/:program_id/matkul/:id/edit", "subjects#matkul_edit", array(
  "methods"=>"GET", 
  "name" => "matkul_edit",
  "filters" => array("program_id" => "(\d+)", "id"=>"(\d+)")
));

// Urutan Pertemuan Mata Kuliah
Router::set("/subject/:subject_id/meeting/set/orders", "meetings#set_orders", array(
  "methods"=>"PATCH", 
  "name" => "set_subject_meeting_orders",
  "filters" => array("subject_id" => "(\d+)")
));

// Urutan Tahapan Pembelajaran
Router::set("/meeting/:meeting_id/stage/set/orders", "meetingstages#set_orders", array(
  "methods"=>"PATCH", 
  "name" => "set_subject_meeting_stage_orders",
  "filters" => array("meeting_id" => "(\d+)")
));

// Halaman Bantuan
Router::set("/configs/bantuan", "configs#bantuan", array(
  "methods"=>"GET", 
  "name" => "configs#bantuan",
  "filters" => array("id" => "(\d+)")
));

// Halaman Informasi
Router::set("/configs/informasi", "configs#informasi", array(
  "methods"=>"GET", 
  "name" => "configs#informasi",
  "filters" => array("id" => "(\d+)")
));