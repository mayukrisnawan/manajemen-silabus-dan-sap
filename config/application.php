<?php

return array(
  'current_mode' => 'development',
  'connections' => array(
    'development' => array(
      "host"=>"localhost",
      "username"=>"root",
      "password"=>"",
      "database"=>"silabus"
    ),
    'production' => array(
      "host"=>"localhost",
      "username"=>"root",
      "password"=>"",
      "database"=>"silabus"
    )  
  ),
  'user_level' => array(
    0 =>'admin',
    1 =>'dosen',
    2 => 'guest'
  ),
  'default_user_level' => 'admin',
  'user_level_callback' => 'guest#login'
);