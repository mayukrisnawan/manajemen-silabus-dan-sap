<?php

return array(
    'gif' => 'image/gif',
    'png' => 'image/png',
    'jpg' => 'image/jpg',
    'css' => 'text/css',
    'less' => 'text/css',
    'js' => 'text/javascript',
    'html' => 'text/html',
    'txt' => 'text/plain'
);