function loadDotsAnimationInto(element, msg) {
  var useVal = false;
  if (!msg) {
    if ($(element).html()) {
      msg = $(element).html();
    } else {
      useVal = true;
      msg = $(element).val();
    }
  }
  var context = this;
  context.dots = 1;
  if (useVal) {
    $(element).val(msg);
  } else {
    $(element).html(msg);
  }

  timer = setInterval(function(){
    var dots = "";
    for (i=0; i<context.dots; i++) dots += ".";
    if (useVal) {
      $(element).val(msg+dots);
    } else {
      $(element).html(msg+dots);
    }
    context.dots += 1;
    context.dots %= 3;
  }, 700);
  return timer;
}

function stopDotsAnimationOn(timer, element, msg) {
  if (!timer) return false;
  clearTimeout(timer);
  if (msg) element.innerHTML = msg;
}


function ajaxSubmit(form, btnSubmit) {
  var data = "";
  var $form = $(form);
  var $btnSubmit = $(btnSubmit);
  $form.find("*[name]").each(function(key, element){
    var $element = $(element);
    if (!data == "") {
      data += "&";
    }
    data += $element.attr("name") + "=" + $element.val();
  });

  var target = $form.attr("action");
  if (!target) {
    target = $btnSubmit.attr("href");
    if (!target) return false;
  }
  var method = $form.attr("method");
  if (!method) {
    method = $btnSubmit.attr("method");
    if (!method) method = "GET";
  }

  var actionBackup = null;
  var timer = null;
  var contentBackup = null;
  var useVal = false;
  if ($btnSubmit.html()) {
    contentBackup = $btnSubmit.html();
    useVal = true;
  } else {
    contentBackup = $btnSubmit.val();
  }
  $.ajax({
    url:target,
    data:data,
    type:method,
    success:function(response){
      stopDotsAnimationOn(timer, btnSubmit, contentBackup);
      $btnSubmit.removeAttr("disabled");
      if (useVal) {
        $btnSubmit.html(contentBackup);
      } else {
        $btnSubmit.val(contentBackup);
      }
      $btnSubmit.get(0).onclick = actionBackup;
      eval(response);
    },
    beforeSend:function(){
      $btnSubmit.attr("disabled", "disabled");
      timer = loadDotsAnimationInto($btnSubmit);
      actionBackup = $btnSubmit.get(0).onclick;
    }
  });
  return false;
}

function alert(title, msg, callback) {
  if (title !== undefined && msg === undefined) {
    msg = title;
    title = $("title").html();
  }
  var modalId = '__alert';
  var modal = ["<div class='modal' id='"+modalId+"'>",
        "<div class='modal-dialog'>",
          "<div class='modal-content'>",
            "<div class='modal-header'>",
              "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>",
              "<h4 class='modal-title'><b>"+title+"</b></h4>",
            "</div>",
            "<div class='modal-body'>",
              "<p>"+msg+"</p>",
            "</div>",
            "<div class='modal-footer'>",
              "<button type='button' class='btn btn-success' data-dismiss='modal'>Kembali</button>",
            "</div>",
          "</div>",
        "</div>",
      "</div>"].join("");
  if ($("#"+modalId).length != 0) {
    $("#"+modalId).remove();
  }
  $("body").append(modal);
  $("#"+modalId).modal("show").on("hidden.bs.modal", function(){
    if (callback) callback();
  });
}

function confirm(title, msg, callback) {
  if (title !== undefined && msg === undefined) {
    msg = title;
    title = $("title").html();
  }
  var modalId = '__confirm';
  var modal = ["<div class='modal' id='"+modalId+"'>",
        "<div class='modal-dialog'>",
          "<div class='modal-content'>",
            "<div class='modal-header'>",
              "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>",
              "<h4 class='modal-title'><b>"+title+"</b></h4>",
            "</div>",
            "<div class='modal-body'>",
              "<p>"+msg+"</p>",
            "</div>",
            "<div class='modal-footer'>",
              "<button type='button' class='btn btn-success _btn_yes'>Ya</button>",
              "<button type='button' class='btn btn-danger _btn_no'>Tidak</button>",
            "</div>",
          "</div>",
        "</div>",
      "</div>"].join("");
  if ($("#"+modalId).length != 0) {
    $("#"+modalId).remove();
  }
  $("body").append(modal);
  $("#"+modalId).modal("show").on("hidden.bs.modal", function(){
    if (callback) callback();
  }).find("._btn_yes").click(function(){
    confirm.result = true;
    $("#"+modalId).modal("hide");
  });
   $("#"+modalId).find("._btn_no").click(function(){
    confirm.result = false;
    $("#"+modalId).modal("hide");
  });
}
confirm.result = false;


// create a list of <option> html tags
function createOption(data, contentKey, valueKey) {
  if (!valueKey) valueKey = "id";
  var result = "";
  for (var index in data) {
    result += "<option value='"+data[index][valueKey]+"'>"+data[index][contentKey]+"</option>";
  }
  return result;
}

// Add jQuery <option> generator
$.fn.selectify = function(options){
  var child = options["child"];
  if (!child) return false;
  var empty_content = options["empty"].content;
  var empty_value = options["empty"].value;
  var initial_content = options["initial"].content;
  var initial_value = options["initial"].value;
  var loading = options["loading"];
  var data_processor = options["data_processor"] ? options["data_processor"] : function(data){ return data; };
  var url_processor = options["url_processor"] ? options["url_processor"] : function(url, value){ return url; };

  var $child = $(child);
  this.change(function(){
    var value = $(this).val();
    if (value === "") {
      $child.find("option").remove();
      if (intial) {
        $child.append("<option value='" + initial_value + "'>" + initial_content + "</option>");     
      }
      return false;
    }
    $child.attr("disabled", "disabled");
    if (loading) {
      $child.find("option").remove();
      $child.append("<option value='" + initial_value + "'>" + loading + "</option>");     
    }

    var url = options["url"];
    if (!url) return false;
    url = url_processor(url, value);
    $.getJSON(url, function(options){
      options = data_processor(options);
      $child.find("option").remove();
      if (options.data.length == 0) {
        $child.append("<option value='"+ empty_value +"'>" + empty_content + "</option>")
        return false;
      }
      $child = $(child).removeAttr("disabled");
      options = createOption(options.data, options.content_key, options.value_key);
      if (options["intial"]) {
        $child.append("<option value='"+ initial_value +"'>" + initial_content + "</option>");     
      }
      $child.removeAttr("disabled").append(options);
    });
  });
  return this;
};

$(document).ready(function(){
  // Remote links
  $("body").on("click", "[remote='true']", function(){
    var btnSubmit = this;
    if ($(this).attr("confirmTitle") && $(this).attr("confirmMessage")) {
      var title = $(this).attr("confirmTitle");
      var message = $(this).attr("confirmMessage");
      confirm(title, message, function(){
        if (confirm.result) {
          ajaxSubmit(null, btnSubmit);
        }
      });
      return false;
    } else {
      ajaxSubmit(null, btnSubmit);
      return false;
    }
  });

  // App menu
  (function(){
    $(document).click(function(){
        $(".app_menu_content_container").hide();
    });
    function resizeContainer(){
      $(".app_menu_content_container").each(function(index){
        $(this).mousemove(function(e){
          if ($(window).width() > 900) {
            /*
            var top = (index+1) * 46 + 5;
            if (index == 0) top += 5;
            var bottom = top + $(this).width() + 13;*/
            var childCount = $(this).find("li").length;
            var top = 50 + (index+1) * (30+index*2);
            var bottom = top + childCount * 36;
            console.log(bottom + " " + e.clientY);
            if (e.clientX < 20 || e.clientX > $(this).width()+37
            ||  e.clientY < top || e.clientY > bottom) {
              $(this).hide();
              var id = $(this).attr("id");
            }
          }
        });

        if ($(window).width() < 900) {
          var leftOffset = (index*44);
          var height = $(this).find("li").length * 41;
          $(this).css({
            "top":40,
            "left":leftOffset,
            "display":"none",
            "height":height
          });
        } else {
          var topOffset = (index*40)+51;
          var height = $(this).find("li").length * 41;
          $(this).css({
            "left":40,
            "top":topOffset,
            "display":"none",
            "height":height
          });
        }
        $(".app_menu_icon").unbind("click").unbind("mouseover");
        $(".app_menu").unbind("mouseout");
        if ($(window).width() > 900) {
          $(".app_menu_icon").mouseover(function(){
            var target = $(this).attr("href");
            $(".app_menu_content_container").hide();
            $(".app_menu_icon").removeClass("active");
            $(this).addClass("active");
            if ($(this).hasClass("app_menu_link")) return true;
            $(target).show(100);
          }).click(function(){
            if ($(this).hasClass("app_menu_link")) return true;
            return false;
          });
        } else {
          $(".app_menu_icon").click(function(){
            if ($(this).hasClass("app_menu_link")) return true;
            var target = $(this).attr("href");
            $(".app_menu_content_container").hide();
            $(".app_menu_icon").removeClass("active");
            $(this).addClass("active");
            $(target).toggle(500);
            return false;
          }).mouseover(function(){
            $(".app_menu_content_container").hide();
            $(".app_menu_icon").removeClass("active");
            $(this).addClass("active");
            return false;
          });
          $(".app_menu").mouseout(function(){
            $(".app_menu_icon").removeClass("active");
          });
        }
      });
    }

    $(window).resize(function(){
      resizeContainer();
    });
    resizeContainer();    
  })();

  // App checkbox
  $(document).ready(function(){
    $("body").on("click", ".app-checkbox", function(){
      console.log(this);
      if (this.checked) {
        this.value = 1;
      } else {
        this.value = 0;
      }
    });
  });
});

function jqueryUI_sortableHelper(e, ui) {
  ui.children().each(function() {
    $(this).width($(this).width());
  });
  return ui;
}