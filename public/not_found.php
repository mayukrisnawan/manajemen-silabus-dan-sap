<html>
<head>
  <title>Page not found</title>
  <?php
    css(array(
      'font-awesome/font-awesome.min',
      'bootstrap',
      'app'
    ));
  ?>
<style type="text/css">
body {
  text-align: center;
  background-color : #efefef;
}
#emot {
  margin-top: 100px;
  font-size: 150px;
}
#page_title {
  font-size: 60px;
}
</style>
</head>
<body>
  <div id="emot">:(</div>
  <div id="page_title"><b>404</b>. Halaman tidak ditemukan</div>
  <div>
    Sistem Informasi Manajemen SAP dan Silabus<br/>
    <a href="<?php echo u('guest#index');  ?>">
      <i class="glyphicon glyphicon-home"></i>
      &nbsp;Kembali ke halaman utama
    </a>
  </div>
</body>
</html>