<?php

class User extends Model {
  static $has_one = array(
    array("lecturer")
  );
  static $validates_presence_of = array(
    array("username", "message"=>"tidak boleh kosong"),
    array("password", "message"=>"tidak boleh kosong")
  );
  static $validates_length_of = array(
    array("username", "minimum"=>5, "too_short"=>"minimal 5 karakter"),
    array("password", "minimum"=>5, "too_short"=>"minimal 5 karakter"),
    array("username", "maximum"=>100, "too_long"=>"tidak boleh lebih dari 100 karakter"),
    array("password", "maximum"=>100, "too_long"=>"tidak boleh lebih dari 100 karakter")
  );
  static $validates_uniqueness_of = array(
    array("username", "message"=>"username sudah digunakan, gunakan yang lain")
  );
}