<?php

class News extends Model {
  static $validates_presence_of = array(
    array("title", "message"=>"judul tidak boleh kosong")   
  );
  static $validates_length_of = array(
    array("title", "maximum"=>255, "too_long"=>"terlalu panjang, panjang maksimal adalah 255 karakter"),
    array("title", "minimum"=>5, "too_short"=>"minimal terdiri dari 5 karakter")
  );
  static $validates_uniqueness_of = array(
    array("title", "message"=>"judul berita telah digunakan, gunakan judul yang lain")
  );

  public function after_validation() {
    $this->last_updated=date("Y-m-d H:i:s");
  }
}