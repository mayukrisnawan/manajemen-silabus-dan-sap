<?php

class Subject extends Model {
  static $belongs_to = array(
    array("lecturer"),
    array("curriculum", "foreign_key"=>"parent_id"),
    array("program", "foreign_key"=>"parent_id")
  );
  static $has_many = array(
    array("meetings", "order"=>"meetings.order"),
    array("standard_competencies", "order"=>"standard_competencies.content")
  );
  public $lecturer_name = "";

  static $validates_presence_of = array(
    array("name", "message"=>"Nama mata kuliah tidak boleh kosong"),
    array("code", "message"=>"Kode mata kuliah tidak boleh kosong"),
    array("credit_units", "message"=>"Jumlah SKS harus diisi")
  );

  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );

  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"terlalu panjang, panjang maksimal adalah 100 karakter"),
    array("name", "minimum"=>5, "too_short"=>"minimal terdiri dari 5 karakter")
  );

  /*
  static $validates_uniqueness_of = array(
    array("name", "message"=>"nama mata kuliah sudah digunakan, gunakan nama yang lain"), // Setiap Kurikulum Membuat Program Studi Baru, Lalu Haruskah Nama Program Studi Unik?
    array("code", "message"=>"kode mata kuliah sudah digunakan, gunakan kode yang lain")
  );
  */

  static $validates_numericality_of = array(
    array("semester", "only_integer"=>true, "message"=>"Format semester tidak valid"),
    array("credit_units", "only_integer"=>true, "message"=>"Format jumlah SKS tidak valid")
  );

  static $params = array();

  public function after_validation() {
    $this->validasi_nama_dosen();
    $this->validasi_semester();
    $this->validasi_keunikan_nama_dan_kode();
  }

  private function validasi_nama_dosen() {
    if ($this->lecturer_name != "") {
      $lecturer = Lecturer::find_by_name($this->lecturer_name);
      if (!$lecturer) {
        $this->errors->add("lecturer_name", "Nama dosen pengampu tidak ditemukan");
        $this->lecturer_id = "";
        return false;
      } else {
        $this->lecturer_id = $lecturer->id;
      }
    }
    return true;
  }

  private function validasi_semester() {
    if ($this->type != 2) {
      if (!$this->semester) {
        $this->errors->add("semester", "Semester mata kuliah harus diisi");
      }
    }
  }

  private function validasi_keunikan_nama_dan_kode() {
    if ($this->type == 0) {
      $curriculum_id = $this->parent_id;
      $curriculum = Curriculum::find($curriculum_id);
      $programs = $curriculum->programs;
      $program_ids = array();
      foreach ($programs as $program) {
        $program_ids[] = $program->id;
      }
    } else {
      $program_id = $this->parent_id;
      $program = Program::find($program_id);
      $curriculum = $program->curriculum;
      $curriculum_id = $curriculum->id;
      $programs = $curriculum->programs;
      $program_ids = array();
      foreach ($programs as $program) {
        $program_ids[] = $program->id;
      }
    }
    $program_ids = implode(",", $program_ids);
    $matkul_umum_nama = Subject::all(array(
      "conditions"=>array("id <> ? AND parent_id = ? AND name = ? AND type=0", $this->id, $curriculum_id, $this->name)
    ));
    $matkul_umum_kode = Subject::all(array(
      "conditions"=>array("id <> ? AND parent_id = ? AND code = ? AND type=0", $this->id, $curriculum_id, $this->code)
    ));
    $matkul_nama = Subject::all(array(
      "conditions"=>array("id <> ? AND name = ? AND parent_id IN (?)", $this->id, $this->name, $program_ids)
    ));
    $matkul_kode = Subject::all(array(
      "conditions"=>array("id <> ? AND code = ? AND parent_id IN (?)", $this->id, $this->code, $program_ids)
    ));
    if (count($matkul_umum_nama) !=0 || count($matkul_nama) !=0) {
      $this->errors->add("name", "nama mata kuliah sudah digunakan pada, gunakan yang lain");
    }
    if (count($matkul_umum_kode) !=0 || count($matkul_kode) !=0) {
      $this->errors->add("code", "kode mata kuliah sudah digunakan pada, gunakan yang lain");
    }
  }

  private static function umum_conditions() {
    $conditions_umum = "";
    $conditions_umum .= Subject::params("curriculum_id") == -1 ? "parent_id <> ?" : "parent_id = ?";
    $conditions_umum .= " AND type = 0";
    $conditions_umum .= Subject::params("semester") == -1 ? " AND semester <> ?" : " AND semester = ?";
    $conditions_umum .= Subject::params("subject_name") != "" ? "AND name LIKE ?" : "";
    $return = array(
      $conditions_umum,
      Subject::params("curriculum_id"),
      Subject::params("semester")
    );
    if (Subject::params("subject_name") != "") {
      $return[] = Subject::params("subject_name") . "%";
    }
    return $return;
  }

  private static function khusus_conditions() {
    $conditions_khusus = "";
    if (Subject::params("program_id") == -1) {
      $conditions_khusus .= Subject::params("curriculum_id") == -1 ? "programs.curriculum_id <> ?" : "programs.curriculum_id = ?";
    } else {
      $conditions_khusus .= "programs.id = ?";
    }
    $conditions_khusus .= Subject::params("subject_type_id") != -1 ? " AND type = ?" : " AND type <> 0";
    $conditions_khusus .= Subject::params("semester") == -1 ? " AND semester <> ?" : " AND semester = ?";
    $conditions_khusus .= Subject::params("subject_name") != "" ? "AND subjects.name LIKE ?" : "";

    if (Subject::params("subject_type_id") != -1) {
      $return = array(
        $conditions_khusus,
        Subject::params("curriculum_id"),
        Subject::params("subject_type_id"),
        Subject::params("semester")
      );
    } else {
       $return = array(
        $conditions_khusus,
        Subject::params("curriculum_id"),
        Subject::params("semester")
      );
    }
    if (Subject::params("program_id") != -1) {
      $return[1] = Subject::params("program_id");
    }
    if (Subject::params("subject_name") != "") {
      $return[] = Subject::params("subject_name") . "%";
    }
    return $return;
  }

  private static function khusus_joins() {
    return "INNER JOIN programs ON programs.id = subjects.parent_id
            INNER JOIN curriculums ON curriculums.id = programs.curriculum_id";
  }

  private static function replaceParamsWith(array $paramReplacements=array()) {
    foreach ($paramReplacements as $key => $value) {
      Subject::$params[$key] = $value;
    } 
  }

  private static function params($name) {
    return Subject::$params[$name];
  }

  public static function searchPaginate(array $paramReplacements=array(), $key="_page") {
    $paginator = new Pagination(Subject::$itemPerPage, $key);
    $records = array();
    Subject::$params = params();
    Subject::replaceParamsWith($paramReplacements);    

    // Mata Kuliah Umum
    if ((Subject::params("subject_type_id") == 0 || Subject::params("subject_type_id") == -1) && Subject::params("program_id") == -1) {
      $count_umum = Subject::first(array(
        "select"=>"COUNT(id) AS cnt",
        "conditions"=>Subject::umum_conditions()
      ))->cnt;

      $models = Subject::all(array(
        "conditions"=>Subject::umum_conditions()
      ));

      foreach ($models as $model) {
        $records[] = $model;
      }
    } else {
      $count_umum = 0;
    }

    // Mata Kuliah Khusus
    if (Subject::params("subject_type_id") != 0) {
      $count_khusus = Subject::first(array(
        "select"=>"COUNT(subjects.id) AS cnt",
        "conditions"=>Subject::khusus_conditions(),
        "joins"=>Subject::khusus_joins()
      ))->cnt;

      $models = Subject::all(array(
        "conditions"=>Subject::khusus_conditions(),
        "joins"=>Subject::khusus_joins()
      ));

      foreach ($models as $model) {
        $records[] = $model;
      }
    } else {
      $count_khusus = 0;
    }
    
    $count = $count_umum + $count_khusus;
    $paginator->set_total($count);

    $limit = $paginator->limit();
    $offset = $paginator->offset();
    $length = $limit + $offset;

    $trimmed_records = array();
    for ($i=$offset; $i < $length; $i++) { 
      if (isset($records[$i])) $trimmed_records[] = $records[$i];
    }

    $result = new MetaData();
    $result->records = $trimmed_records;
    $result->links = $paginator->page_links();
    $result->limit = $paginator->limit();
    $result->offset = $paginator->offset();
    return $result;
  }

  public function before_destroy() {
    StandardCompetency::delete_all(array(
      "conditions"=>array("subject_id"=>$this->id)
    ));
    Meeting::delete_all(array(
      "conditions"=>array("subject_id"=>$this->id)
    ));
  }
}