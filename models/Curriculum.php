<?php

class Curriculum extends Model {
  static $has_many = array(
    array("subjects", "foreign_key"=>"parent_id"),
    array("programs", "order"=>"name")
  );
  static $validates_presence_of = array(
    array("name", "message"=>"nama kurikulum tidak boleh kosong")
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );
  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"terlalu panjang, panjang maksimal adalah 100 karakter"),
    array("name", "minimum"=>5, "too_short"=>"minimal terdiri dari 5 karakter")
  );
  static $validates_uniqueness_of = array(
    array("name", "message"=>"nama kurikulum sudah digunakan, gunakan nama yang lain")
  );
  public function before_destroy() {
    Program::delete_all(array(
      'conditions'=>array('curriculum_id'=>$this->id)
    ));
    Subject::delete_all(array(
      'conditions'=>array('type = 0 AND parent_id=?', $this->id)
    ));
  }
}