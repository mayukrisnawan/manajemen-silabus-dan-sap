<?php

class AchievementIndicator extends Model {
  static $belongs_to = array(
    array("meeting")
  );
  static $validates_presence_of = array(
    array("content", "message"=>"Indikator pencapaian tidak boleh kosong")
  );  
}