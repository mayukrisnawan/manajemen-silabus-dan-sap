<?php

class Resource extends Model {
  static $has_many = array(
    array("meeting_stage_resources"),
    array("meeting_stages", "through"=>"meeting_stage_resources")
  );
  static $validates_presence_of = array(
    array("name", "message"=>"input tidak boleh kosong")
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );
  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"terlalu panjang, maksimal adalah 100 karakter"),
    array("name", "minimum"=>3, "too_short"=>"minimal terdiri dari 3 karakter")
  );
  static $validates_uniqueness_of = array(
    array("name", "message"=>"data telah ada")
  );
  public function before_destroy() {
    MeetingStageResource::delete_all(array(
      "condition"=>array("resource_id"=>$this->id)
    ));
  }
}