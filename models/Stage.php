<?php

class Stage extends Model {
  static $has_many = array(
    array("meetings", "trough"=>"meeting_stages")
  );
  static $validates_presence_of = array(
    array("label", "message"=>"input tidak boleh kosong")
  );
  static $validates_format_of = array(
    array("label", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );
  static $validates_length_of = array(
    array("label", "maximum"=>100, "too_long"=>"terlalu panjang, maksimal adalah 100 karakter"),
    array("label", "minimum"=>3, "too_short"=>"minimal terdiri dari 3 karakter")
  );
  static $validates_uniqueness_of = array(
    array("label", "message"=>"data telah ada")
  );

  public function before_destroy(){
    MeetingStage::delete_all(array(
      "conditions"=>array("stage_id"=>$this->id)
    ));
  }
}