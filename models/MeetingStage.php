<?php

class MeetingStage extends Model {
  static $belongs_to = array(
    array("meeting"),
    array("stage")
  );

  static $has_many = array(
    array("meeting_stage_resources"),
    array("resources", "through"=>"meeting_stage_resources")
  );

  public function after_validation() {
    if ($this->general_act != "") {
      $this->lecturer_act = "";
      $this->student_act = "";
    } 
  }

  public function after_validation_on_create() {
    $meeting_stage = MeetingStage::find(array(
      "select"=>"MAX(meeting_stages.order) as biggest_order",
      "conditions"=>array("meeting_id"=>$this->meeting_id)
    ));
    $biggest_order = !is_null($meeting_stage->biggest_order) ? $meeting_stage->biggest_order+1 : 0;
    $this->order = $biggest_order;
  }

  public function before_destroy() {
    MeetingStageResource::delete_all(array(
      "conditions"=>array("meeting_stage_id"=>$this->id)
    ));
  }
}