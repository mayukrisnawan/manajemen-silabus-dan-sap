<?php

class Program extends Model {
  static $belongs_to = array(
    array("curriculum")
  );
  static $has_many = array(
    array("subjects", "foreign_key"=>"parent_id")
  );
  static $validates_presence_of = array(
    array("name", "message"=>"nama program studi tidak boleh kosong")/*,
    array("min_semester", "message"=>"nilai semester minimal tidak boleh kosong"),    
    array("max_semester", "message"=>"nilai semester maksimal tidak boleh kosong")*/    
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet"),
  );
  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"terlalu panjang, panjang maksimal adalah 100 karakter"),
    array("name", "minimum"=>5, "too_short"=>"minimal terdiri dari 5 karakter")
  );
  /*
  static $validates_numericality_of = array(
    array("min_semester", "only_integer"=>true, "message"=>"harus berupa bilangan bulat"),
    array("max_semester", "only_integer"=>true, "message"=>"harus berupa bilangan bulat"),
    array("min_semester", "greater_than_or_equal_to"=>1, "message"=>"tidak boleh kurang dari satu"),    
    array("max_semester", "greater_than_or_equal_to"=>1, "message"=>"tidak boleh kurang dari satu")  
  );
  static $validates_uniqueness_of = array(
    array("name", "message"=>"nama program studi sudah digunakan, gunakan nama yang lain")
  );
  */
  public function before_destroy() {
    Subject::delete_all(array(
      'conditions'=>array('type <> 0 AND parent_id=?', $this->id)
    ));
  }

  public function after_validation() {
    $this->validasi_keunkikan_nama();
  }

  private function validasi_keunkikan_nama() {
    $programs_count = Program::count(array(
      "conditions"=>array("curriculum_id = ? AND id <> ? AND name = ?", $this->curriculum_id, $this->id, $this->name)
    ));
    if ($programs_count != 0) {
      $this->errors->add("name", "nama program studi sudah digunakan, gunakan nama yang lain");
    }
  }
}