<?php

class MeetingReference extends Model {
  static $belongs_to = array(
    array("meeting"),
    array("reference")
  );  
}