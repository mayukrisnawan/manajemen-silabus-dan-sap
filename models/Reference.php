<?php

class Reference extends Model {
  static $has_many = array(
    array("meeting_references"),
    array("meetings", "through"=>"meeting_references")
  );
  static $validates_presence_of = array(
    array("content", "message"=>"tidak boleh kosong")
  );
  static $validates_length_of = array(
    array("content", "maximum"=>500, "too_long"=>"terlalu panjang, panjang maksimal adalah 500 karakter"),
    array("content", "minimum"=>5, "too_short"=>"minimal terdiri dari 5 karakter")
  );
  static $validates_format_of = array(
    array("content", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );

  public function before_destroy() {
    MeetingReference::delete_all(array(
      "conditions"=>array("reference_id"=>$this->id)
    ));
  }
}