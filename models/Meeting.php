<?php

class Meeting extends Model {
  static $belongs_to = array(
    array("subject")
  );

  static $has_many = array(
    array("meeting_references"),
    array("references", "through"=>"meeting_references"),
    array("meeting_stages"),
    array("stages", "through"=>"meeting_stages"),    
    array("achievement_indicators")
  );

  static $validates_presence_of = array(
    array("main_lesson", "message"=>"Materi pokok harus diisi"),
    array("learning_experiences", "message"=>"Pengalaman belajar harus diisi"),
    array("tm", "message"=>"Harus diisi"),
    array("p", "message"=>"Harus diisi"),
    array("l", "message"=>"Harus diisi"),
    array("basic_competency", "message"=>"Kompetensi dasar harus diisi")    
  );

  static $validates_length_of = array(
    array("main_lesson", "maximum"=>100, "too_long"=>"terlalu panjang, panjang maksimal adalah 100 karakter"),
    array("main_lesson", "minimum"=>5, "too_short"=>"minimal terdiri dari 5 karakter")
  );

  static $validates_numericality_of = array(
    array("tm", "only_integer"=>true, "message"=>"harus berupa bilangan bulat"),
    array("p", "only_integer"=>true, "message"=>"harus berupa bilangan bulat"),
    array("l", "only_integer"=>true, "message"=>"harus berupa bilangan bulat"),
    array("tm", "greater_than_or_equal_to"=>0, "message"=>"tidak boleh kurang dari nol"),    
    array("p", "greater_than_or_equal_to"=>0, "message"=>"tidak boleh kurang dari nol"),  
    array("l", "greater_than_or_equal_to"=>0, "message"=>"tidak boleh kurang dari nol")  
  );

  public function after_validation() {
    if (!$this->t) $this->t = 0;
    if (!$this->uk) $this->uk = 0;
    if (!$this->us) $this->us = 0;
  }

  public function after_validation_on_create() {
    $meeting = Meeting::find(array(
      "select"=>"MAX(meetings.order) as biggest_order",
      "conditions"=>array("subject_id"=>$this->subject_id)
    ));
    $biggest_order = !is_null($meeting->biggest_order) ? $meeting->biggest_order+1 : 0;
    $this->order = $biggest_order;
  }

  public function get_order() {
    $meetings = Meeting::find(array(
      "select"=>"COUNT(id) AS jml",
      "conditions"=>array(
        "subject_id = ? AND id <> ? AND meetings.order <= ?",
        $this->subject_id, $this->id, $this->order
      )
    ));
    $order = $meetings->jml + 1;
    return $order;
  }
  public function before_destroy() {
    MeetingReference::delete_all(array(
      "conditions"=>array("meeting_id"=>$this->id)
    ));
    MeetingStage::delete_all(array(
      "conditions"=>array("meeting_id"=>$this->id)
    ));
    AchievementIndicator::delete_all(array(
      "conditions"=>array("meeting_id"=>$this->id)
    ));
  } 
}