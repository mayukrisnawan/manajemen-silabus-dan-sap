<?php

class Lecturer extends Model {
  static $has_many = array(array("subjects"));
  static $belongs_to = array(array("user"));
  static $validates_presence_of = array(
    array("name", "message"=>"Nama Dosen tidak boleh kosong")
  );
  static $validates_length_of = array(
    array("name", "maximum"=>100, "too_long"=>"maksimal Nama 100 karakter"),
    array("name", "minimum"=>3, "too_short"=>"minimal Nama terdiri dari 3 karakter")
  );
  static $validates_format_of = array(
    array("name", "with"=>"/^[a-zA-Z]/", "message"=>"harus dimulai dengan alphabet")
  );

  public function after_validation_on_create() {
    $user = new User();
    $user->level = UM::levelId("dosen");
    $user->username = "dosen";
    $user->password = "dosen";
    if (!$user->save()) {
      $this->errors->add("name", "akun dosen tidak berhasil dibuat");
    }
    $user->username = "dosen" . $user->id;
    $user->password = md5("dosen" . $user->id);
    if (!$user->save()) {
      $this->errors->add("name", "akun dosen tidak berhasil dibuat");
    }
    $this->user_id = $user->id;
  }

  public function before_destroy() {
    Subject::delete_all(array(
      "conditions"=>array("lecturer_id"=>$this->id)
    ));
    $user = User::find($this->user_id);
    $user->delete();
  }
}