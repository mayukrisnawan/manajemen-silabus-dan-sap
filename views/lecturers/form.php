<form role="form" name='lecturer_form' action='{{ u(action, {id:lecturer.id}) }}' method='{{method}}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="lecturer[id]" value="{{lecturer.id}}">
  <div class="form-group">
    <label class="control-label">Nama Dosen</label>
    <input name="lecturer[name]" type="text" class="form-control" value="{{lecturer.name}}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("lecturer"); ?>
</script>