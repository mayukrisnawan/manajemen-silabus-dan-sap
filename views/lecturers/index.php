{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<script type="text/javascript">
var xhrDosen = null;
function searchDosen(page){
  if (!page) page = 1;
  var namaDosen = $("#search_dosen").val();
  if (xhrDosen) {
    xhrDosen.abort();
    xhrDosen = null;
  }
  xhrDosen = $.ajax({
    url:"{{ u('lecturers#search') }}",
    data:"nama_dosen="+namaDosen+"&_page="+page,
    success:function(response){
      $("#lecturers_container").html(response)
                               .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
    },
    beforeSend:function(){
      $("#lecturers_container").html("<center>Memuat data&nbsp;{{ img('loading.gif') }}</center>");
    }
  });
}
</script>
<h1>Dosen</h1>
<hr/>
<form class="form-inline" style="margin : 10px auto">
  {% if has_level('admin') %}
  <div class="form-group">
    {{a("<span class='glyphicon glyphicon-plus'></span> Tambah Daftar Dosen", "lecturers#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
  {% endif %}
  <div class="form-group col-md-12 pull-right" style="padding:0px;">
    <div class="input-group">
      <input id="search_dosen" type="text" class="form-control" placeholder="Cari berdasarkan nama dosen.."/>
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-search"></i>
      </span>
    </div>
  </div>
</form>
<div class="table-responsive" id="lecturers_container">
  {% include "lecturers/_lecturers_table.php" %}
</div>
<script type="text/javascript">
$("#search_dosen").keyup(searchDosen);
</script>
{% endblock %}