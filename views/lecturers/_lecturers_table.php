 <table class="table table-bordered table-striped tabel-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Nama Dosen</th>
      {% if has_level('admin') %}
      <th>Username</th>
      <th>Password</th>
      <th>Pilihan</th>
      {% endif %}
    </tr>
  <thead>
  <tbody>
    {% if pagination.records|length == 0 %}
    <tr>
      <td colspan="5">
        <center>
          {% if query == '' %}
            <i>Belum ada data dosen yang ditambahkan</i>
          {% else %}
            <i>Pencarian dengan kata kunci '{{ query }}' tidak menghasilkan apapun</i>
          {% endif %}
        </center>
      </td>
    </tr>
    {% endif %}
    {% for lecturer in pagination.records %}
    <tr>
      <td>{{loop.index + pagination.offset}}</td>
      <td>{{ lecturer.name }}
      </td>
      {% if has_level('admin') %}
      <td>{{ lecturer.user.username }}</td>
      <td>
        {% if md5(lecturer.user.username) == lecturer.user.password %}
          {{ lecturer.user.username }}
        {% else %}
          {{ a("Reset Password", "reset_password", {id:lecturer.user.id}, {method:"patch", remote:"true",
              confirmTitle:"Reset Password",
              confirmMessage:"Apakah Anda yankin ingin mereset password dari pengguna berikut?"
            }) 
          }}
        {% endif %}
      </td>
      <td>
        {{
          a("Edit", "lecturers#edit", {id:lecturer.id})
        }}
        |
        {{
          a("Hapus","lecturers#destroy", {id:lecturer.id},{method:"delete", remote:"true",
            confirmTitle:"Dosen",
            confirmMessage:"Apakah Anda ingin menghapus data dosen berikut?",
            class:"btn_hapus"
          })
        }}
      </td>
      {% endif %}
    </tr>
  {% endfor %}
  </tbody>
</table>
{{ pagination.links|raw}}
<script type="text/javascript">
  $(".pagination a").click(function(){
    var page = $(this).attr("href");
    page = page.replace("?_page=", "");
    searchDosen(page);
    return false;
  });
</script>