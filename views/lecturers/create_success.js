$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> Dosen pengajar berhasil ditambahkan, {{ a('Kembali','lecturers#index') }}")
           .show();
$("form[name='lecturer_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='lecturer_form']").find("input[type='submit']")
                                  .attr("disabled", "disabled")
                                  .unbind("click");