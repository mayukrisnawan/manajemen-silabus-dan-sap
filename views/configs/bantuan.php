{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Halaman Bantuan</h1>
<hr/>
<div class="table-responsive">
  <table class="table table-bordered table-striped tabel-hover">
    <thead>
      <tr>
        <th>Bantuan</th>
        <th widht="5%"></th>
      </tr>
    <thead>
    <tbody>
      <tr>
        <td><?php echo html_entity_decode("{{ config.value }}"); ?></td>
        <td>
          {{
            a("Edit", "configs#edit", {id:config.id})
          }}
        </td>
      </tr>
     </tbody>
  </table>
</div>
{{ pagination.links|raw}}
{% endblock %}