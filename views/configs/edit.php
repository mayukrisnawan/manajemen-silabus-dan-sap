{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit</h1>
<div class="row">
  <div class="col-md-40">
    {% include 'configs/form.php' %}
  </div>
</div>
{% endblock %}