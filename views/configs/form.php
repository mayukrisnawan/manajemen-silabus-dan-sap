<form role="form" name='config_form' action='{{ u(action, {id:config.id}) }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="config[id]" value="{{ config.id }}">
    <div class="form-group">
    {% if config.key == "halaman_informasi" %}
      <label class="control-label">Halaman Informasi</label>
    {% else %}
      <label class="control-label">Halaman Bantuan</label>
    {% endif %}
    <textarea name="config[value]" class="form-control">{{ config.value }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <a class="btn btn-success" id="btn_submit_edit_config">Simpan</a>
  </div>
</form>
<script type="text/javascript">
var $frm = $("form[name='config_form']"),
    action = $frm.attr("action"),
    method = $frm.attr("method");
var $editor = $("textarea[name='config[value]']");
$editor.summernote({
  height:300
});
$("#btn_submit_edit_config").click(function(){
  var btn = this;
  var content = $editor.code();
  var timer = loadDotsAnimationInto(btn, "Menyimpan");
  $(btn).attr("disabled", "disabled");
  $.ajax({
    url:action,
    method:method,
    dataType:"script",
    data:"config[value]="+encodeURIComponent(content),
    complete:function(){
      $(btn).removeAttr("disabled");
      stopDotsAnimationOn(timer, btn, "Simpan");
    }
  });
  return false;
});
</script>