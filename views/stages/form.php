<form role="form" name='stage_form' action='{{ u(action, {id:stage.id}) }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="stage[id]" value="{{ stage.id }}">
  <div class="form-group">
    <label class="control-label">Nama Tahapan Pembelajaran</label>
    <input name="stage[label]" type="text" class="form-control" value="{{ stage.label }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("stage"); ?>
</script>