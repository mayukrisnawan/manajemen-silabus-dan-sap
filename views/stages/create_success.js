$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> Data Tahapan Pembelajaran berhasil ditambahkan, {{ a('Kembali','stages#index') }}")
           .show();
$("form[name='stage_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='stage_form']").find("inpit[type='submit']")
                                  .attr("disabled", "disabled")
                                  .unbind("click");