{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Tahapan Pembelajaran</h1>
{% if has_level('admin') %}
<hr/>
<form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah Tahapan Pembelajaran", "stages#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped tabel-hover">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Tahapan</th>
          {% if has_level('admin') %}
          <th>Pilihan</th>
          {% endif %}
        </tr>
      </thead>
    <tbody>
      {% if stages|length == 0 %}
      <tr>
        <td colspan="3">
          <center>
            <i>Belum ada daftar tahapan pembelajaran</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for stage in stages %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td> <!--memberikan urutan penomeran data -->
        <td>
            {{ stage.label }}
        </td>
        {% if has_level('admin') %}
        <td>
          {{
            a("Edit", "stages#edit", {id:stage.id}) 
          }}
          | <!--membuat link ke edit.php -->
          {{
            a("Hapus", "stages#destroy", {id:stage.id}, {method:"delete", remote:"true",
              confirmTitle:"Hapus Data",
              confirmMessage:"Apakah anda yakin ingin menghapus Tahapan Pembelajaran berikut?",
              class:"btn_hapus"
            })
          }}
        </td>
        {% endif %}
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{% endblock %}