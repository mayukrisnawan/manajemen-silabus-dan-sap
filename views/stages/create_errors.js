var $frm = $("form[name='stage_form']");
{% for attr, error in errors %}
  {% set joined = "" %}
  {% for msg in error %}
    {% if joined != "" %}
      {% set joined = joined ~ ", "%}
    {% endif %}
    {% set joined = joined ~ msg %}
  {% endfor %}
  $frm.find("[name='stage[{{attr}}]']").parent()
                                          .addClass("has-error")
                                          .find(".help-block")
                                          .html("{{ joined|capitalize }}");
{%endfor %}