{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>{{ curriculum.name }}</h1>
<hr/>
<form class="form-inline" style="margin : 10px auto">
  {% if has_level('admin') %}
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah program studi baru", "curriculum_programs#add", {curriculum_id:curriculum.id}, {class:"btn btn-success form-control"}) }}
  </div>
  {% endif %}
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-ok'></span> Lihat Daftar Mata Kuliah Umum", "matkul_umum", {curriculum_id:curriculum.id}, {class:"btn btn-info form-control"}) }}
  </div>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Program Studi</th>
        {% if has_level('admin') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
      <tr>
        <td colspan="3">
          <center>
            <i>Belum ada program studi</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for program in pagination.records %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td>
        <td>
            {{ a(program.name, "curriculum_programs#show", {curriculum_id:curriculum.id, id:program.id}) }}
        </td>

        {% if has_level('admin') %}
        <td>
          {{ 
            a("Edit", "curriculum_programs#edit", {curriculum_id:curriculum.id, id:program.id}) 
          }}
          |
          {{ 
            a("Hapus", "programs#destroy", {id:program.id}, {method:"delete", remote:"true", 
              confirmTitle:"Program Studi",
              confirmMessage:"Apakah anda yakin ingin menghapus program studi berikut?",
              class:"btn_hapus"
            }) 
          }}
        </td>
        {% endif %} 
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}