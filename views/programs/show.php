{% extends current_user_level() == "guest" ? "layouts/guest_layout.php" : current_user_level() == "admin" ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}
{% block body %}
<script type="text/javascript">
var xhrMatkul = null;
function searchMatkul(page){
  if (!page) page = 1;
  var subject_type_id = $("#filter_subject_type").val(),
      semester = $("#filter_semester").val(),
      subject_name = $("#search_by_subject_name").val();

  var data = "";
  data += "&subject_type_id=" + subject_type_id;
  data += "&semester=" + semester,
  data += "&subject_name=" + subject_name;

  if (xhrMatkul) {
    xhrMatkul.abort();
    xhrMatkul = null;
  }
  xhrMatkul = $.ajax({
    url:"{{ u('programs#search_matkul', {id:program.id, curriculum_id:program.curriculum.id}) }}",
    data:data+"&_page="+page,
    success:function(response){
      $("#subjects_container").html(response)
                              .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
    },
    beforeSend:function(){
      $("#subjects_container").html("<center>Memuat data&nbsp;{{ img('loading.gif') }}</center>");
    }
  });
}
</script>
<h1>{{ program.name }}</h1>
{% if has_level('admin') %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah mata kuliah baru", "matkul_add", {program_id:program.id}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
{% include "subjects/_filter.php" %}
<div class="table-responsive" id="subjects_container">
  {% include "subjects/_subjects_table.php" %}
</div>
{% endblock %}