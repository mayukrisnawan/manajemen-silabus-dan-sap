$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> program studi berhasil ditambahkan, {{ a('Kembali','curriculum_programs#index', {curriculum_id:curriculum.id}) }}")
           .show();
$("form[name='program_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='program_form']").find("input[type='submit']")
                                  .attr("disabled", "disabled")
                                  .unbind("click");