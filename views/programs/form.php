<form role="form" name='program_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="program[id]" value="{{ program.id }}">
  <div class="form-group">
    <label class="control-label">Nama Program Studi</label>
    <input name="program[name]" type="text" class="form-control" value="{{ program.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("program"); ?>
</script>