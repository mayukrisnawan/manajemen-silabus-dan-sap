{% extends "layouts/dosen_layout.php" %}
{% block body %}
<h1>Daftar Ampu Mata Kuliah</h1>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-condensed table_hover">
	<thead>
	  <tr>
	    <th>No</th>
        <th>Kode</th>
        <th>Nama Mata Kuliah</th>
        <th>Semester Mata Kuliah</th>
        <th>Jumlah SKS</th>
        <th>Kurikulum/Program Studi</th>
        <th>Jenis</th>
      </tr>
    </thead>
  <tbody>
      {% if subjects|length == 0 %}
      <tr>
        <td colspan="7">
          <center>
            <i>Belum ada daftar Ampu Mata Kuliah</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for subject in subjects %}
      <tr>
        <td>{{loop.index}}</td>
        <td>{{subject.code}} </td>
        <td>{{ a(subject.name, "subjects#show", {id:subject.id}) }} </td>
        <td>{{subject.semester}} </td>
        <td>{{subject.credit_units}} </td>
        <td>{{subject.type == 0 ? subject.curriculum.name :  subject.program.name}} </td>
        <td>
        	{% if subject.type == 0 %}
        		Umum
        	{% elseif subject.type == 1 %}
        		Wajib
        	{% elseif subject.type == 2 %}
        		Pilihan
        	{% endif %}
        </td>
      </tr>
      {% endfor %}
  </tbody>
 </table>
</div>
{% endblock %}