{% extends "layouts/dosen_layout.php" %}

{% block panel_body_open %}{% endblock %}
{% block panel_body_close %}{% endblock %}

{% block body %}
<h1>Dashboard Dosen</h1>
<h5>{{ user.username }} ({{ user.lecturer.name }}) </h5>
<hr/>
Selamat datang pada halaman dashboard dosen, temukan yang anda bisa cari disini :<br/>
<ul>
  <li>
    {{ a("Daftar mata kuliah yang anda ampu", "dosen#ampu_mata_kuliah") }}
  </li>
  <li>
    {{ a("Daftar semua mata kuliah", "subjects#index") }}
  </li>
  <li>
    {{ a("Daftar kurikulum", "dosen#kurikulum") }}
  </li>
</ul>
{% endblock %}