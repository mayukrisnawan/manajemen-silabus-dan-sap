<form role="form" name='meeting_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="meeting[id]" value="{{ meeting.id }}">
  <div class="form-group">
    <label class="control-label">Materi Pokok</label>
    <textarea name="meeting[main_lesson]" class="form-control">{{ meeting.main_lesson }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Pengalaman Belajar</label>
    <textarea name="meeting[learning_experiences]" class="form-control">{{ meeting.learning_experiences }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Penilaian</label>
    <input name="meeting[t]" type="checkbox" class="app-checkbox" {% if meeting.t==1 %} value="1" checked=checked {% else %} value="0" {% endif %}/>Teori&nbsp;
    <input name="meeting[uk]" type="checkbox" class="app-checkbox" {% if meeting.uk==1 %} value="1" checked=checked {% else %} value="0" {% endif %}/>Unjuk Kemampuan&nbsp;
    <input name="meeting[us]" type="checkbox" class="app-checkbox" {% if meeting.us==1 %} value="1" checked=checked {% else %} value="0" {% endif %}/>Unjuk Sikap&nbsp;
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Kompetensi Dasar</label>
    <textarea name="meeting[basic_competency]" class="form-control">{{ meeting.basic_competency }}</textarea>
    <span class="help-block"></span>
  </div>
  <h3>Alokasi Waktu</h3>
  <div class="form-group">
    <label>Tatap Muka</label>
    <div class="input-group" style="width:80px;">
      <input style="width:60px;" name="meeting[tm]" type="text" class="form-control app-input-number" value="{{ meeting.tm is defined ? meeting.tm : 0}}"/>
      <span style="width:40px;" class="input-group-addon">menit</span>
    </div>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label>Penilaian</label>
    <div class="input-group" style="width:80px;">
      <input style="width:60px;" name="meeting[p]" type="text" class="form-control app-input-number" value="{{ meeting.p is defined ? meeting.p : 0}}"/>
      <span style="width:40px;" class="input-group-addon">menit</span>
    </div>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label>Latihan</label>
    <div class="input-group" style="width:80px;">
      <input style="width:60px;" name="meeting[l]" type="text" class="form-control app-input-number" value="{{ meeting.l is defined ? meeting.l : 0}}"/>
      <span style="width:40px;" class="input-group-addon">menit</span>
    </div>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("meeting"); ?>
</script>