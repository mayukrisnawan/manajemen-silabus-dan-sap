<table class="table table-bordered table-striped table-hover">
  <tbody>
    {% for meeting_reference in meeting_references %}
    {% set reference = meeting_reference.reference %}
    <tr>
      <td width="5%">{{ loop.index }}</td>
      <td>{{ reference.content }}</td>
      <td width="15%">
        {{ a("[x] Batalkan Penyertaan Referensi", "meetingreferences#destroy", {id:meeting_reference.id}, {method:"delete", remote:"true", 
              confirmTitle:"Pembatalan Penyertaan Referensi",
              confirmMessage:"Apakah anda yakin ingin membatalkan penyertaan referensi berikut?",
              class:"btn_hapus"
            })
        }}
      </td>
    </tr>
    {% endfor %}
    {% if meeting_references|length == 0 %}
    <tr>
      <td>
        <center>
          <i>Belum ada referensi pada pertemuan ini, {{ a("Tambahkan Referensi", meeting.id, {}, {class:"btn-add-reference"}) }}</i>
        </center>
      </td>
    </tr>
    {% else %}
    <tr>
      <td colspan="3" align="right">
        {{ a("+ Tambahkan Referensi Lagi", meeting.id, {}, {class:"btn-add-reference"}) }}
      </td>
    </tr>
    {% endif %}
  </tbody>
</table>
<script type="text/javascript">
linkReferenceModalTo($(".btn-add-reference"), function(){
  loadReferenceList();
});
</script>