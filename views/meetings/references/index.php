{% include "meetings/references/_modal_select_reference.php" %}
<script type="text/javascript">
  function loadReferenceList() {
    var container = document.getElementById("references_container");
    var loader = loadDotsAnimationInto(container, "Memuat Referensi");
    $.ajax({
      url:"{{ u('meeting_references_table', {id:meeting.id}) }}",
      success:function(response){
        stopDotsAnimationOn(loader, container, "");
        $("#references_container").hide()
                                  .html(response)
                                  .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
      }
    });
  }
</script>
<h3>Daftar Referensi</h3>
<div class="table-responsive" id="references_container">
  {% include "meetings/references/_references_table.php" %}
</div>