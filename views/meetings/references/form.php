<form role="form" name='meeting_reference_form' action='{{ u("meetingreferences#create") }}' method='post'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="meeting_reference[meeting_id]" value="{{ meeting.id }}">
  <div class="form-group">
    <label class="control-label">Referensi Baru</label>
    <textarea name="meeting_reference[new_content]" class="form-control"></textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Referensi yang sudah tersedia</label>
    <div class="input-group" style="width:100%">
      <textarea name="meeting_reference[existing_content]" id="search_reference" class="form-control"></textarea>
      <span style="width:40px;" class="input-group-addon">
        <i class="glyphicon glyphicon-search"></i>
      </span>
    </div>
    <span class="help-block"></span>
  </div>
</form>
<script type="text/javascript">
  // Pencarian Referensi
  $("#search_reference").typeahead({
    source: function (query, typeahead) {
      return $.getJSON("{{ u('references#json') }}", { content: query }, function (data) {
        var references = [];
        for (var index in data) {
          references.push(data[index].content);
        }
        return typeahead(references);
      });
    }
  });

  
</script>