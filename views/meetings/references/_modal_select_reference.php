<div class="modal-select-reference modal">
  <div class="modal-dialog" style='width:60%'>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title"><b>Tambah Referensi</b></h4>
      </div>
      <div class="modal-body">
        {% include "meetings/references/form.php" %}
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-success" id="btn_submit_select_reference" disabled>Tambahkan</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal">Kembali</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var $reference_form = $("form[name='meeting_reference_form']"),
    $flash = $reference_form.find(".flash"),
    $new_reference = $("textarea[name='meeting_reference[new_content]']"),
    $existing_reference = $("textarea[name='meeting_reference[existing_content]']"),
    $submit_reference = $("#btn_submit_select_reference");

function restoreReferenceFormState() {
  $submit_reference.show();
  $("form[name='meeting_reference_form']").find(".form-control").removeAttr("disabled");
  $flash.html("").hide();
  $new_reference.val("");
  $existing_reference.val("");
}

function linkReferenceModalTo($element, callback) {
  $element.click(function(){
    restoreReferenceFormState();
    var modal = $(".modal-select-reference").modal("show");
    modal.on("hidden.bs.modal", function(){
      if (callback) callback();
    });
    return false;
  });
}

function showReferenceError(msg) {
  msg += "<span class='close'>x</span>"
  $flash.removeClass("alert-success").addClass("alert-danger");
  $flash.html(msg).slideDown();
  $flash.find(".close").click(function(){
    $flash.slideUp();
  });
}

function showReferenceSuccess(msg) {
  msg += "<span class='close'>x</span>"
  $flash.removeClass("alert-danger").addClass("alert-success");
  $flash.html(msg).slideDown();
  $flash.find(".close").click(function(){
    $flash.slideUp();
  });
}

function checkMeetingSubmitTrigger() {
  var new_content = $new_reference.val(),
      existing_content = $existing_reference.val();
  new_content = new_content.replace(/[ \n]/g,"");
  existing_content = existing_content.replace(/[ \n]/g,"");
  if (new_content != "" || existing_content != "") {
    $submit_reference.removeAttr("disabled");
  } else {
    $submit_reference.attr("disabled", "disabled");
  }
}

$new_reference.keyup(checkMeetingSubmitTrigger);
$existing_reference.keyup(checkMeetingSubmitTrigger);

$submit_reference.click(function(){
  $flash.slideUp();
  $submit_reference.attr("disabled", "disabled");
  var loader = loadDotsAnimationInto($submit_reference.get(0), "Menyimpan data");
  $("form[name='meeting_reference_form']").ajaxSubmit({
    dataType:'script',
    success:function(){
      stopDotsAnimationOn(loader, $submit_reference.get(0), "Tambahkan");
      $submit_reference.removeAttr("disabled");
    },
    error:function(){
      stopDotsAnimationOn(loader, $submit_reference.get(0), "Tambahkan");
      $submit_reference.removeAttr("disabled");
      showReferenceError("Terjadi kesalahan, referensi tidak berhasil ditambahkan");
    }
  });
});
</script>