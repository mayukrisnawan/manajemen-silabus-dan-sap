function linkEditAchievementIndicatorModalTo($element, callback) {
  $element.each(function(){
    $(this).click(function(){
      var id = $(this).attr("href");
      restoreAchievementIndicatorFormState();
      $achievement_indicator_form_identity.val(id);
      var content = $("#achievement_indicator_content_"+id).html();
      $frm_achievement_indicator.find("[name='achievement_indicator[content]']").val(content);
      $submit_edit_achievement_indicator.removeAttr("disabled").show();
      $submit_add_achievement_indicator.hide();
      $frm_achievement_indicator.get(0).method = "patch";
      var url = "{{ u('achievementindicators#update', {id:''}) }}";
      url = url.substr(0, url.length-1);
      url = url + id;
      $frm_achievement_indicator.attr("action", url);
      $achievement_indicator_modal_title.html("Edit Indikator Pencapaian");
      var modal = $(".modal-achievement-indicator").modal("show");
      modal.on("hidden.bs.modal", function(){
        if (callback) callback();
      });
      return false;
    });
  });
}

$submit_edit_achievement_indicator.click(function(){
  $submit_edit_achievement_indicator.attr("disabled", "disabled");
  var loader = loadDotsAnimationInto($submit_edit_achievement_indicator.get(0), "Menyimpan data");
  $.ajax({
    url:$frm_achievement_indicator.attr("action"),
    method:"patch",
    data:$frm_achievement_indicator.formSerialize(),
    dataType:'script',
    success:function(){
      stopDotsAnimationOn(loader, $submit_edit_achievement_indicator.get(0), "Simpan");
      $submit_edit_achievement_indicator.removeAttr("disabled");
    },
    error:function(){
      stopDotsAnimationOn(loader, $submit_edit_achievement_indicator.get(0), "Simpan");
      $submit_edit_achievement_indicator.removeAttr("disabled");
      showAchievementIndicatorError("Terjadi kesalahan, indikator pencapaian tidak berhasil ditambahkan");
    }
  });
  return false;
});