<form role="form" name='achievement_indicator_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="achievement_indicator[meeting_id]" value="{{ meeting.id }}">
  <input type="hidden" name="achievement_indicator[id]" value="">
  <div class="form-group">
    <label class="control-label">Indikator Pencapaian</label>
    <textarea name="achievement_indicator[content]" class="form-control"></textarea>
    <span class="help-block"></span>
  </div>
</form>