function linkAddAchievementIndicatorModalTo($element, callback) {
  $element.click(function(){
    restoreAchievementIndicatorFormState();
    $achievement_indicator_form_identity.val("");
    $submit_add_achievement_indicator.attr("disabled", "disabled").show();
    $submit_edit_achievement_indicator.hide();
    $frm_achievement_indicator.get(0).method = "post";
    $frm_achievement_indicator.attr("action", "{{ u('achievementindicators#create') }}");
    $achievement_indicator_modal_title.html("Tambah Indikator Pencapaian");
    var modal = $(".modal-achievement-indicator").modal("show");
    modal.on("hidden.bs.modal", function(){
      if (callback) callback();
    });
    return false;
  });
}

$submit_add_achievement_indicator.click(function(){
  $submit_add_achievement_indicator.attr("disabled", "disabled");
  var loader = loadDotsAnimationInto($submit_add_achievement_indicator.get(0), "Menyimpan data");
  $frm_achievement_indicator.ajaxSubmit({
    dataType:'script',
    success:function(){
      stopDotsAnimationOn(loader, $submit_add_achievement_indicator.get(0), "Simpan");
      $submit_add_achievement_indicator.removeAttr("disabled");
    },
    error:function(){
      stopDotsAnimationOn(loader, $submit_add_achievement_indicator.get(0), "Simpan");
      $submit_add_achievement_indicator.removeAttr("disabled");
      showAchievementIndicatorError("Terjadi kesalahan, indikator pencapaian tidak berhasil ditambahkan");
    }
  });
});