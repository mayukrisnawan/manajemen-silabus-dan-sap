<table class="table table-bordered table-striped table-hover">
  <tbody>
    {% for achievement_indicator in achievement_indicators %}
    <tr>
      <td width="5%">{{ loop.index }}</td>
      <td id='achievement_indicator_content_{{ achievement_indicator.id }}'>{{ achievement_indicator.content }}</td>
      <td width="18%">
        {{
          a("Edit", achievement_indicator.id, {}, {class:"btn-edit-achievement-indicator"})
        }}
        |
        {{ a("Hapus", "achievementindicators#destroy", {id:achievement_indicator.id}, {method:"delete", remote:"true", 
              confirmTitle:"Hapus Indikator Pencapaian",
              confirmMessage:"Apakah anda yakin ingin menghapus indikator pencapaian berikut?",
              class:"btn_hapus"
            })
        }}
      </td>
    </tr>
    {% endfor %}
    {% if achievement_indicators|length == 0 %}
    <tr>
      <td>
        <center>
          <i>Belum ada indikator pencapaian pada pertemuan ini, {{ a("Tambah indikator pencapaian", meeting.id, {}, {class:"btn-add-achievement-indicator"}) }}</i>
        </center>
      </td>
    </tr>
    {% else %}
    <tr>
      <td colspan="3" align="right">
        {{ a("+ Tambahkan Indikator Pencapaian Lagi", meeting.id, {}, {class:"btn-add-achievement-indicator"}) }}
      </td>
    </tr>
    {% endif %}
  </tbody>
</table>
<script type="text/javascript">
linkAddAchievementIndicatorModalTo($(".btn-add-achievement-indicator"), function(){
  loadAchievementIndicatorList();
});
linkEditAchievementIndicatorModalTo($(".btn-edit-achievement-indicator"), function(){
  loadAchievementIndicatorList();
});
</script>