<div class="modal-achievement-indicator modal">
  <div class="modal-dialog" style='width:60%'>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title"><b id="achivement_indicator_modal_title"></b></h4>
      </div>
      <div class="modal-body">
        {% include "meetings/achievement_indicators/form.php" %}
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-success" id="btn_submit_add_achievement_indicator" disabled>Simpan</a>
        <a type="button" class="btn btn-success" id="btn_submit_edit_achievement_indicator" disabled>Simpan</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal">Kembali</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var $submit_add_achievement_indicator = $("#btn_submit_add_achievement_indicator"),
    $submit_edit_achievement_indicator = $("#btn_submit_edit_achievement_indicator"),
    $frm_achievement_indicator = $("form[name='achievement_indicator_form']"),
    $flash_achievement_indicator = $frm_achievement_indicator.find(".flash"),
    $achievement_indicator_modal_title = $("#achivement_indicator_modal_title"),
    $achievement_indicator_form_identity = $frm_achievement_indicator.find("input[name='achievement_indicator[id]']");

{% include "meetings/achievement_indicators/_action_add.js" %}
{% include "meetings/achievement_indicators/_action_edit.js" %}

function restoreAchievementIndicatorFormState() {
  $("form[name='achievement_indicator_form']").find(".form-control").removeAttr("disabled");
  $flash_achievement_indicator.html("").hide();
  $frm_achievement_indicator.find(".form-control").val("");
}

function showAchievementIndicatorError(msg) {
  msg += "<span class='close'>x</span>"
  $flash_achievement_indicator.removeClass("alert-success").addClass("alert-danger");
  $flash_achievement_indicator.html(msg).slideDown();
  $flash_achievement_indicator.find(".close").click(function(){
    $flash_achievement_indicator.slideUp();
  });
}

function showAchievementIndicatorSuccess(msg) {
  msg += "<span class='close'>x</span>"
  $flash_achievement_indicator.removeClass("alert-danger").addClass("alert-success");
  $flash_achievement_indicator.html(msg).slideDown();
  $flash_achievement_indicator.find(".close").click(function(){
    $flash_achievement_indicator.slideUp();
  });
}

$("[name='achievement_indicator[content]']").keyup(function(){
  var content = $(this).val();
  content = content.replace(/[ \n]/g, "");
  if (content == "") {
    $submit_add_achievement_indicator.attr("disabled", "disabled");
    $submit_edit_achievement_indicator.attr("disabled", "disabled");
  } else {
    $submit_add_achievement_indicator.removeAttr("disabled");
    $submit_edit_achievement_indicator.removeAttr("disabled");
  }
});
</script>