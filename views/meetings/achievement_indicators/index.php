{% include "meetings/achievement_indicators/_modal_achievement_indicator.php" %}
<script type="text/javascript">
  function loadAchievementIndicatorList() {
    var container = document.getElementById("achievement_indocators_container");
    var loader = loadDotsAnimationInto(container, "Memuat Indikator Pencapaian");
    $.ajax({
      url:"{{ u('achievement_indicators_table', {id:meeting.id}) }}",
      success:function(response){
        stopDotsAnimationOn(loader, container, "");
        $("#achievement_indocators_container").hide()
                                  .html(response)
                                  .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
      }
    });
  }
</script>
<h3>Indikator Pencapaian</h3>
<div class="table-responsive" id="achievement_indocators_container">
  {% include "meetings/achievement_indicators/_achievement_indicators_table.php" %}
</div>