$("form[name='meeting_form'] > .flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> data pertemuan berhasil disimpan, {{ a('Kembali','subjects#show', {id:meeting.subject_id}) }}")
           .show();
setTimeout(function(){
  $("form[name='meeting_form'] > .flash").fadeOut();
}, 10000);
