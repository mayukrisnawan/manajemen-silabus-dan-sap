{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Pertemuan ke-{{ meeting_order }}</h1>
{{ meeting.main_lesson }}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah tahapan pembelajaran baru", "meeting_meetingstages#add", {meeting_id:meeting.id}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
<table class="table table-bordered table-striped table-condensed table-hover">
  <tbody id="meeting_stage_container">
    {% if meeting_stages|length == 0 %}
    <tr>
      <td>
        <center>
          <i>Belum ada tahapan pembelajaran pada pertemuan ini</i>
        </center>
      </td>
    </tr>
    {% endif %}
    {% for meeting_stage in meeting_stages %}
    <tr class='meeting-stage-box' data-meeting-stage-id="{{ meeting_stage.id }}">
      <td>
        <div class='pull-right'>
          {{ a("Edit", "meeting_meetingstages#edit", {meeting_id:meeting.id, id:meeting_stage.id}) }}
          |
          {{ 
            a("Hapus", "meetingstages#destroy", {id:meeting_stage.id}, {method:"delete", remote:"true", 
              confirmTitle:"Hapus Tahapan Pembelajaran",
              confirmMessage:"Apakah anda yakin ingin menghapus tahapan pembelajaran berikut?",
              class:"btn_hapus"
            }) 
          }}
        </div>
        {{ meeting_stage.stage.label }}
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>
<i class='app-small-info'>*Atur urutan tahapan pembelajaran dengan drag dan drop tahapan pembelajaran</i>
<span class='pull-right' style='font-size:10px;' id="meeting_stage_orders_status"></span>
<script type="text/javascript">
var loader = null;
var meeting_stage_orders_status = document.getElementById("meeting_stage_orders_status");
function meetingStageOrdersUpdate_LoadState(){
  loader = loadDotsAnimationInto(meeting_stage_orders_status, "<i>Menyimpan urutan tahapan pembelajaran</i>");
  $(meeting_stage_orders_status).show();
}
function meetingStageOrdersUpdate_SuccessState(){
  meeting_stage_orders_status.innerHTML = "<font color='green'><i class='glyphicon glyphicon-ok'></i> Urutan tahapan pembelajaran berhasil disimpan</font>";
  $(meeting_stage_orders_status).show().fadeOut(2000); 
}
function meetingStageOrdersUpdate_ErrorState(){
  meeting_stage_orders_status.innerHTML = "<font color='red'><i class='glyphicon glyphicon-remove'></i> Terjadi kesalahan, urutan tahapan pembelajaran tidak berhasil disimpan</font>";
  $(meeting_stage_orders_status).show().fadeOut(2000);
}
function meetingStageOrdersUpdate_SilentState(){
  stopDotsAnimationOn(loader, meeting_stage_orders_status);
}

var meeting_stage_orders_xhr = null;
$("#meeting_stage_container").sortable({
  helper:jqueryUI_sortableHelper,
  update:function(event, element){
    var meeting_stage_orders = "";
    $(".meeting-stage-box").each(function(key){
      var id = $(this).attr("data-meeting-stage-id");
      if (!meeting_stage_orders == "") meeting_stage_orders += "&";
      meeting_stage_orders += "meeting_stage_orders[" +  key + "]=" + id; 
      $(this).find(".meeting-stage-index").html("Pertemuan ke-"+(key+1));
    });

    if (meeting_stage_orders_xhr) {
      meeting_stage_orders_xhr.abort();
    }
    
    meeting_stage_orders_xhr = $.ajax({
      "url":"{{ u('set_subject_meeting_stage_orders', {meeting_id:meeting.id}) }}",
      "type":"patch",
      "data":meeting_stage_orders,
      success:function(){
        meetingStageOrdersUpdate_SuccessState();
      },
      error:function(){
        meetingStageOrdersUpdate_ErrorState();        
      },
      beforeSend:function(){
        meetingStageOrdersUpdate_LoadState();
      },
      complete:function(){
        meetingStageOrdersUpdate_SilentState();
      }
    });
  }
}).disableSelection();
</script>
{% endblock %}