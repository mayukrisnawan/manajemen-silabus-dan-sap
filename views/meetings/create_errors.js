var $frm = $("form[name='meeting_form']");
{% for attr, error in errors %}
  {% set joined = "" %}
  {% for msg in error %}
    {% if joined != "" %}
      {% set joined = joined ~ ", "%}
    {% endif %}
    {% set joined = joined ~ msg %}
  {% endfor %}
var $frmGroup = $frm.find("[name='meeting[{{attr}}]']").parent();
if (!$frmGroup.hasClass("form-group")) $frmGroup = $frmGroup.parent();
$frmGroup.addClass("has-error")
        .find(".help-block")
        .html("{{ joined|capitalize }}");
{% endfor %}