{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit Pertemuan</h1>
{% set method = "patch" %}
{% set action = u("meetings#update", {id:meeting.id}) %}
<div class="row">
  <div class="col-md-15">
    {% include 'meetings/form.php' %}
  </div>
  <div class="col-md-24 col-md-offset-1">
    {% include 'meetings/_detail.php' %}
  </div>
</div>
{% endblock %}