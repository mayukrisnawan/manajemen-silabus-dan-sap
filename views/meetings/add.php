{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Tambah Pertemuan Baru</h1>
{% set method = "post" %}
{% set action = u("subject_meetings#create", {subject_id:subject.id}) %}
<div class="row">
  <div class="col-md-15">
    {% include 'meetings/form.php' %}
  </div>
</div>
{% endblock %}