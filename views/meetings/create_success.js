$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> pertemuan berhasil ditambahkan, {{ a('Kembali','subjects#show',{id:subject.id}) }}")
           .show();
$("form[name='resource_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='resource_form']").find("inpit[type='submit']")
                                  .attr("disabled", "disabled")
                                  .unbind("click");