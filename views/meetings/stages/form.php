<form role="form" name='meeting_stage_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="meeting_stage[meeting_id]" value="{{ meeting.id }}">
  <input type="hidden" name="meeting_stage[id]" value="{{ meeting_stage.id }}">
  <div class="form-group">
    <label class="control-label">Jenis Tahapan Pembelajaran</label>
    <select name="meeting_stage[stage_id]" class="form-control">
    {% for stage in stages %}
      <option value="{{ stage.id }}">{{ stage.label }}</option>
    {% endfor %}
    </select>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Aktivitas Dosen</label>
    <textarea name="meeting_stage[lecturer_act]" class="form-control"/>{{ meeting_stage.lecturer_act }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Aktivitas Mahasiswa</label>
    <textarea name="meeting_stage[student_act]" class="form-control"/>{{ meeting_stage.student_act }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Aktivitas Umum</label>
    <textarea name="meeting_stage[general_act]" class="form-control"/>{{ meeting_stage.general_act }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("meeting_stage"); ?>
var $lecturer_act = $("textarea[name='meeting_stage[lecturer_act]']"),
    $student_act = $("textarea[name='meeting_stage[student_act]']"),
    $general_act = $("textarea[name='meeting_stage[general_act]']");

$general_act.keyup(function(){
  var content = $general_act.val();
  content = content.replace(/[\n ]/g, "");
  if (content == "") {
    $lecturer_act.removeAttr("disabled");
    $student_act.removeAttr("disabled");
  } else {
    $lecturer_act.attr("disabled", "disabled");
    $student_act.attr("disabled", "disabled");
  }
});


function cekAktivitasDosenDanMahasiswa() {
  var contentDosen = $lecturer_act.val();
  contentDosen = contentDosen.replace(/[\n ]/g, "");

  var contentMahasiswa = $student_act.val();
  contentMahasiswa = contentMahasiswa.replace(/[\n ]/g, "")

  if (contentDosen == "" && contentMahasiswa == "") {
    $general_act.removeAttr("disabled");
  } else {
    $general_act.html("").attr("disabled", "disabled");
  }

}
$lecturer_act.keyup(cekAktivitasDosenDanMahasiswa);
$student_act.keyup(cekAktivitasDosenDanMahasiswa);
</script>