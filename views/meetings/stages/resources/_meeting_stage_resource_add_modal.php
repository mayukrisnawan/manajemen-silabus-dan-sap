{% set action=u('meetingstageresources#create') %}
{% set method="post" %}
{% set form_name="meeting_stage_resource_add_form" %}
<div class="modal-add-meeting-stage-resource modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title"><b>Tambah Alat atau Bahan</b></h4>
      </div>
      <div class="modal-body">
        <div class="flash"></div>
        {% include "meetings/stages/resources/form.php" %}
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-success" id="btn_submit_add_meeting_stage_resource">Tambahkan</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal">Kembali</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var $frm_add = $("form[name='{{ form_name }}']");
$("#btn_submit_add_meeting_stage_resource").click(function(){
  var btn = this;
  var timer = loadDotsAnimationInto(this, "Menyimpan");
  $(btn).attr("disabled", "disabled");
  $frm_add.ajaxSubmit({
    dataType:"script",
    success:function(){
      stopDotsAnimationOn(timer, btn, "Tambahkan");
    }
  });
  return false;
});
</script>