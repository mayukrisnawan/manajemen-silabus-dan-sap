var $frm = $("form[name='meeting_stage_resource_add_form']");
$frm.find(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> alat atau bahan berhasil ditambahkan")
           .show();
$frm.find("*[name]").attr("disabled", "disabled");
$("#btn_submit_add_meeting_stage_resource").hide();