var $frm = $("form[name='meeting_stage_resource_add_form']");
$frm.find(".flash").addClass("alert-danger")
           .removeClass("alert-success")
           .html("<strong>Terjadi kesalahan</strong>, alat atau bahan tidak berhasil ditambahkan")
           .show();
$frm.find("*[name]").attr("disabled", "disabled");
$("#btn_submit_add_meeting_stage_resource").hide();