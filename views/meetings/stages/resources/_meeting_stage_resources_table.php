{% if meeting_stage_resources|length == 0 %}
<table class="table table-bordered table-striped table-hover">
  <tbody>
    <tr>
      <td>
        <center>
          <i>Belum ada alat atau bahan yang digunakan pada tahapan pembelajaran ini, {{ a("Tambah alat atau bahan pada tahapan pembelajaran berikut", "", {}, {class:"btn-add-meeting-stage-resource"}) }}</i>
        </center>
      </td>
    </tr>
  </tbody>
</table>
{% else %}
  {% for meeting_stage_resource in meeting_stage_resources %}
    {{ loop.index }}. {{ meeting_stage_resource.resource.name }}
     {{ a("[x]", "meetingstageresources#destroy", {id:meeting_stage_resource.id}, {method:"delete", remote:"true", 
          confirmTitle:"Pembatalan Penggunaan Alat dan Bahan",
          confirmMessage:"Apakah anda yakin ingin membatalkan penggunaan alat dan bahan berikut?",
          class:"btn-delete-meeting-stage-resource"
        })
    }}
    <br/>
  {% endfor %}
  {{ a("+ Tambah alat atau bahan lain", "", {}, {class:"btn-add-meeting-stage-resource"}) }}
{% endif %}
<script type="text/javascript">
$(".btn-add-meeting-stage-resource").click(function(){
  var $frm = $("form[name='meeting_stage_resource_add_form']");
      $frm.find(".flash").removeClass("alert-success")
                         .removeClass("alert-danger")
                         .hide();
  $frm.find("*[name]").removeAttr("disabled");
  $("#btn_submit_add_meeting_stage_resource").removeAttr("disabled").show();
  $(".modal-add-meeting-stage-resource").modal('show')
  .on('hidden.bs.modal', function() {
    loadResourceList();
  });
  return false;
});
</script>