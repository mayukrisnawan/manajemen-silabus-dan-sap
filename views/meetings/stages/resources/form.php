<form role="form" name='{{ form_name }}' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="meeting_stage_resource[id]" value="">
  <input type="hidden" name="meeting_stage_resource[meeting_stage_id]" value="{{ meeting_stage.id }}">
  <div class="form-group">
    <label class="control-label">Pilih Alat atau Bahan</label>
    <select name="meeting_stage_resource[resource_id]" class="form-control">
    {% for resource in resources %}
      <option value='{{ resource.id }}'>{{ resource.name }}</option>
    {% endfor %}
    </select>
    <span class="help-block"></span>
  </div>
</form>