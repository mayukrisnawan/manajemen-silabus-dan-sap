<script type="text/javascript">
function loadResourceList() {
  var container = document.getElementById("resources_container");
  var loader = loadDotsAnimationInto(container, "Memuat Alat dan Bahan");
  $.ajax({
    url:"{{ u('meeting_stage_resources_table', {id:meeting_stage.id}) }}",
    success:function(response){
      stopDotsAnimationOn(loader, container, "");
      $(container).hide()
                  .html(response)
                  .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
    }
  });
}
</script>
<h4>Daftar Penggunaan Alat dan Bahan</h4>
{% include "meetings/stages/resources/_meeting_stage_resource_add_modal.php" %}
<div id="resources_container">
  {% include "meetings/stages/resources/_meeting_stage_resources_table.php" %}
</div>