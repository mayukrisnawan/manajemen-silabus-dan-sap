{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Tambah Tahapan Pembelajaran</h1>
<div class="row">
  <div class="col-md-20">
    {% if stages|length != 0 %}
      {% set action = u("meeting_meetingstages#create", {meeting_id:meeting.id}) %}
      {% set method = "POST" %}
      {% include 'meetings/stages/form.php' %}
    {% else %}
      Semua tahapan pembelajaran yang tersedia sudah ditambahkan,
      {{ a("kembali", "subject_meetings#show", {id:meeting.id, subject_id:subject.id}) }}
    {% endif %}
  </div>
</div>
{% endblock %}