var $frm = $("form[name='meeting_stage_form']");
{% for attr, error in errors %}
  {% set joined = "" %}
  {% for msg in error %}
    {% if joined != "" %} 
      {% set joined = joined ~ ", " %}
    {% endif %}
    {% set joined = joined ~ msg %}
  {% endfor %}
  $frm.find("[name='meeting_stage[{{attr}}]']").parent()
                                        .addClass("has-error")
                                        .find(".help-block")
                                        .html("{{ joined|capitalize }}");
{% endfor %}