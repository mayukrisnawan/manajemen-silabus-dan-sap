$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> tahapan pembelajaran berhasil ditambahkan, {{ a('Kembali','subject_meetings#show', {id:meeting_stage.meeting.id, subject_id:meeting_stage.meeting.subject.id}) }}")
           .show();
$("form[name='meeting_stage_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='meeting_stage_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");