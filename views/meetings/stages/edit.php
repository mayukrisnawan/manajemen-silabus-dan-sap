{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit Tahapan Pembelajaran</h1>
<div class="row">
  <div class="col-md-15">
    {% set action = u("meetingstages#update", {meeting_id:meeting.id, id:meeting_stage.id}) %}
    {% set method = "PATCH" %}
    {% include 'meetings/stages/form.php' %}
  </div>
  <div class="col-md-24 col-md-offset-1">
    <div style='border-left:1px solid #eee; padding-left:20px;'>
      {% include "meetings/stages/resources/index.php" %}
    </div>
  </div>
</div>
{% endblock %}