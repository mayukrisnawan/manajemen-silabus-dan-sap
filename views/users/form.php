<form role="form" name='user_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <div class="form-group">
    <label class="control-label">Username</label>
    <input name="user[username]" type="text" class="form-control" value="{{ user.username }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Password</label>
    <br>
    <a href="" class="btn btn-primary" id="btn_ganti_password">Ganti Password</a>
    <input name="user[password]" type="text" id="ganti_password" class="form-control" value="" style="display:none" />
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" id="btn_simpan" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
var $frm = $("form[name='user_form']");
$("#btn_ganti_password").click(function(){
  $( "#ganti_password").show();
  $("#btn_ganti_password").hide();
  return false;
});
$("#btn_simpan").click(function(){
  var $btn = $(this);
  var loader = loadDotsAnimationInto(this, "Menyimpan data");
  $btn.attr("disabled", "disabled");
  $frm.ajaxSubmit({
    dataType:"script",
    type:"PATCH",
    success:function(){
      stopDotsAnimationOn(loader, $btn.get(0), "Simpan");
      $btn.removeAttr("disabled");
    },
    error:function(){
      stopDotsAnimationOn(loader, $btn.get(0), "Simpan");
      $btn.removeAttr("disabled");
    }
  });
  return false;
});
</script>