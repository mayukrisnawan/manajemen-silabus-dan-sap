{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Informasi Akun</h1>
<div class="row">
  <div class="col-md-15">
    {% set method="PATCH" %}
    {% set action=u('users#update', {id:user.id}) %}
    {% include 'users/form.php' %}
  </div>
</div>
{% endblock %}