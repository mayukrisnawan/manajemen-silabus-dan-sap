<?php if (UM::isLogin()): ?>
  {% if not guest %}
    <li>{{ a("Logout", "guest#logout") }}</li>
  {% else %}
    <?php if (UM::info("level") == 0): ?>
      <li>{{ a("Dashboard", "admin#index") }}</li>
    <?php elseif (UM::info("level") == 1): ?>
      <li>{{ a("Dashboard", "dosen#index") }}</li>
    <?php endif; ?>
  {% endif %}
<?php else: ?>
  <li>{{ a("Login", "guest#login") }}</li>
<?php endif; ?>