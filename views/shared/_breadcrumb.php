{% if navigations|length > 1 %}
<ol class="breadcrumb">
  {% for navigation in navigations %}
    {% set index = loop.index %}
    {% for label,url in navigation %}
      <li {{ index == navigations|length ? "class='active'" : "" }}>
        {% if index == navigations|length or url=="" %}
          {{ label }}
        {% else %}
          <a href="{{ url }}">{{ label }}</a>
        {% endif %}
      </li>
    {% endfor %}
  {% endfor %}
</ol>
{% endif %}