<div class="app_menu">
  <div class="app_menu_icon_container">
    <a class="app_menu_icon app_menu_link glyphicon glyphicon-home" href="{{ u('admin#index') }}" title="Dashboard"></a>
    <a class="app_menu_icon app_menu_link glyphicon glyphicon-user" href="{{ u('admin#akun') }}" title="Informasi Akun"></a>
    <a class="app_menu_icon glyphicon glyphicon-book" href="#menu-akademik"></a>
    <a class="app_menu_icon glyphicon glyphicon-hdd" href="#menu-master"></a>
    <a class="app_menu_icon glyphicon glyphicon-cog" href="#menu-website"></a>
  </div>
  <ul class="app_menu_content_container"></ul>
  <ul class="app_menu_content_container"></ul>

  <ul class="app_menu_content_container" id="menu-akademik">
    <li class="app_menu_title">Akademik</li>
    <li><a href="{{ u('curriculums#index') }}">Kurikulum</a></li>
    <li><a href="{{ u('lecturers#index') }}">Dosen</a></li>
    <li><a href="{{ u('subjects#index') }}">Mata Kuliah</a></li>
  </ul>
  <ul class="app_menu_content_container" id="menu-master">
    <li class="app_menu_title">Data Pendukung</li>
        <li><a href="{{ u('stages#index') }}">Tahapan Pembelajaran</a></li>
    <li><a href="{{ u('resources#index') }}">Alat dan Bahan</a></li>
    <li><a href="{{ u('references#index') }}">Referensi</a></li>
  </ul>
  <ul class="app_menu_content_container" id="menu-website">
    <li class="app_menu_title">Pengaturan Website</li>
    <li><a href="{{ u('news#index') }}">Berita dan Pengumuman</a></li>
    <li><a href="{{ u('configs#informasi') }}">Halaman Informasi</a></li>
    <li><a href="{{ u('configs#bantuan') }}">Halaman Bantuan</a></li>
  </ul>
</div>