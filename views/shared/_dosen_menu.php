<div class="app_menu">
  <div class="app_menu_icon_container">
    <a class="app_menu_icon app_menu_link glyphicon glyphicon-home" href="{{ u('dosen#index') }}" title="Dashboard"></a>
    <a class="app_menu_icon app_menu_link glyphicon glyphicon-user" href="{{ u('dosen#akun') }}" title="Informasi Akun"></a>
    <a class="app_menu_icon glyphicon glyphicon-book" href="#menu-akademik"></a>
    <a class="app_menu_icon glyphicon glyphicon-hdd" href="#menu-master"></a>
  </div>
  <ul class="app_menu_content_container"></ul>
  <ul class="app_menu_content_container"></ul>

  <ul class="app_menu_content_container" id="menu-akademik">
    <li class="app_menu_title">Akademik</li>
    <li><a href="{{ u('dosen#kurikulum') }}">Kurikulum</a></li>
    <li><a href="{{ u('dosen#ampu_mata_kuliah') }}">Ampu Mata Kuliah</a></li>
    <li><a href="{{ u('subjects#index') }}">Mata Kuliah</a></li>
    <li><a href="{{ u('dosen#dosen') }}">Daftar Dosen</a></li>
  </ul>
  <ul class="app_menu_content_container" id="menu-master">
    <li class="app_menu_title">Data Pendukung</li>
    <li><a href="{{ u('dosen#tahapan_pembelajaran') }} ">Tahapan Pembelajaran</a></li>
    <li><a href="{{ u('dosen#alat_dan_bahan') }}">Alat dan Bahan</a></li>
    <li><a href="{{ u('dosen#referensi') }}">Referensi</a></li>
  </ul>
</div>