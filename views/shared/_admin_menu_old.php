<ul class="panel-body nav nav-stacked nav-pills">
  <li class="nav-header">
    <h4><b>Akademik</b></h4>
    <hr/>
  </li>
  <li {{ menu == 'index' ? 'class="active"' : '' }}><a href="#">Selamat Datang</a></li>
  <li {{ menu == 'kurikulum' ? 'class="active"' : '' }}><a href="#">Program Studi</a></li>
  <li {{ menu == 'dosen' ? 'class="active"' : '' }}><a href="#">Dosen</a></li>
</ul>


<ul class="panel-body nav nav-stacked nav-pills">
  <li class="nav-header">
    <h4><b>Data Master</b></h4>
    <hr/>
  </li>
  <li {{ menu == 'alat_dan_bahan' ? 'class="active"' : '' }}><a href="#">Alat dan Bahan</a></li>
  <li {{ menu == 'referensi' ? 'class="active"' : '' }}><a href="#">Referensi</a></li>
  <li {{ menu == 'referensi' ? 'class="active"' : '' }}><a href="#">Tahapan Pembelajaran</a></li>
</ul>

<ul class="panel-body nav nav-stacked nav-pills">
  <li class="nav-header">
    <h4><b>Pengaturan Website</b></h4>
    <hr/>
  </li>
  <li {{ menu == 'alat_dan_bahan' ? 'class="active"' : '' }}><a href="#">Berita dan Pengumuman</a></li>
  <li {{ menu == 'referensi' ? 'class="active"' : '' }}><a href="#">Informasi Website</a></li>
  <li {{ menu == 'referensi' ? 'class="active"' : '' }}><a href="#">Tahapan Pembelajaran</a></li>
</ul>