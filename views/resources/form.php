<form role="form" name='resource_form' action='{{ u(action, {id:resource.id}) }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="resource[id]" value="{{ resource.id }}">
  <div class="form-group">
    <label class="control-label">Nama Alat dan Bahan</label>
    <input name="resource[name]" type="text" class="form-control" value="{{ resource.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("resource"); ?>
</script>