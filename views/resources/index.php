{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Alat dan Bahan</h1>
{% if has_level('admin') %}
<hr/>
<form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah Alat dan Bahan", "resources#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped tabel-hover">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Alat/Bahan</th>
          {% if has_level('admin') %}
          <th>Pilihan</th>
          {% endif %}
        </tr>
      </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
      <tr>
        <td colspan="3">
          <center>
            <i>Belum ada daftar Alat dan Bahan</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for resource in pagination.records %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td>
        <td>
            {{ resource.name }}
        </td>
        {% if has_level('admin') %}
        <td>
          {{
            a("Edit", "resources#edit", {id:resource.id})
          }}
          |
          {{
            a("Hapus", "resources#destroy", {id:resource.id}, {method:"delete", remote:"true",
              confirmTitle:"Hapus Data",
              confirmMessage:"Apakah anda yakin ingin menghapus Data Alat dan Bahan berikut?",
              class:"btn_hapus"
            })
          }}
        </td>
        {% endif %}
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}