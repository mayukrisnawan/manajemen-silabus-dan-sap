$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> Data Alat dan Bahan berhasil ditambahkan, {{ a('Kembali','resources#index') }}")
           .show();
$("form[name='resource_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='resource_form']").find("inpit[type='submit']")
                                  .attr("disabled", "disabled")
                                  .unbind("click");