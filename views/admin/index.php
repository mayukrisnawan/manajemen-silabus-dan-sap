{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block panel_body_open %}{% endblock %}
{% block panel_body_close %}{% endblock %}

{% block body %}
<h1>Selamat Datang di Dashboard Administrator</h1>
<hr/>
Administrator bisa mengatur halaman Manajemen Silabus dan SAP. Temukan yang akan diinputkan dan diedit disini :
<ul>
  <li>
    {{ a("Daftar kurikulum", "curriculums#index") }}
  </li>
  <li>
    {{ a("Berita dan Pengumuman", "news#index") }}
  </li>
  <li>
    {{ a("Daftar dosen", "lecturers#index") }}
  </li>
</ul>
{% endblock %}