<form role="form" name='curriculum_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="curriculum[id]" value="{{ curriculum.id }}">
  <div class="form-group">
    <label class="control-label">Nama Kurikulum</label>
    <input name="curriculum[name]" type="text" class="form-control" value="{{ curriculum.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("curriculum"); ?>
</script>