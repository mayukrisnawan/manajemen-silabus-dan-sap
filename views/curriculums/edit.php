{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit Kurikulum >> {{ curriculum.name }}</h1>
<div class="row">
  <div class="col-md-15">
    {% include 'curriculums/form.php' %}
  </div>
</div>
{% endblock %}