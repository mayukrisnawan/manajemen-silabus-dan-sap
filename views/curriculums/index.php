{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Daftar Kurikulum</h1>
{% if has_level('admin') %}
<hr/>
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah kurikulum baru", "curriculums#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        {% if has_level('admin') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
      <tr>
        <td colspan="3">
          <center>
            <i>Belum ada kurikulum yang ditambahkan</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for curriculum in pagination.records %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td>
        <td>
            {{ a(curriculum.name, "curriculum_programs#index", {curriculum_id:curriculum.id}) }}
        </td>

        {% if has_level('admin') %}
        <td>
          {{ 
            a("Edit", "curriculums#edit", {id:curriculum.id}) 
          }}
          |
          {{
            a("Hapus", "curriculums#destroy", {id:curriculum.id}, {method:"delete", remote:"true", 
              confirmTitle:"Kurikulum",
              confirmMessage:"Apakah anda yakin ingin menghapus kurikulum berikut?",
              class:"btn_hapus"
            }) 
          }}
        </td>
        {% endif %}

      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}