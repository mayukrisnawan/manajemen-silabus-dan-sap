$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> kurikulum berhasil ditambahkan, {{ a('Kembali','curriculums#index') }}")
           .show();
$("form[name='curriculum_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='curriculum_form']").find("input[type='submit']")
                                 .attr("disabled", "disabled")
                                 .unbind("click");