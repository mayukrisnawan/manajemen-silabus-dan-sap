{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit Referensi >> {{ reference.content }} </h1>
<div class = "row">
	<div class = "col-md-20">
		{% include 'references/form.php' %}
	</div>
</div>
{% endblock %}