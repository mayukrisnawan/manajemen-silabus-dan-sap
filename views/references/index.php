{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<script type="text/javascript">
var xhrReferensi = null;
function searchReferensi(page){
  if (!page) page = 1;
  var referensi = $("#search_referensi").val();
  if (xhrReferensi) {
    xhrReferensi.abort();
    xhrReferensi = null;
  }
  xhrReferensi = $.ajax({
    url:"{{ u('references#search') }}",
    data:"referensi="+referensi+"&_page="+page,
    success:function(response){
      $("#references_container").html(response)
                               .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
    },
    beforeSend:function(){
      $("#references_container").html("<center>Memuat data&nbsp;{{ img('loading.gif') }}</center>");
    }
  });
}
</script>
<h1>Referensi</h1>
<hr/>
<form class = "form-inline" style = "margin : 10px auto">
  {% if has_level('admin') %}
	<div class = "form-group">
		{{ a("<span class = 'glyphicon glyphicon-plus'></span> Tambah referensi baru", "references#add", {}, {class:"btn btn-success form-control"}) }}
	</div>
  {% endif %}
	<div class="form-group col-md-20 pull-right" style="padding:0px;">
    <div class="input-group">
      <input id="search_referensi" type="text" class="form-control" placeholder="Cari referensi.."/>
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-search"></i>
      </span>
    </div>
  </div>
</form>
<div class="table-responsive" id="references_container">
	{% include "references/_references_table.php" %}
</div>
{{ pagination.links|raw}}
<script type="text/javascript">
$("#search_referensi").keyup(searchReferensi);
</script>
{% endblock %}
	
