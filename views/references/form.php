<form role ="form" name='reference_form' action='{{ u(action, {id:reference.id}) }}' method = '{{ method }}'>
	<div class="flash alert" style="display:none"></div>
	<input type = "hidden" name="reference[id]" value="{{ reference.id }}">
	<div class = "form-group">
		<label class = "control-label"> Nama Referensi </label>
		<textarea name ="reference[content]" type="text" class="form-control">{{ reference.content}}</textarea>
		<span class = "help-block"></span>
	</div>
	<div class = "form-group">
		<input type = "submit" class ="btn btn-success" value = "Simpan">
	</div>
</form>
<script type = "text/javascript">
	<?php jQueryValidate("reference"); ?>
</script>
