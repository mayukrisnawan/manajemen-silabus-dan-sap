<table class = "table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      {% if has_level('admin') %}
        <th>Pilihan</th>
      {% endif %}
    </tr>
  </thead>
  <tbody>
    {% if pagination.records|length == 0 %}
    <tr>
      <td colspan="3">
        <center>
          <i>Belum ada referensi<i>
        </center>
      </td>
    </tr>
    {% endif %}
    {% for reference in pagination.records %}
    <tr>
      <td>{{ loop.index + pagination.offset }}</td>
      <td>
          {{ reference.content}}
      </td>
      {% if has_level('admin') %}
      <td>
        {{
          a("Edit", "references#edit", {id:reference.id})
        }}
        |
        {{
          a("Hapus", "references#destroy", {id:reference.id}, {method:"delete", remote:"true",
            confirmTitle:"Referensi",
            confirmMessage:"Apakah anda yakin ingin menghapus referensi berikut?",
            class:"btn_hapus"
          })
        }}
      </td>
      {% endif %}

    </tr>
  {% endfor %}
  </tbody>
</table>
<script type="text/javascript">
  $(".pagination a").click(function(){
    var page = $(this).attr("href");
    page = page.replace("?_page=", "");
    searchReferensi(page);
    return false;
  });
</script>