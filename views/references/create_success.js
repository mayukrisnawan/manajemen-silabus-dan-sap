$(".flash").addClass("alert-success")
					 .removeClass("alert-danger")
					 .html("<strong>Selamat!</strong> referensi berhasil ditambahkan, {{ a('Kembali', 'references#index') }}")
					 .show();
$("form[name='reference_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='reference_form']").find("input[type='submit']")
																		.attr("disabled", "disabled")
																		.unbind("click");