{% extends "layouts/guest_layout.php" %}

{% block body %}
<div class="panel-heading" style="border-bottom:solid 1px #f9f9f9">
  <h3>{{ tb.title }}</h3>
    <h6>{{ tb.last_updated.format("d M Y, H:i:s") }}</h6>
</div>
<div class="panel-body">
  <?php echo html_entity_decode("{{ tb.content }}"); ?>
  <hr/>
  <a href="{{ u('guest#berita') }}" style='float:right'>Berita Lainnya &gt;&gt;</a>
</div>
{% endblock %}