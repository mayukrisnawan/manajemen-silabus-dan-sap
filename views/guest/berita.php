{% extends "layouts/guest_layout.php" %}

{% block body %}
<div class="row">
  <div class="col-md-38">
    <h1><b>Berita dan Pengumuman</b></h1>
    <hr/>

    <div class="panel col-md-40">
    {% for news in pagination.records %}
      <div class="panel-heading" style="border-bottom:solid 1px #f9f9f9">
        <h3>{{ news.title }}</h3>
        <h6>{{ news.last_updated.format("d M Y, H:i:s") }}</h6>
      </div>
      <div class="panel-body">
        <hr/>
        <a href="{{ u('tampilkan_berita', {id:news.id}) }}" style='float:right'>Tampilkan Berita &gt;&gt;</a>
      </div>
      {% endfor %}
    </div>
  </div>
</div>
{{ pagination.links|raw }}
{% endblock %}