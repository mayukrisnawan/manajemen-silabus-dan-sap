{% extends "layouts/guest_layout.php" %}

{% block body %}
<div class="row" style="margin: 50px auto;">
  <div class="col-md-26">
    <h1><b>Halaman Utama</b></h1>
    <blockquote><i>"Selamat datang di Sistem Manajemen Silabus dan SAP (Satuan Acara Perkuliahan)"</i></blockquote>
    <p style="font-size:16px; text-indent:28px; text-align:justify">
      Sistem ini memiliki fitur-fitur yang mendukung dalam penyusunan silabus
      pada suatu mata kuliah dalam beberapa program studi. Silabus yang memiliki
      SAP (Satuan Acara Perkuliahan) dapat diolah dengan fitur yang sudah tersedia
      dalam sistem ini.
      <br/>
      <br/>
      Temukan yang bisa anda cari disini :
    </p>
    <ul>
      <li>
        {{ a("Lihat Daftar Mata Kuliah","subjects#index") }}
      </li>
      <li>
        {{ a("Info Selengkapnya","guest#info") }}
      </li>
    </ul>
  </div>
  <div class="panel col-md-13 col-md-offset-1">
    <div class="panel-heading" style="border-bottom:solid 1px #f9f9f9">
      <h3>Berita dan Pengumuman Terbaru</h3>
    </div>
    <div class="panel-body">
      <ul>
        {% for news in pagination.records %}
        <li>
          {{ a(news.title, "tampilkan_berita", {id:news.id}) }}
        </li>
        {% endfor %}
      </ul>
      <hr/>
      <a href="{{ u('guest#berita') }}" style='float:right'>Selengkapnya &gt;&gt;</a>
    </div>
  </div>
</div>
{% endblock %}