{% extends "layouts/guest_layout.php" %}

{% block body %}
<div class="panel col-md-14 col-md-offset-13" style="position : relative; top:100px;">
  <div class="panel-heading" style="border-bottom:solid 1px #f8f8f8">
    <h1 class="text-center">Login</h1>
  </div>
  <div class="panel-body">
    {% include "guest/_form_login.php" %}
  </div>
</div>
<script type="text/javascript">
  $("input[name='user[username]']").focus();
</script>
{% endblock %}