var $frm = $("form[name='user_form']");
$frm.find(".flash").addClass("alert-warning")
                   .html("Login tidak berhasil, pasangan username dan password tidak sesuai")
                   .show()
                   .delay(4000)
                   .fadeOut(1000);
$frm.find("[name='user[username]']").focus();