{% extends "layouts/guest_layout.php" %}

{% block body %}
<div class="row">
  <div class="col-md-38">
    <h1><b>Info Selengkapnya</b></h1>
    <hr/>
    <?php echo html_entity_decode("{{ informasi.value }}"); ?>
  </div>
</div>
{% endblock %}