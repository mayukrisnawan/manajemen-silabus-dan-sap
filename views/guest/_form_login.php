<form role="form" name="user_form" class="form-horizontal" action='{{ u("guest#auth") }}' method="POST">
  <div class="flash alert" style="display:none"></div>
  <div class="form-group">
    <label class="col-sm-9 control-label">Username</label>
    <div class="col-sm-31">
      <input type="text" name="user[username]" class="form-control" placeholder="Username">
      <span class="help-block"></span>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-9 control-label">Password</label>
    <div class="col-sm-31">
      <input type="password" name="user[password]" class="form-control" placeholder="Password">
      <span class="help-block"></span>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-9 col-sm-31">
      <input type="submit" class="btn btn-default" value="Sign in"/>
    </div>
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("user"); ?>
</script>