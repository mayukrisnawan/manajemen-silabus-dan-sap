$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> Data berhasil ditambahkan, {{ a('Kembali','news#index') }}")
           .show();
$("form[name='news_form']").find("*[name]").attr("disabled", "disabled");
$("form[name='news_form']").find("input[type='submit']")
                                  .attr("disabled", "disabled")
                                  .unbind("click");