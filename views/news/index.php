{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}
 
{% block body %}
<h1>Berita dan Pengumuman</h1>
<hr/>
<form class="form-inline" style="margin : 10px auto">
    <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah Baru", "news#add", {}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
<div class="table-responsive">
  <table class="table table-bordered table-striped tabel-hover">
      <thead>
        <tr>
          <th style="vertical-align:middle">No</th>
          <th style="vertical-align:middle">Judul</th>
          <th style="vertical-align:middle" width="20%">Pembaharuan Terakhir</th>
          <th style="vertical-align:middle" width="10%">Pilihan</th>
        </tr>
      </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
      <tr>
        <td colspan="5">
          <center>
            <i>Belum ada daftar Berita dan Pengumuman</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for news in pagination.records %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td>
        <td>
            {{ news.title }}
        </td>
        <td>
            {{ news.last_updated.format("d M Y, H:i:s") }}
        </td>
        <td>
          {{
            a("Edit", "news#edit", {id:news.id})
          }}
          |
          {{
            a("Hapus", "news#destroy", {id:news.id}, {method:"delete", remote:"true",
              confirmTitle:"Hapus Data",
              confirmMessage:"Apakah anda yakin ingin menghapus Data ini?",
              class:"btn_hapus"
            })
          }}
        </td>
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}