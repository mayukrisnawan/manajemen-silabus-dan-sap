<form role="form" name='news_form' action='{{ u(action, {id:news.id}) }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="news[id]" value="{{ news.id }}">
  <div class="form-group">
    <label class="control-label">Judul</label>
    <input name="news[title]" type="text" class="form-control" value="{{ news.title }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Isi Berita</label>
    <textarea name="news[content]" id="news_container" class="form-control">{{ news.content }}</textarea>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <a class="btn btn-success" id="btn_submit_berita">Simpan</a>
  </div>
</form>
<script type="text/javascript">
var $frm = $("form[name='news_form']"),
    action = $frm.attr("action"),
    method = $frm.attr("method");
var $editor = $("textarea[name='news[content]']");
$editor.summernote({
  height:300
});
$("#btn_submit_berita").click(function(){
  var btn = this;
  var content = $editor.code();
  var timer = loadDotsAnimationInto(btn, "Menyimpan");
  var title = $("input[name='news[title]']").val();
  $(btn).attr("disabled", "disabled");
  $.ajax({
    url:action,
    method:method,
    dataType:"script",
    data:"news[content]="+encodeURIComponent(content)+"&news[title]="+title,
    complete:function(){
      $(btn).removeAttr("disabled");
      stopDotsAnimationOn(timer, btn, "Simpan");
    }
  });
  return false;
});
</script>