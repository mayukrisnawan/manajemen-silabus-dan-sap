{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Tambah Berita atau Pengumuman</h1>
<div class="row">
  <div class="col-md-30">
    {% include 'news/form.php' %}
  </div>
</div>
{% endblock %}