{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Tambah Mata Kuliah</h1>
{% set subject_type = "" %}
{% set method = "post" %}
{% set action = u("subjects#create") %}
{% set type = -1 %}
<div class="col-md-15">
  {% include "subjects/form.php" %}
</div>
{% endblock %}