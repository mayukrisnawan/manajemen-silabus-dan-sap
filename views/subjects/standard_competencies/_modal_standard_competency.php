<div class="modal-standard-competency modal">
  <div class="modal-dialog" style='width:60%'>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title"><b id="standard_competency_modal_title"></b></h4>
      </div>
      <div class="modal-body">
        {% include "subjects/standard_competencies/form.php" %}
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-success" id="btn_submit_add_standard_competency" disabled>Simpan</a>
        <a type="button" class="btn btn-success" id="btn_submit_edit_standard_competency" disabled>Simpan</a>
        <a type="button" class="btn btn-primary" data-dismiss="modal">Kembali</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var $submit_add_standard_competency = $("#btn_submit_add_standard_competency"),
    $submit_edit_standard_competency = $("#btn_submit_edit_standard_competency"),
    $frm_standard_competency = $("form[name='standard_competency_form']"),
    $flash_standard_competency = $frm_standard_competency.find(".flash"),
    $standard_competency_modal_title = $("#standard_competency_modal_title"),
    $standard_competency_form_identity = $frm_standard_competency.find("input[name='standard_competency[id]']");

{% include "subjects/standard_competencies/_action_add.js" %}
{% include "subjects/standard_competencies/_action_edit.js" %}

function restoreStandardCompetencyFormState() {
  $("form[name='standard_competency_form']").find(".form-control").removeAttr("disabled");
  $flash_standard_competency.html("").hide();
  $frm_standard_competency.find(".form-control").val("");
}

function showStandardCompetencyError(msg) {
  msg += "<span class='close'>x</span>"
  $flash_standard_competency.removeClass("alert-success").addClass("alert-danger");
  $flash_standard_competency.html(msg).slideDown();
  $flash_standard_competency.find(".close").click(function(){
    $flash_standard_competency.slideUp();
  });
}

function showStandardCompetencySuccess(msg) {
  msg += "<span class='close'>x</span>"
  $flash_standard_competency.removeClass("alert-danger").addClass("alert-success");
  $flash_standard_competency.html(msg).slideDown();
  $flash_standard_competency.find(".close").click(function(){
    $flash_standard_competency.slideUp();
  });
}

$("[name='standard_competency[content]']").keyup(function(){
  var content = $(this).val();
  content = content.replace(/[ \n]/g, "");
  if (content == "") {
    $submit_add_standard_competency.attr("disabled", "disabled");
    $submit_edit_standard_competency.attr("disabled", "disabled");
  } else {
    $submit_add_standard_competency.removeAttr("disabled");
    $submit_edit_standard_competency.removeAttr("disabled");
  }
});
</script>