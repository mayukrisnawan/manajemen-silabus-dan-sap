function linkEditStandardCompetencyModalTo($element, callback) {
  $element.each(function(){
    $(this).click(function(){
      var id = $(this).attr("href");
      restoreStandardCompetencyFormState();
      $standard_competency_form_identity.val(id);
      var content = $("#standard_competency_content_"+id).html();
      $frm_standard_competency.find("[name='standard_competency[content]']").val(content);
      $submit_edit_standard_competency.removeAttr("disabled").show();
      $submit_add_standard_competency.hide();
      $frm_standard_competency.get(0).method = "patch";
      var url = "{{ u('standardcompetencies#update', {id:''}) }}";
      url = url.substr(0, url.length-1);
      url = url + id;
      $frm_standard_competency.attr("action", url);
      $standard_competency_modal_title.html("Edit Standar Kompetensi");
      var modal = $(".modal-standard-competency").modal("show");
      modal.on("hidden.bs.modal", function(){
        if (callback) callback();
      });
      return false;
    });
  });
}

$submit_edit_standard_competency.click(function(){
  $submit_edit_standard_competency.attr("disabled", "disabled");
  var loader = loadDotsAnimationInto($submit_edit_standard_competency.get(0), "Menyimpan data");
  $.ajax({
    url:$frm_standard_competency.attr("action"),
    method:"patch",
    data:$frm_standard_competency.formSerialize(),
    dataType:'script',
    success:function(){
      stopDotsAnimationOn(loader, $submit_edit_standard_competency.get(0), "Simpan");
      $submit_edit_standard_competency.removeAttr("disabled");
    },
    error:function(){
      stopDotsAnimationOn(loader, $submit_edit_standard_competency.get(0), "Simpan");
      $submit_edit_standard_competency.removeAttr("disabled");
      showStandardCompetencyError("Terjadi kesalahan, standar kompetensi tidak berhasil ditambahkan");
    }
  });
  return false;
});