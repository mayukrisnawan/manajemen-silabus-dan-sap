{% set joined = "" %}
{% for attr, error in errors %}
  {% for msg in error %}
    {% if joined != "" %} 
      {% set joined = joined ~ ", " %}
    {% endif %}
    {% set joined = joined ~ msg %}
  {% endfor %}
{% endfor %}
showStandardCompetencyError("{{ joined }}");