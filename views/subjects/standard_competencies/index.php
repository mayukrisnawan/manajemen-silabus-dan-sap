{% include "subjects/standard_competencies/_modal_standard_competency.php" %}
<script type="text/javascript">
  function loadStandardCompetencyList() {
    var container = document.getElementById("standard_competencies_container");
    var loader = loadDotsAnimationInto(container, "Memuat Standar Kompetensi");
    $.ajax({
      url:"{{ u('standard_competencies_table', {id:subject.id}) }}",
      success:function(response){
        stopDotsAnimationOn(loader, container, "");
        $("#standard_competencies_container").hide()
                                  .html(response)
                                  .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
      }
    });
  }
</script>
<h3>Standar Kompetensi</h3>
<div class="table-responsive" id="standard_competencies_container">
  {% include "subjects/standard_competencies/_standard_competencies_table.php" %}
</div>