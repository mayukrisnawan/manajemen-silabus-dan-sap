function linkAddStandardCompetencyModalTo($element, callback) {
  $element.click(function(){
    restoreStandardCompetencyFormState();
    $standard_competency_form_identity.val("");
    $submit_add_standard_competency.attr("disabled", "disabled").show();
    $submit_edit_standard_competency.hide();
    $frm_standard_competency.get(0).method = "post";
    $frm_standard_competency.attr("action", "{{ u('standardcompetencies#create') }}");
    $standard_competency_modal_title.html("Tambah Standar Kompetensi");
    var modal = $(".modal-standard-competency").modal("show");
    modal.on("hidden.bs.modal", function(){
      if (callback) callback();
    });
    return false;
  });
}

$submit_add_standard_competency.click(function(){
  $submit_add_standard_competency.attr("disabled", "disabled");
  var loader = loadDotsAnimationInto($submit_add_standard_competency.get(0), "Menyimpan data");
  $frm_standard_competency.ajaxSubmit({
    dataType:'script',
    success:function(){
      stopDotsAnimationOn(loader, $submit_add_standard_competency.get(0), "Simpan");
      $submit_add_standard_competency.removeAttr("disabled");
    },
    error:function(){
      stopDotsAnimationOn(loader, $submit_add_standard_competency.get(0), "Simpan");
      $submit_add_standard_competency.removeAttr("disabled");
      showStandardCompetencyError("Terjadi kesalahan, standar kompetensi tidak berhasil ditambahkan");
    }
  });
});