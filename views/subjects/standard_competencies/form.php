<form role="form" name='standard_competency_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="standard_competency[subject_id]" value="{{ subject.id }}">
  <input type="hidden" name="standard_competency[id]" value="">
  <div class="form-group">
    <label class="control-label">Standar Kompetensi</label>
    <textarea name="standard_competency[content]" class="form-control"></textarea>
    <span class="help-block"></span>
  </div>
</form>