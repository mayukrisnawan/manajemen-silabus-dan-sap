<table class="table table-bordered table-striped table-hover">
  <tbody>
    {% for standard_competency in standard_competencies %}
    <tr>
      <td width="5%">{{ loop.index }}</td>
      <td id='standard_competency_content_{{ standard_competency.id }}'>{{ standard_competency.content }}</td>
      <td width="18%">
        {{
          a("Edit", standard_competency.id, {}, {class:"btn-edit-standard-competency"})
        }}
        |
        {{ a("Hapus", "standardcompetencies#destroy", {id:standard_competency.id}, {method:"delete", remote:"true", 
              confirmTitle:"Hapus Standar Kompetensi",
              confirmMessage:"Apakah anda yakin ingin menghapus standar kompetensi berikut?",
              class:"btn_hapus"
            })
        }}
      </td>
    </tr>
    {% endfor %}
    {% if standard_competencies|length == 0 %}
    <tr>
      <td>
        <center>
          <i>Belum ada standar kompetensi pada pertemuan ini, {{ a("Tambah standar kompetensi", meeting.id, {}, {class:"btn-add-standard-competency"}) }}</i>
        </center>
      </td>
    </tr>
    {% else %}
    <tr>
      <td colspan="3" align="right">
        {{ a("+ Tambahkan standar kompetensi Lagi", meeting.id, {}, {class:"btn-add-standard-competency"}) }}
      </td>
    </tr>
    {% endif %}
  </tbody>
</table>
<script type="text/javascript">
linkAddStandardCompetencyModalTo($(".btn-add-standard-competency"), function(){
  loadStandardCompetencyList();
});
linkEditStandardCompetencyModalTo($(".btn-edit-standard-competency"), function(){
  loadStandardCompetencyList();
});
</script>