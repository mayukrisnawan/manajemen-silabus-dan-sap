var $frm = $("form[name='subject_form']");
{% for attr, error in errors %}
  {% set joined = "" %}
  {% for msg in error %}
    {% if joined != "" %} 
      {% set joined = joined ~ ", " %}
    {% endif %}
    {% set joined = joined ~ msg %}
  {% endfor %}
  var frmGroup = $frm.find("[name='subject[{{attr}}]']").parent();
  if ("{{ attr }}" == 'lecture_name' || "{{ attr }}" == 'semester') frmGroup = frmGroup.parent();
  frmGroup.addClass("has-error")
          .find(".help-block")
          .html("{{ joined|capitalize }}");
{% endfor %}