<table class="table table-bordered table-striped tabel-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Kode</th>
      <th>Nama Mata Kuliah</th>
      <th>Semester Mata Kuliah</th>
      <th>Jumlah SKS</th>
      {% if not program_defined %}
        <th>Kurikulum/Program Studi</th>
      {% endif %}
      <th>Jenis Mata Kuliah</th>
      <th>Dosen Pengampu</th>
      <th>Pilihan</th>
    </tr>
    <thead>
  <tbody>
    {% if pagination.records|length == 0 %}
    <tr>
      <td colspan="9">
        <center>
          <i>Belum ada daftar Mata Kuliah</i>
        </center>
      </td>
    </tr>
    {% endif %}
    {% for matkul in pagination.records %}
    <tr>
      <td>{{loop.index + pagination.offset}}</td>
      <td>{{ matkul.code }}</td>
      {% if has_level("admin") %}
        <td>{{ a(matkul.name, "subjects#show", {id:matkul.id}) }}</td>
      {% else %}
        <td>{{ matkul.name }}</td>
      {% endif %}
      <td>{{ matkul.type == 2 ? "-" : matkul.semester}}</td>
      <td>{{ matkul.credit_units}}</td>
      {% if not program_defined %}
        <td>{{ matkul.type == 0 ? matkul.curriculum.name :  matkul.program.name }}</td>
      {% endif %}
      <td>
        {% if matkul.type == 0 %}
          Umum
        {% elseif matkul.type == 1 %}
          Wajib
        {% elseif matkul.type == 2 %}
          Pilihan
        {% endif %}
      </td>
      <td>
        {% if matkul.lecturer %}
          {{ matkul.lecturer.name }}
        {% else %}
          <i>Dosen Pengampu Belum Dipilih</i>
        {% endif %}
      </td>
      <td>
      {% if has_level("admin") %}
        {{ 
          a("Edit", "matkul_edit", {id:matkul.id, program_id:matkul.parent_id}) 
        }}
        |
        {{ 
          a("Hapus", "subjects#destroy", {id:matkul.id}, {method:"delete", remote:"true", 
            confirmTitle:"Mata Kuliah",
            confirmMessage:"Apakah anda yakin ingin menghapus mata kuliah berikut?",
            class:"btn_hapus"
          }) 
        }}
        <br/>
      {% endif %} 
        {{ 
          a("<i class='glyphicon glyphicon-download'></i>&nbsp;Download Silabus", "download_silabus", {id:matkul.id}) 
        }}
        <br/>
        {{ 
          a("<i class='glyphicon glyphicon-download'></i>&nbsp;Download SAP", "download_sap", {id:matkul.id}) 
        }}
      </td>
    </tr>
  {% endfor %}
  </tbody>
</table>
{{ pagination.links|raw}}
<script type="text/javascript">
  $(".pagination a").click(function(){
    var page = $(this).attr("href");
    page = page.replace("?_page=", "");
    searchMatkul(page);
    return false;
  });
</script>