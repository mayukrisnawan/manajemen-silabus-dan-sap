{% extends current_user_level() == "guest" ? "layouts/guest_layout.php" : current_user_level() == "admin" ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}
{% block body %}
<script type="text/javascript">
var xhrMatkul = null;
function searchMatkul(page){
  if (!page) page = 1;
  var curriculum_id = $("#filter_curriculum").val(),
      program_id = $("#filter_program").val(),
      subject_type_id = $("#filter_subject_type").val(),
      semester = $("#filter_semester").val(),
      subject_name = $("#search_by_subject_name").val();

  var data = "";
  data += "curriculum_id=" + curriculum_id;
  data += "&program_id=" + program_id;
  data += "&subject_type_id=" + subject_type_id;
  data += "&semester=" + semester,
  data += "&subject_name=" + subject_name;

  if (xhrMatkul) {
    xhrMatkul.abort();
    xhrMatkul = null;
  }
  xhrMatkul = $.ajax({
    url:"{{ u('subjects#search') }}",
    data:data+"&_page="+page,
    success:function(response){
      $("#subjects_container").html(response)
                              .show("slide", { direction: "right", easing: "easeInCirc" }, 500);
    },
    beforeSend:function(){
      $("#subjects_container").html("<center>Memuat data&nbsp;{{ img('loading.gif') }}</center>");
    }
  });
}
</script>
<h1>Daftar Mata Kuliah</h1>
{% include "subjects/_filter.php" %}
<div class="table-responsive" id="subjects_container">
  {% include "subjects/_subjects_table.php" %}
</div>
{% endblock %}