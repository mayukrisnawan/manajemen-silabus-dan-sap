<h1>{{ subject.name }}</h1>
<hr/>
<table>
  <tr>
    <td><b>Program Studi</b></td>
    <td><b>: {{ subject.type == 0 ? "" : subject.program.name }}</b></td>
  </tr>
  <tr>
    <td><b>Mata Kuliah (MK)</b></td>
    <td><b>: {{ subject.name }}</b></td>
  </tr>
  <tr>
    <td><b>Kode MK</b></td>
    <td><b>: {{ subject.code }}</b></td>
  </tr>
  <tr>
    <td><b>Nama Dosen</b></td>
    <td><b>: {{ subject.lecturer ? subject.lecturer.name : "-" }}</b></td>
  </tr>
  <tr>
    <td><b>Semester</b></td>
    <td><b>: {{ subject.type == 2 ? "-" : subject.semester }}</b></td>
  </tr>
  <tr>
    <td><b>SKS</b></td>
    <td><b>: {{ subject.credit_units }}</b></td>
  </tr>
  <tr>
    <td style='word-wrap:break-word; vertical-align:top'><b>Standar Kompetensi</b></td>
    <td style='width:10%; word-wrap:break-word; vertical-align:top'>
      :<br/>
      {% for standard_competency in subject.standard_competencies %}
      - {{ standard_competency.content }}<br/>
      {% endfor %}
    </td>
  </tr>
</table>

<style type="text/css">
.table-bordered td {
  border-collapse: collapse;
  border:solid 1px #000;
  padding: 5px;
}  
</style>
<br/>
<table class='table-bordered' cellspacing="0">
  <tr>
    <td rowspan="2">No</td>
    <td rowspan="2">Komptensi Dasar</td>
    <td rowspan="2">Materi Pokok</td>
    <td rowspan="2">Pengalaman Belajar</td>
    <td rowspan="2">Indikator Pencapaian</td>
    <td colspan="3">Penilaian</td>
    <td colspan="3">Alokasi Waktu</td>
    <td rowspan="2">Sumber/Alat/Bahan</td>
  </tr>
  <tr>
    <td>T</td>
    <td>UK</td>
    <td>US</td>
    <td>TM</td>
    <td>P</td>
    <td>L</td>
  </tr>
  {% for meeting in subject.meetings %}
  <tr>
    <td style='vertical-align:top'>{{ loop.index }}</td>
    <td style='width:20%; word-wrap:break-word; vertical-align:top'>{{ meeting.basic_competency }}</td>
    <td style='width:10%; word-wrap:break-word; vertical-align:top'>{{ meeting.main_lesson }}</td>
    <td style='width:10%; word-wrap:break-word; vertical-align:top'>{{ meeting.learning_experiences }}</td>
    <td style='width:20%; word-wrap:break-word; vertical-align:top'>
      <ul style="margin:-15px">
      {% for achievement_indicator in meeting.achievement_indicators %}
        <li>{{ achievement_indicator.content }}</li>
      {% endfor %}
      </ul>
    </td>
    <td style='vertical-align:top'>{{ meeting.t == 1 ? 'x' : '' }}</td>
    <td style='vertical-align:top'>{{ meeting.uk == 1 ? 'x' : '' }}</td>
    <td style='vertical-align:top'>{{ meeting.us == 1 ? 'x' : '' }}</td>
    <td style='vertical-align:top'>{{ meeting.tm }}</td>
    <td style='vertical-align:top'>{{ meeting.p }}</td>
    <td style='vertical-align:top'>{{ meeting.l }}</td>
    <td style='width:10%; word-wrap:break-word; vertical-align:top'>
      {% set index = 1 %}
      {% for meeting_stage in meeting.meeting_stages %}
        {% for resource in meeting_stage.resources %}
          {{ index }}. {{ resource.name }}<br/>
          {% set index = index +1 %}
        {% endfor %}
      {% endfor %}
    </td>
  </tr>
  {% endfor %}
</table>

{% if references|length != 0 %}
  <page>
  <h4>Pustaka</h4>
  <hr/>
  {% for reference in references %}
    {{ loop.index }}. {{ reference.content }}<br/>
  {% endfor %}
  </page>
{% endif %}