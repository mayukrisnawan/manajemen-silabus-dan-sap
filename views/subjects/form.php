<form role="form" name='subject_form' action='{{ action }}' method='{{ method }}'>
  <div class="flash alert" style="display:none"></div>
  <input type="hidden" name="subject[id]" value="{{ subject.id }}">
  {% if subject_type == "umum" %}
    <input type="hidden" name="subject[parent_id]" value="{{ curriculum.id }}">
  {% else %}
    <input type="hidden" name="subject[parent_id]" value="{{ program.id }}">
  {% endif %}
  {% if type == -1 %}
  <div class="form-group">
    <label class="control-label">Jenis Mata Kuliah</label>
    <select name="subject[type]" class="form-control app-small" id="subject_type">
      <option value="1" {{ subject.type == 1 ? "selected" : "" }}>Wajib</option>
      <option value="2" {{ subject.type == 2 ? "selected" : "" }}>Pilihan</option>
    </select>
  </div>
  {% else %}
    <input type="hidden" name="subject[type]" value="{{ type }}">
  {% endif %}
  <div class="form-group">
    <label class="control-label">Dosen Pengampu</label>
    <div class="input-group">
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-search"></i>
      </span>
      <input id="lecturer_name" autocomplete="off" name="subject[lecturer_name]" type="text" class="form-control app-medium" value="{{ subject.lecturer.name }}" data-provide="typeahead"/>
    </div>
    <span class="help-block"></span>
  </div>

  <div class="form-group">
    <label class="control-label">Nama Mata Kuliah</label>
    <input name="subject[name]" type="text" class="form-control" value="{{ subject.name }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Kode Mata Kuliah</label>
    <input name="subject[code]" type="text" class="form-control app-medium" value="{{ subject.code }}"/>
    <span class="help-block"></span>
  </div>
  <div class="form-group" id="semester_container" {{ subject.type == 2 ? "style='display:none'" : "" }}>
    <label class="control-label">Semester Mata Kuliah</label>
    <div class="input-group app-medium">
      <span class="input-group-addon">Semester ke </span>
      <input style="width:40px;" name="subject[semester]" type="text" class="form-control" value="{{ subject.semester }}"/>
    </div>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <label class="control-label">Jumlah SKS</label>
    <div class="input-group" style="width:80px;">
      <input style="width:40px;" name="subject[credit_units]" type="text" class="form-control app-input-number" value="{{ subject.credit_units }}"/>
      <span style="width:40px;" class="input-group-addon">SKS</span>
    </div>
    <span class="help-block"></span>
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-success" value="Simpan">
  </div>
</form>
<script type="text/javascript">
  <?php jQueryValidate("subject"); ?>

  // Pemilihan Dosen
  $("#lecturer_name").typeahead({
    source: function (query, typeahead) {
      return $.getJSON("{{ u('lecturers#json') }}", { name: query }, function (data) {
        var lecturer_names = [];
        for (var index in data) {
          lecturer_names.push(data[index].name);
        }
        return typeahead(lecturer_names);
      });
    }
  });

  // Jenis Mata Kuliah
  $("#subject_type").change(function(){
    if ($(this).val() == 2) {
      $("#semester_container").hide();
    } else {
      $("#semester_container").show();
    }
  });
</script>