{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit Mata Kuliah</h1>
{% set subject_type = "" %}
{% set method = "patch" %}
{% set action = u("subjects#update", {id:subject.id}) %}
{% set type = -1 %}
<div class="row">
  <div class="col-md-15">
    {% include 'subjects/form.php' %}
  </div>
  <div class="col-md-24 col-md-offset-1">
    {% include 'subjects/_detail.php' %}
  </div>
</div>
{% endblock %}