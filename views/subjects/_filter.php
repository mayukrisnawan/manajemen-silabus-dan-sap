<b>Tampilkan Berdasarkan</b>
<form class="form-inline" style="margin : 0px 0px 10px 0px">
  {% if not program_defined %}
  <div class="form-group">
    <select class="form-control filter-matkul" id="filter_curriculum">
      <option value="-1">--Semua Kurikulum--</option> 
      {% for curriculum in curriculums %}
        <option value='{{ curriculum.id }}'>{{ curriculum.name }}</option> 
      {% endfor %}
    </select>
  </div>
  <div class="form-group">
    <select class="form-control filter-matkul" id="filter_program">
      <option value="-1">--Semua Program Studi--</option>
    </select>
  </div>
  {% endif %}
  <div class="form-group">
    <select class="form-control filter-matkul" id="filter_subject_type">
       <option value="-1">--Semua Jenis--</option>
       {% if not program_defined %}
          <option value="0">Umum</option>
       {% endif %}
       <option value="1">Wajib</option>
       <option value="2">Pilihan</option> 
    </select>
  </div>
  <div class="form-group">
    <select class="form-control filter-matkul" id="filter_semester">
      <option value="-1">--Semua Semester--</option> 
      {% for semester in semesters %}
        <option value='{{ semester.semester }}'>{{ semester.semester }}</option> 
      {% endfor %}
    </select>
  </div>
</form>

<b>Cari Mata Kuliah</b>
<form class="form-inline" style="margin : 0px 0px 10px 0px">
  <div class="form-group col-md-12" style="padding:0px 0px 10px 0px">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Cari berdasarkan nama mata kuliah.." id="search_by_subject_name"/>
      <span class="input-group-addon">
        <i class="glyphicon glyphicon-search"></i>
      </span>
    </div>
  </div>
</form>
<script type="text/javascript">
$("#filter_curriculum").selectify({
  child:document.getElementById("filter_program"),
  url:"{{ u('program_options') }}",
  data_processor:function(options){
    if (options.data.length != 0) {
      options.data.unshift({
        id:-1,
        name:"--Semua Program Studi--"
      });
    }
    return options;
  },
  url_processor:function(url, value){
    return url.replace(":curriculum_id", value);
  },
  loading:"--Memuat Program Studi--",
  initial:{
    content:"test",
    value:-1
  },
  empty:{
    content:"--Semua Program Studi--",
    value:-1
  }
});

$(".filter-matkul").change(function(){
  searchMatkul(); 
});
$("#search_by_subject_name").keyup(function(){
  searchMatkul();
});
</script>