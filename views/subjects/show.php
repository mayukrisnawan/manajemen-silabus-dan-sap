{% extends current_user_level() == "guest" ? "layouts/guest_layout.php" : current_user_level() == "admin" ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>{{ subject.name }}</h1>
{% if not (current_user_level() == "guest")  %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah pertemuan baru", "subject_meetings#add", {subject_id:subject.id}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<table class="table table-bordered table-striped table-condensed table-hover">
  <tbody id="meeting_container">
    {% if meetings|length == 0 %}
    <tr>
      <td>
        <center>
          <i>Belum ada pertemuan untuk mata kuliah "{{ subject.name }}"</i>
        </center>
      </td>
    </tr>
    {% endif %}
    {% for meeting in meetings %}
    <tr class='meeting-box' data-meeting-id="{{ meeting.id }}">
      <td>
        <div class='pull-right'>
        {% if not (current_user_level() == "guest")  %}
          {{ a("Edit", "subject_meetings#edit", {subject_id:subject.id, id:meeting.id}) }}
          |
          {{ 
            a("Hapus", "meetings#destroy", {id:meeting.id}, {method:"delete", remote:"true", 
              confirmTitle:subject.name,
              confirmMessage:"Apakah anda yakin ingin menghapus pertemuan berikut?",
              class:"btn_hapus"
            }) 
          }}
        {% endif %}
        </div>
        {% if not (current_user_level() == "guest")  %}
          {{ a(["Pertemuan ke-", loop.index]|join, "subject_meetings#show", { subject_id:subject.id, id:meeting.id }, {class:'meeting-index'}) }}
        {% else %}
          {{ ["Pertemuan ke-", loop.index]|join }}
        {% endif %}
        <span class='meeting-description'>{{ meeting.main_lesson != null ? [", ", meeting.main_lesson]|join : "" }}</span>
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>
<i class='app-small-info'>*Atur urutan pertemuan dengan drag dan drop pertemuan</i>
<span class='pull-right' style='font-size:10px;' id="meeting_orders_status"></span>
<script type="text/javascript">
var loader = null;
var meeting_orders_status = document.getElementById("meeting_orders_status");
function meetingOrdersUpdate_LoadState(){
  loader = loadDotsAnimationInto(meeting_orders_status, "<i>Menyimpan urutan pertemuan</i>");
  $(meeting_orders_status).show();
}
function meetingOrdersUpdate_SuccessState(){
  meeting_orders_status.innerHTML = "<font color='green'><i class='glyphicon glyphicon-ok'></i> Urutan pertemuan berhasil disimpan</font>";
  $(meeting_orders_status).show().fadeOut(2000); 
}
function meetingOrdersUpdate_ErrorState(){
  meeting_orders_status.innerHTML = "<font color='red'><i class='glyphicon glyphicon-remove'></i> Terjadi kesalahan, urutan pertemuan tidak berhasil disimpan</font>";
  $(meeting_orders_status).show().fadeOut(2000);
}
function meetingOrdersUpdate_SilentState(){
  stopDotsAnimationOn(loader, meeting_orders_status);
}


var meeting_orders_xhr = null;
$("#meeting_container").sortable({
  helper:jqueryUI_sortableHelper,
  update:function(event, element){
    var meeting_orders = "";
    $(".meeting-box").each(function(key){
      var id = $(this).attr("data-meeting-id");
      if (!meeting_orders == "") meeting_orders += "&";
      meeting_orders += "meeting_orders[" +  key + "]=" + id; 
      $(this).find(".meeting-index").html("Pertemuan ke-"+(key+1));
    });

    if (meeting_orders_xhr) {
      meeting_orders_xhr.abort();
    }
    
    meeting_orders_xhr = $.ajax({
      "url":"{{ u('set_subject_meeting_orders', {subject_id:subject.id}) }}",
      "type":"patch",
      "data":meeting_orders,
      success:function(){
        meetingOrdersUpdate_SuccessState();
      },
      error:function(){
        meetingOrdersUpdate_ErrorState();        
      },
      beforeSend:function(){
        meetingOrdersUpdate_LoadState();
      },
      complete:function(){
        meetingOrdersUpdate_SilentState();
      }
    });
  }
}).disableSelection();
</script>
{% endblock %}