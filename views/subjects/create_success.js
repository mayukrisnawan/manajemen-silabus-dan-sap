$(".flash").addClass("alert-success")
           .removeClass("alert-danger")
           .html("<strong>Selamat!</strong> mata kuliah berhasil ditambahkan, {{ a('Kembali', back_link) }}")
           .show();
$("form[name='subject_form']").find("*").attr("disabled", "disabled");
$("form[name='subject_form']").find("input[type='submit']")
                               .attr("disabled", "disabled")
                               .unbind("click");