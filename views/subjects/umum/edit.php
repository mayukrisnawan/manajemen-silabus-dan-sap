{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Edit Mata Kuliah Umum</h1>
{% set subject_type = "umum" %}
{% set method = "patch" %}
{% set action = u("subjects#update", {id:subject.id}) %}
{% set type = 0 %}
<div class="col-md-15">
  {% include "subjects/form.php" %}
</div>
{% endblock %}