{% extends has_level("admin") ? "layouts/admin_layout.php" : "layouts/dosen_layout.php" %}

{% block body %}
<h1>Mata Kuliah Umum {{ curriculum.name }}</h1>
{% if has_level('admin') %}
<form class="form-inline" style="margin : 10px auto">
  <div class="form-group">
    {{ a("<span class='glyphicon glyphicon-plus'></span> Tambah mata kuliah umum", "matkul_umum_add", {curriculum_id:curriculum.id}, {class:"btn btn-success form-control"}) }}
  </div>
</form>
{% endif %}
<div class="table-responsive">
  <table class="table table-bordered table-striped table-condensed table-hover">
    <thead>
      <tr>
        <th width="5%">No</th>
        <th>Kode</th>
        <th>Nama Mata Kuliah</th>
        <th>Jumlah SKS</th>
        <th>Dosen Pengampu</th>
        {% if has_level('admin') %}
          <th>Pilihan</th>
        {% endif %}
      </tr>
    </thead>
    <tbody>
      {% if pagination.records|length == 0 %}
      <tr>
        <td colspan="8">
          <center>
            <i>Belum ada mata kuliah umum yang ditambahkan</i>
          </center>
        </td>
      </tr>
      {% endif %}
      {% for matkul_umum in pagination.records %}
      <tr>
        <td>{{ loop.index + pagination.offset }}</td>
        <td>
            {{ matkul_umum.code }}
        </td>
        <td>
            {% if has_level("admin") %}
              {{ a(matkul_umum.name, "subjects#show", {id:matkul_umum.id}) }}
            {% else %}
              {{ matkul_umum.name }}
            {% endif %}
        </td>
        <td>
            {{ matkul_umum.credit_units }}
        </td>
        <td>
          {% if matkul_umum.lecturer %}
            {{ matkul_umum.lecturer.name }}
          {% else %}
            <i>Dosen Pengampu Belum Dipilih</i>
          {% endif %}
        </td>

        {% if has_level('admin') %}
        <td>
          {{ 
            a("Edit", "matkul_umum_edit", {id:matkul_umum.id, curriculum_id:matkul_umum.parent_id}) 
          }}
          |
          {{ 
            a("Hapus", "subjects#destroy", {id:matkul_umum.id}, {method:"delete", remote:"true", 
              confirmTitle:"Mata Kuliah Umum",
              confirmMessage:"Apakah anda yakin ingin menghapus mata kuliah berikut?",
              class:"btn_hapus"
            }) 
          }}
        </td> 
        {% endif %}
      </tr>
    {% endfor %}
    </tbody>
  </table>
</div>
{{ pagination.links|raw }}
{% endblock %}