<style type="text/css">
.table-bordered td {
  border-collapse: collapse;
  border:solid 1px #000;
  padding: 5px;
}  
</style>
{% for meeting in meetings %}
<page>
{% if loop.index == 1 %}
  <h1>{{ subject.name }}</h1>
  <hr/>
{% endif %}
<table class='table-bordered' cellspacing="0">
  <tr>
    <td>1. Mata Kuliah (MK)</td>
    <td colspan="3">{{ subject.name }}</td>
  </tr>
  <tr>
    <td>2. Kode MK</td>
    <td colspan="3">{{ subject.code }}</td>
  </tr>
  <tr>
    <td>3. Waktu Pertemuan</td>
    <td colspan="3">{{ subject.credit_units }} SKS</td>
  </tr>
  <tr>
    <td>4. Pertemuan ke-</td>
    <td colspan="3">{{ loop.index }}</td>
  </tr>
  <tr>
    <td style='vertical-align:top'>5. Indikator Pencapaian</td>
    <td colspan="3" style='width:50%; word-wrap:break-word; vertical-align:top'>
      <ul style="margin:-15px">
      {% for achievement_indicator in meeting.achievement_indicators %}
        <li>{{ achievement_indicator.content }}</li>
      {% endfor %}
      </ul>
    </td>
  </tr>
  <tr>
    <td>6. Materi Pokok</td>
    <td colspan="3">{{ meeting.main_lesson }}</td>
  </tr>
  <tr>
    <td>7. Pengalaman Belajar</td>
    <td colspan="3" style='width:80%; word-wrap:break-word; vertical-align:top'>{{ meeting.learning_experiences }}</td>
  </tr>
  <tr>
    <td colspan="4" style='font-size:16px; background-color:#eee; font-weight:bold; text-align:center;'>Strategi Pembelajaran</td>
  </tr>
  <tr>
    <td>Tahapan</td>
    <td>Kegiatan Dosen</td>
    <td>Kegiatan Mahasiswa</td>
    <td>Media dan Alat Pembelajaran</td>
  </tr>
  <tr>
    <td>(1)</td>
    <td>(2)</td>
    <td>(3)</td>
    <td>(4)</td>
  </tr>
  {% for meeting_stage in meeting.meeting_stages %}
  <tr>
    <td>{{ meeting_stage.stage.label }}</td>
    {% if meeting_stage.general_act != "" %}
      <td colspan="2" style='width:40%; word-wrap:break-word; vertical-align:top'>{{ meeting_stage.general_act }}</td>
    {% else %}
      <td style='width:20%; word-wrap:break-word; vertical-align:top'>{{ meeting_stage.lecturer_act }}</td>
      <td style='width:20%; word-wrap:break-word; vertical-align:top'>{{ meeting_stage.student_act }}</td>
    {% endif %}
    <td style='width:20%; word-wrap:break-word; vertical-align:top'>
      {% for resource in meeting_stage.resources %}
        {{ loop.index }}. {{ resource.name }}<br/>
      {% endfor %}
    </td>
  </tr>
  {% endfor %}
  <tr>
    <td style="vertical-align:top">Referensi</td>
    <td colspan="3" style='width:60%; word-wrap:break-word; vertical-align:top'>
      <ul style="margin:-15px">
      {% for meeting_reference in meeting.meeting_references %}
        <li>{{ meeting_reference.reference.content }}</li>
      {% endfor %}
      </ul>
    </td>
  </tr>
  <tr>
    <td colspan="4" style="text-align:right">Dosen : {{ subject.lecturer ? subject.lecturer.name : "-" }}</td>
  </tr>
</table>
</page>
{% endfor %}
   