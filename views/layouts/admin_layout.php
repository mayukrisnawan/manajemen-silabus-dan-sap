<!DOCTYPE html>
<html>
<head>
  <title>Manajemen Silabus dan SAP</title>
  <?php
    css(array(
      'font-awesome/font-awesome.min',
      'bootstrap',
      'jquery-ui-1.10.3.custom.min',
      'summernote',
      'summernote-bs3',
      'app'
    ));
    js(array(
      'jquery-1.10.2.min',
      'jquery-ui-1.10.3.custom.min',
      'bootstrap.min',
      'jquery.validate.min',
      'bootstrap3-typeahead.min',
      'jquery.form.min',
      'summernote.min',
      'app'
    )); 
  ?>
</head>
<body>
  {% include "shared/_admin_menu.php" %}
  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Tampilkan/Sembunyikan Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php eu('guest#index'); ?>">Manajemen Silabus dan SAP</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <!--<li class="active"><a href="#">Home</a></li>-->
        </ul>
        <ul class="nav navbar-nav navbar-right">
          {% block right_nav %}
            {% include "shared/_login_nav.php" %}
          {% endblock %}
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="100%" style="position:relative;top:70px;">
        <div class="panel-body">
          {% block panel_body_open %}
          <div class="panel">
            <div class="panel-body">
              {% include "shared/_breadcrumb.php" %}
          {% endblock %}
              {% block body %}{% endblock %}
          {% block panel_body_close %}
            </div>
          </div>
          {% endblock %}
        </div>
      </div>
    </div>
  </div>
</body>
</html>