<!DOCTYPE html>
<html>
<head>
  <title>Manajemen Silabus dan SAP</title>
  <?php
    css(array(
      'bootstrap'
    ));
    js(array(
      'jquery-1.10.2.min',
      'bootstrap.min',
      'jquery.validate.min',
      'app'
    )); 
  ?>
</head>
<body>
  <div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Tampilkan/Sembunyikan Menu</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        {{ a("Manajemen Silabus dan SAP", "guest#index", {}, {class:"navbar-brand"}) }}
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="vertical-divider"></li>
          <li>{{ a("Daftar Mata Kuliah", "subjects#index") }}</a></li>
          <li>{{ a("Berita dan Pengumuman", "guest#berita") }}</a></li>
          <li>{{ a("Info Selengkapnya", "guest#info") }}</a></li>
          <li>{{ a("Bantuan", "guest#bantuan") }}</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          {% set guest = true %}
          {% block right_nav %}
            {% include "shared/_login_nav.php" %}
          {% endblock %}
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-40" style="padding: 0px 0px 100px 0px">
        {% block body %}{% endblock %}
      </div>
    </div>
  </div>
</body>
</html>