<?php

class StagesController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Tahapan Pembelajaran"=>u("stages#index")),
    );
  }

  # GET /stages Fungsi->Menampilkan semua data
	public function index() {
		$this->data->stages = Stage::all(array("order"=>"label"));
		$this->render("stages/index");
	}
  
  # GET /stage/add
  public function add() {
    $this->data->push("navigations", array("Tambah Tahapan Pembelajaran Baru"=>"" ));
    $this->data->action = "stages#create";
    $this->data->method = "post";
    $this->render("stages/add");
  }

  # POST /stages (aksi belakang layar)
  public function create() {
    $stage = new Stage();
    foreach (params("stage") as $key => $value) {
      $stage->$key = $value;
    }
    if ($stage->save()) {
      $this->render("stages/create_success");
    } else {
      $this->data->errors = $stage->errors->fetch();
      $this->render("stages/create_errors");
    }
  }

  # GET /stages/:id/edit
  public function edit($id) {
    $this->data->push("navigations", array("Edit Tahapan Pembelajaran"=>""));
    try {
      $this->data->stage = Stage::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "stages#update";
    $this->data->method = "patch";
    $this->render("stages/edit");
  }

  # PATCH /stage/:id
  public function update($id) {
    try {
      $stage = Stage::find($id);
    } catch (Exception $e) {
      $this->render("stages/update_success");
    }
    foreach (params("stage") as $key => $value) {
      $stage->$key = $value;
    }
    if ($stage->save()) {
      $this->render("stages/update_success");
    } else {
      $this->data->errors = $stage->errors->fetch();
      $this->render("stages/update_errors");
    }
  }

  # DELETE /stage/:id
  public function destroy($id) {
    try {
      $stage = Stage::find($id);
    } catch (Exception $e) {
      $this->render("stages/destroy_success");
    }
    if ($stage->delete()) {
      $this->render("stages/destroy_success");
    } else {
      $this->render("stages/destroy_errors");
    }
  }
}