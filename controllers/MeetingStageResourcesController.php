<?php

class MeetingStageResourcesController extends Controller {
  # POST /meetingstageresources/
  public function create() {
    $meeting_stage_resource = new MeetingStageResource(params("meeting_stage_resource"));
    cek_punya_akses_untuk_mata_kuliah($meeting_stage_resource->meeting_stage->meeting->subject_id);
    if ($meeting_stage_resource->save()) {
      $this->render("meetings/stages/resources/create_success");
    } else {
      $this->render("meetings/stages/resources/create_errors");
    }
  }

  # DELETE /meetingstageresources/:id
  public function destroy($id) {
    try {
      $meeting_stage_resource = MeetingStageResource::find($id);  
      cek_punya_akses_untuk_mata_kuliah($meeting_stage_resource->meeting_stage->meeting->subject_id);    
    } catch (Exception $e) {
      $this->render("meetings/stages/resources/destroy_errors");
    }
    if ($meeting_stage_resource->delete()) {
      $this->render("meetings/stages/resources/destroy_success");
    } else {
      $this->render("meetings/stages/resources/destroy_errors");
    }
  }
}