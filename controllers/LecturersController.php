<?php

class LecturersController extends Controller {
  function __construct() {
    parent:: __construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Dosen"=>u("lecturers#index"))
    );
  }

  # GET /lecturers/json
  public function json() {
    $name = params("name");
    if (!$name) exit();
    $lectures = Lecturer::all(
      array(
        "conditions"=>array("name LIKE ?", "%".$name."%"),
        "order"=>"name"
      )
    );
    echo Model::json($lectures);
  }

  public function search() {
    UM::requiredLevels(array("admin", "dosen"));
    $this->data->query = params("nama_dosen");
    $query = "%%";
    if (params("nama_dosen")) $query = "%". params("nama_dosen") . "%";
    $this->data->pagination = Lecturer::paginate(array(
      "order"=>"name",
      "conditions"=>array("name LIKE ?", $query)
    ));
    $this->render("lecturers/_lecturers_table");
  }
  
  public function index() {
    $this->data->pagination = Lecturer::paginate(array("order"=>"name"));
    $this->render("lecturers/index");
  }

  public function add(){
    $this->data->push("navigations", array("Tambah Data Dosen"=>""));
    $this->data->action = "lecturers#create";
    $this->data->method = "post";
    $this->render("lecturers/add");
  }

  public function create(){
    $lecturer = new Lecturer();
    foreach (params("lecturer") as $key => $value) {
      $lecturer->$key = $value;
    }
    if ($lecturer->save()) {
      $this->render("lecturers/create_success");
    } else {
      $this->data->errors = $lecturer->errors->fetch();
      $this->render("lecturers/create_errors");
    }
  }

  public function edit($id) {
    $this->data->push("navigations", array("Edit Data Dosen"=>""));
    try {
      $this->data->lecturer = Lecturer::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "lecturers#update";
    $this->data->method = "patch";
    $this->render("lecturers/edit");
  }
  public function update($id) {
    try {
      $lecturer = Lecturer::find($id);
    } catch (Exception $e) {
      $this->render("lecturers/update_success");
    }
    foreach (params("lecturer") as $key => $value) {
      $lecturer->$key = $value;
    }
    if ($lecturer->save()) {
      $this->render("lecturers/update_success");
    } else {
      $this->data->errors = $lecturer->errors->fetch();
      $this->render("lecturers/update_errors");
    }
  }

  public function destroy($id) {
    try {
      $lecturer = Lecturer::find($id);
    } catch (Exception $e) {
      $this->render("lecturers/destroy_success");
    }
    if ($lecturer->delete()) {
      $this->render("lecturers/destroy_success");
    } else {
      $this->render("lecturers/destroy_errors");
    }
  }
}