<?php

class NewsController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Berita dan Pengumuman"=>u("news#index"))
    );
  }

  # GET /news
  public function index() {
    $this->data->pagination = News::paginate(array("order"=>"last_updated desc"));
    $this->render("news/index");
  }

  # GET /news/add
  public function add() {
    $this->data->push("navigations", array("Tambah Baru"=>"" ));
    $this->data->action = "news#create";
    $this->data->method = "post";
    $this->render("news/add");
  }

  # POST /news (aksi belakang layar)
  public function create() {
    $news = new News();
    foreach (params("news") as $key => $value) {
      $news->$key = $value;
    }
    if ($news->save()) {
      $this->render("news/create_success");
    } else {
      $this->data->errors = $news->errors->fetch();
      $this->render("news/create_errors");
    }
  }

  # GET /news/:id/edit
  public function edit($id) {
    $this->data->push("navigations", array("Edit Data"=>""));
    try {
      $this->data->news = News::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "news#update";
    $this->data->method = "patch";
    $this->render("news/edit");
  }

  # PATCH /news/:id
  public function update($id) {
    try {
      $news = News::find($id);
    } catch (Exception $e) {
      $this->render("news/update_success");
    }
    foreach (params("news") as $key => $value) {
      $news->$key = $value;
    }
    if ($news->save()) {
      $this->render("news/update_success");
    } else {
      $this->data->errors = $news->errors->fetch();
      $this->render("news/update_errors");
    }
  }

  # DELETE /news/:id
  public function destroy($id) {
    try {
      $news = News::find($id);
    } catch (Exception $e) {
      $this->render("news/destroy_success");
    }
    if ($news->delete()) {
      $this->render("news/destroy_success");
    } else {
      $this->render("news/destroy_errors");
    }
  }
}