<?php

class AchievementIndicatorsController extends Controller {
  private function cek_meeting($id) {
    $meeting = Meeting::find($id);
    cek_punya_akses_untuk_mata_kuliah($meeting->subject_id);
  }

  # POST /achievementindicators
  public function create() {
    $achievement_indicator = new AchievementIndicator();
    foreach (params("achievement_indicator") as $key => $value) {
      $achievement_indicator->$key = $value;
    }
    $this->cek_meeting($achievement_indicator->meeting_id);
    if ($achievement_indicator->save()) {
      $this->render("meetings/achievement_indicators/create_success");
    } else {
      $this->data->errors = $achievement_indicator->errors->fetch();
      $this->render("meetings/achievement_indicators/create_errors");
    }
  }

  # PATCH /achievementindicators/:id
  public function update($id) {
    try {
      $achievement_indicator = AchievementIndicator::find($id);
      $this->cek_meeting($achievement_indicator->meeting_id);
    } catch (Exception $e) {
      App::notFound();
    }
    foreach (params("achievement_indicator") as $key => $value) {
      $achievement_indicator->$key = $value;
    }
    if ($achievement_indicator->save()) {
      $this->render("meetings/achievement_indicators/update_success");
    } else {
      $this->render("meetings/achievement_indicators/update_errors");
    }
  }

  # DELETE /achievementindicators/:id
  public function destroy($id) {
    try {
      $achievement_indicator = AchievementIndicator::find($id);
      $this->cek_meeting($achievement_indicator->meeting_id);
    } catch (Exception $e) {
      App::notFound();
    }
    if ($achievement_indicator->delete()) {
      $this->render("meetings/achievement_indicators/destroy_success");
    } else {
      $this->render("meetings/achievement_indicators/destroy_errors");
    }
  }
}
