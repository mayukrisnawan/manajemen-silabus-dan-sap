<?php

class ResourcesController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Alat dan Bahan"=>u("resources#index")),
    );
  }

  # GET /resource Fungsi->Menampilkan semua data
	public function index() {
		$this->data->pagination = Resource::paginate(array("order"=>"name"));
		$this->render("resources/index");
	}
  
  # GET /resource/add
  public function add() {
    $this->data->push("navigations", array("Tambah Daftar Alat dan Bahan Baru"=>"" ));
    $this->data->action = "resources#create";
    $this->data->method = "post";
    $this->render("resources/add");
  }

  # POST /resource (aksi belakang layar)
  public function create() {
    $resource = new Resource();
    foreach (params("resource") as $key => $value) {
      $resource->$key = $value;
    }
    if ($resource->save()) {
      $this->render("resources/create_success");
    } else {
      $this->data->errors = $resource->errors->fetch();
      $this->render("resources/create_errors");
    }
  }

  # GET /resource/:id/edit
  public function edit($id) {
    $this->data->push("navigations", array("Edit Alat dan Bahan"=>""));
    try {
      $this->data->resource = Resource::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "resources#update";
    $this->data->method = "patch";
    $this->render("resources/edit");
  }

  # PATCH /recouce/:id
  public function update($id) {
    try {
      $resource = Resource::find($id);
    } catch (Exception $e) {
      $this->render("resources/update_success");
    }
    foreach (params("resource") as $key => $value) {
      $resource->$key = $value;
    }
    if ($resource->save()) {
      $this->render("resources/update_success");
    } else {
      $this->data->errors = $resource->errors->fetch();
      $this->render("resources/update_errors");
    }
  }

  # DELETE /resource/:id
  public function destroy($id) {
    try {
      $resource = Resource::find($id);
    } catch (Exception $e) {
      $this->render("resources/destroy_success");
    }
    if ($resource->delete()) {
      $this->render("resources/destroy_success");
    } else {
      $this->render("resources/destroy_errors");
    }
  }
}