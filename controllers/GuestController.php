<?php

class GuestController extends Controller {
  function __construct() {
    parent::__construct();
    UM::requiredLevels(array("guest", "admin", "dosen"));
  }

  public function index() {
    $this->data->pagination = News::paginate(array("order"=>"last_updated desc", "limit"=>3));
    $this->render("guest/index");
  }

  public function berita() {
    $this->data->pagination = News::paginate(array("order"=>"last_updated desc"));
    $this->render("guest/berita");
  }

  public function tampilkan_berita($id) {
    $this->data->tb = News::find($id);
    $this->render("guest/tampilkan_berita");
  }

  public function info() {
    $this->data->informasi = Config::find(array('key'=>'halaman_informasi'));
    $this->render("guest/info");
  }

  public function bantuan() {
    $this->data->bantuan = Config::find(array('key'=>'halaman_bantuan'));
    $this->render("guest/bantuan");
  }

  public function login() {
    $this->render("guest/login");
  }

  public function logout() {
    UM::logout();
    header("Location:".u("guest#index"));
  }

  public function auth() {
    $authInfo = params("user");
    $username = $authInfo["username"];
    $password = $authInfo["password"];
    if (UM::login($username, $password)) {
      $currentLevel = UM::currentLevel();
      $currentLevel = UM::levelName($currentLevel);
      if ($currentLevel == "admin") {
        $this->data->url = u("admin#index");
      } else {
        $this->data->url = u("dosen#index");
      }
      $this->render("guest/auth_success");
    } else {
      $this->render("guest/auth_failed");
    }
  }
}