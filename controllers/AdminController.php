<?php

class AdminController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index"))
    );
  }
  public function index() {
    $this->data->menu = "index";
    $this->render("admin/index");
  }
  public function akun() {
    $this->data->push("navigations", array(
      "Informasi Akun"=>""
    ));
    $this->data->user = UM::currentUser();
    $this->render("users/edit");
  }
}