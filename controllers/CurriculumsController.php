<?php

class CurriculumsController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>UM::currentLevel() == "admin" ? u("admin#index") : u("admin#index")),
      array("Daftar Kurikulum"=>u("curriculums#index"))
    );
  }

  # GET /curriculums
  public function index() {
    $this->data->pagination = Curriculum::paginate();
    $this->render("curriculums/index");
  }

  # GET /curriculums/add
  public function add() {
    $this->data->push("navigations", array("Tambah Kurikulum Baru"=>""));
    $this->data->action = u("curriculums#create"); 
    $this->data->method = "post"; 
    $this->render("curriculums/add");
  }

  # POST /curiculums
  public function create() {
    $this->data->curriculum = new Curriculum();
    foreach (params("curriculum") as $key => $value) {
      $this->data->curriculum->$key = $value;
    }
    if ($this->data->curriculum->save()) {
      $this->render("curriculums/create_success");
    } else {
      $this->data->errors = $this->data->curriculum->errors->fetch();
      $this->render("curriculums/create_errors");
    }
  }

  # GET /curriculums/:id/edit
  public function edit($id) {
    try {
      $this->data->curriculum = Curriculum::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array("Edit Program Studi"=>""));
    $this->data->action = u("curriculums#update", array("id"=>$id)); 
    $this->data->method = "patch"; 
    $this->render("curriculums/edit");
  }

  # PATCH curriculums/:id
  public function update($id) {
    try {
      $this->data->curriculum = Curriculum::find($id);
    } catch (Exception $e) {
      $this->render("curriculums/update_success");
    }
    foreach (params("curriculum") as $key => $value) {
      $this->data->curriculum->$key = $value;
    }
    if ($this->data->curriculum->save()) {
      $this->render("curriculums/update_success");
    } else {
      $this->data->errors = $this->data->curriculum->errors->fetch();
      $this->render("curriculums/update_errors");
    }
  }

  # DELETE /curriculums/:id
  public function destroy($id) {
    try {
      $curriculum = Curriculum::find($id);
    } catch (Exception $e) {
      $this->render("curriculums/destroy_success");
    }
    if ($curriculum->delete()) {
      $this->render("curriculums/destroy_success");
    } else {
      $this->render("curriculums/destroy_errors");
    }
  }
}