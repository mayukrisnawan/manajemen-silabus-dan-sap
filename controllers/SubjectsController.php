<?php

class SubjectsController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Kurikulum"=>u("curriculums#index"))
    );
  }

  // Mengecek kurikulum yang valid
  // Lalu menambahkan navigasi ke kurikulum
  private function cek_kurikulum($id) {
    try {
      $this->data->curriculum = Curriculum::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array(
      $this->data->curriculum->name=>u("curriculum_programs#index", array("curriculum_id"=>$id))
    ));
  }

  // Mengecek program studi yang valid
  private function cek_prodi($id) {
    try {
      $this->data->program = Program::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array(
      $this->data->program->curriculum->name=>u("curriculum_programs#index", array("curriculum_id"=>$this->data->program->curriculum_id))
    ));
    $this->data->push("navigations", array(
      $this->data->program->name=>u("curriculum_programs#show", array(
        "curriculum_id"=>$this->data->program->curriculum_id,
        "id"=>$id
      ))
    ));
  }

  # GET /subject/:id/standard/competencies/table
  public function standard_competencies_table($id) {
    UM::requiredLevels(array("admin", "dosen"));
    try {
      $this->data->subject = Subject::find($id);
      cek_punya_akses_untuk_mata_kuliah($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->standard_competencies = $this->data->subject->standard_competencies;
    $this->render("subjects/standard_competencies/_standard_competencies_table");
  }

  # GET /curriculums/:curriculum_id/matkul/umum
  public function matkul_umum($curriculum_id) {
    UM::requiredLevels(array("admin", "dosen"));
    $this->cek_kurikulum($curriculum_id);
    $this->data->push("navigations", array("Mata Kuliah Umum"=>""));
    $this->data->pagination = Subject::paginate(array(
      "conditions"=>array("parent_id = ? AND type = 0", $curriculum_id),
      "order"=>"name"
    ));
    $this->render("subjects/umum/index");
  }

  # GET /curriculums/:curriculum_id/matkul/umum/add
  public function matkul_umum_add($curriculum_id) {
    $this->cek_kurikulum($curriculum_id);
    $this->data->push("navigations", array(
      "Mata Kuliah Umum"=>u("matkul_umum", array("curriculum_id"=>$curriculum_id))
    ));
    $this->data->push("navigations", array(
      "Tambah Mata Kuliah Umum"=>""
    ));
    $this->render("subjects/umum/add");
  }

  # GET /curriculums/:curriculum_id/matkul/umum/:id/edit
  public function matkul_umum_edit($curriculum_id, $id) {
    $this->cek_kurikulum($curriculum_id);
    try {
      $this->data->subject = Subject::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array(
      "Mata Kuliah Umum"=>u("matkul_umum", array("curriculum_id"=>$curriculum_id))
    ));
    $this->data->push("navigations", array(
      $this->data->subject->name=>""
    ));
    $this->render("subjects/umum/edit");
  }

  # GET /programs/:id/matkul/add
  public function matkul_add($program_id) {
    $this->cek_prodi($program_id);
    $this->data->push("navigations", array(
      "Tambah Mata Kuliah"=>""
    ));
    $this->render("subjects/add");
  }

  # GET /programs/:id/matkul/:id/edit
  public function matkul_edit($program_id, $id) {
    $this->cek_prodi($program_id);
    try {
      $this->data->subject = Subject::find($id);      
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->standard_competencies = $this->data->subject->standard_competencies;
    $this->data->push("navigations", array(
      "Edit Mata Kuliah"=>""
    ));
    $this->render("subjects/edit");
  }

  public function search() {
    UM::requiredLevels(array("admin", "dosen", "guest"));
    $this->data->pagination = Subject::searchPaginate();
    $this->render("subjects/_subjects_table");
  }

  # GET /subjects/
  public function index() {
    UM::requiredLevels(array("admin", "dosen", "guest"));
    $this->data->semesters = Subject::all(array(
      "conditions"=>"semester <> 0",
      "select"=>"DISTINCT semester",
      "order"=>"semester"
    ));
    $this->data->curriculums = Curriculum::all(array("order"=>"name"));
    $this->data->programs = Program::all(array("order"=>"name"));
    $this->data->pagination = Subject::paginate(array("order"=>"name"));
    $this->data->replace("navigations",array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Mata Kuliah"=>"")
    ));
    $this->render("subjects/index");
  }

  # GET /subjects/:id
  public function show($id) {
    UM::requiredLevels(array("admin", "dosen"));
    cek_punya_akses_untuk_mata_kuliah($id);
    try {
      $this->data->subject = Subject::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    if ($this->data->subject->type == 0) {
      $this->cek_kurikulum($this->data->subject->parent_id);
    } else {
      $this->cek_prodi($this->data->subject->parent_id);
    }
    $this->data->push("navigations", array(
      $this->data->subject->name => ""
    ));
    $this->data->meetings = Meeting::all(array(
      "conditions"=>array("subject_id"=>$id),
      "order"=>"meetings.order"
    ));
    $this->render("subjects/show");
  }

  # POST /subjects
  public function create() {
    $subject = new Subject();
    foreach (params("subject") as $key => $value) {
      $subject->$key = $value;
    }
    if ($subject->type == 0) {
      $this->cek_kurikulum($subject->parent_id);
      $this->data->back_link = u("matkul_umum", array("curriculum_id"=>$subject->parent_id));
    } else {
      $this->cek_prodi($subject->parent_id);
      $this->data->back_link = u("curriculum_programs#show", array(
        "curriculum_id"=>$this->data->program->curriculum_id,
        "id"=>$this->data->program->id
      ));
    }
    if ($subject->save()) {
      $this->render("subjects/create_success");
    } else {
      $this->data->errors = $subject->errors->fetch();
      $this->render("subjects/create_errors");
    }
  }

  # PATCH /subject/:id
  public function update($id) {
    try {
      $this->data->subject = Subject::find($id);
    } catch (Exception $e) {
      $this->render("subject/update_success");
    }
    foreach (params("subject") as $key => $value) {
      $this->data->subject->$key = $value;
    }
    if ($this->data->subject->type == 0) {
      $this->data->back_link = u("matkul_umum", array("curriculum_id"=>$this->data->subject->parent_id));
      $this->cek_kurikulum($this->data->subject->parent_id);
    } else {
      $this->cek_prodi($this->data->subject->parent_id);
      $this->data->back_link = u("curriculum_programs#show", array(
        "curriculum_id"=>$this->data->program->curriculum_id,
        "id"=>$this->data->program->id
      ));
    }
    if ($this->data->subject->save()) {
      $this->render("subjects/update_success");
    } else {
      $this->data->errors = $this->data->subject->errors->fetch();
      $this->render("subjects/update_errors");
    }
  }

  # DELETE /subject/:id
  public function destroy($id) {
    try {
      $subject = Subject::find($id);
    } catch (Exception $e) {
      $this->render("subjects/destroy_success");
    }
    if ($subject->delete()) {
      $this->render("subjects/destroy_success");
    } else {
      $this->render("subjects/destroy_errors");
    }
  }

  public function silabus($id) {
    UM::requiredLevels(array("admin", "dosen", "guest"));
    try {
      $subject = Subject::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $filename = $subject->name;
    $contents = $this->silabus_html($id);
    //echo $contents;
    sendPDF($contents, "Silabus mata kuliah $filename.pdf");
  }

  private function silabus_html($id) {
    try {
      $this->data->subject = Subject::find($id);
      $references = array();
      foreach ($this->data->subject->meetings as $meeting) {
        foreach ($meeting->references as $ref) {
          $references[] = $ref;
        }
      }
      $this->data->references = $references;
    } catch (Exception $e) {
      return "";
    }
    return $this->getContent("subjects/silabus_html");
  }

  public function sap($id) {
    UM::requiredLevels(array("admin", "dosen", "guest"));
    try {
      $subject = Subject::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $filename = $subject->name;
    $contents = $this->sap_html($id);
    //echo $contents;
    sendPDF($contents, "SAP mata kuliah $filename.pdf");
  }

  private function sap_html($id) {
    try {
      $this->data->subject = Subject::find($id);
      $this->data->meetings = $this->data->subject->meetings;
    } catch (Exception $e) {
      return "";
    }
    return $this->getContent("subjects/sap_html");
  }
}