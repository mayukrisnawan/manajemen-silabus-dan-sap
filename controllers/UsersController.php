<?php

class UsersController extends Controller {
  # PATCH /users/:id/reset/password
  public function reset_password($id) {
    try {
      $user = User::find($id);
    } catch (Exception $e) {
      $this->render("users/reset_errors");
    }
    $user->password = md5($user->username);

    if ($user->save()) {
      $this->render("users/reset_success");
    } else {
      $this->render("users/reset_errors");
    }
  }

  # PATCH /users/:id
  public function update($id) {
    UM::requiredLevels(array("admin", "dosen"));
    try {
      $user = User::find($id);
      $current_user = UM::currentUser();
      if ($user->id != $current_user->id) {
        $this->render("users/update_errors");
      }
    } catch (Exception $e) {
      $this->render("users/update_errors");
    }

    $params = params("user");
    $username = $params["username"];
    $password = $params["password"];

    if (strlen($username) < 5 || strlen($password) < 5) {
      $this->render("users/update_errors");
    }

    $user->username = $username;
    if ($password != "") {
      $user->password = md5($password); 
    }

    if ($user->save()) {
      $this->render("users/update_success");
    } else {
      $this->render("users/update_errors");
    }
  }
}