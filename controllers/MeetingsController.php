<?php

class MeetingsController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Kurikulum"=>u("curriculums#index"))
    );
  }

  // Mengecek mata kuliah yang valid
  // Lalu menambahkan navigasi ke mata kuliah
  private function cek_matkul($id) {
    cek_punya_akses_untuk_mata_kuliah($id);
    try {
      $this->data->subject = Subject::find($id);
    } catch (Exception $e) {
      App::notFound();
    }

    if ($this->data->subject->type == 0) {
      $this->data->push("navigations", array(
        $this->data->subject->curriculum->name=>u("matkul_umum", array(
          "curriculum_id"=>$this->data->subject->parent_id
        ))
      ));
    } else {
      $this->data->push("navigations", array(
        $this->data->subject->program->curriculum->name=>u("curriculum_programs#index", array(
          "curriculum_id"=>$this->data->subject->program->curriculum->id
        ))
      ));
      $this->data->push("navigations", array(
        $this->data->subject->program->name=>u("curriculum_programs#show", array(
          "curriculum_id"=>$this->data->subject->program->curriculum_id,
          "id"=>$this->data->subject->program->id
        ))
      ));
    }
    $this->data->push("navigations", array(
      $this->data->subject->name=>u("subjects#show", array("id"=>$id))
    ));
  }

  # PATCH /subject/:subject_id/meeting/set/orders
  public function set_orders($subject_id) {
    cek_punya_akses_untuk_mata_kuliah($subject_id);
    foreach (params("meeting_orders") as $order => $meeting_id) {
      try {
        $meeting = Meeting::find($meeting_id);
        if ($meeting->subject_id != $subject_id) continue;
        $meeting->order = $order;
        $meeting->save();
      } catch (Exception $e) {}
    }
  }

  # GET /meeting/:id/references/table
  public function references_table($id) {
    try {
      $this->data->meeting = Meeting::find($id);
      cek_punya_akses_untuk_mata_kuliah($this->data->meeting->subject_id);
    } catch (Exception $e) {
      App:notFound();
    }
    $this->data->meeting_references = $this->data->meeting->meeting_references;
    $this->render("meetings/references/_references_table");
  }

  # GET /meeting/:id/achievement/indicators/table
  public function achievement_indicators_table($id) {
    try {
      $this->data->meeting = Meeting::find($id);
      cek_punya_akses_untuk_mata_kuliah($this->data->meeting->subject_id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->achievement_indicators = $this->data->meeting->achievement_indicators;
    $this->render("meetings/achievement_indicators/_achievement_indicators_table");
  }

  # GET /subjects/:subject_id/meetings/:id
  public function show($subject_id, $id) {
    $this->cek_matkul($subject_id);
    try {
      $this->data->meeting = Meeting::find($id);
      $this->data->meeting_order = $this->data->meeting->get_order();
      $this->data->meeting_stages = MeetingStage::all(array(
        "conditions"=>array("meeting_id = ?", $id),
        "order"=>"meeting_stages.order",
      ));
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array(
      "Pertemuan ke-" . $this->data->meeting_order => ""
    ));
    $this->render("meetings/show");
  }  

  # GET /subjects/:subject_id/meetings/add
  public function add($subject_id) {
    $this->cek_matkul($subject_id);
    $this->data->push("navigations", array(
      "Tambah Pertemuan Baru"=>""
    ));
    $this->render("meetings/add");
  }

  # GET /subjects/:subject_id/meetings/:id/edit
  public function edit($subject_id, $id) {
    $this->cek_matkul($subject_id);
    try {
      $this->data->meeting = Meeting::find($id);
    } catch (Exception $e) {
      App:notFound();
    }
    $this->data->meeting_references = $this->data->meeting->meeting_references;
    $this->data->achievement_indicators = $this->data->meeting->achievement_indicators;
    $this->data->push("navigations", array(
      "Edit Pertemuan"=>""
    ));
    $this->render("meetings/edit");
  }

  # POST /subjects/:subject_id/meetings/
  public function create($subject_id) {
    $this->cek_matkul($subject_id);
    $meeting = new Meeting();
    foreach (params("meeting") as $key => $value) {
      $meeting->$key = $value;
    }
    $meeting->subject_id = $subject_id;
    if ($meeting->save()) {
      $this->render("meetings/create_success");
    } else {
      $this->data->errors = $meeting->errors->fetch();
      $this->render("meetings/create_errors");
    }
  }

  # PATCH /meetings/:id/
  public function update($id) {
    try {
      $this->data->meeting = Meeting::find($id);
      $this->data->subject = $this->data->meeting->subject;
      cek_punya_akses_untuk_mata_kuliah($this->data->meeting->subject_id);
    } catch (Exception $e) {
      App::notFound();
    }
    foreach (params("meeting") as $key => $value) {
      $this->data->meeting->$key = $value;
    }
    if ($this->data->meeting->save()) {
      $this->render("meetings/update_success");
    } else {
      $this->data->errors = $this->data->meeting->errors->fetch();
      $this->render("meetings/update_errors");
    }
  }

  # DELETE /meetings/:id
  public function destroy($id) {
    try {
      $meeting = Meeting::find($id);
      cek_punya_akses_untuk_mata_kuliah($meeting->subject_id);
    } catch (Exception $e) {
      $this->render("meetings/destroy_success");
    }
    if ($meeting->delete()) {
      $this->render("meetings/destroy_success");
    } else {
      $this->render("meetings/destroy_errors");
    }
  }
}