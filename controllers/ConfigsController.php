<?php

class ConfigsController extends Controller {
  public function informasi() {
    $this->data->config = Config::find(array('key'=>'halaman_informasi'));
    $this->data->action = "configs#update";
    $this->data->method = "patch";
    $this->render("configs/edit");
  }

  public function bantuan() {
    $this->data->config = Config::find(array('key'=>'halaman_bantuan'));
    $this->data->action = "configs#update";
    $this->data->method = "patch";
    $this->render("configs/edit");
  }

  /*
  # GET /config/:id/edit
  public function edit($id) {
    try {
      $this->data->config = Config::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = "configs#update";
    $this->data->method = "patch";
    $this->render("configs/edit");
  }
  */

  # PATCH /recouce/:id
  public function update($id) {
    try {
      $config = Config::find($id);
    } catch (Exception $e) {
      $this->render("configs/update_success");
    }
    foreach (params("config") as $key => $value) {
      $config->$key = $value;
    }
    if ($config->save()) {
      $this->data->config_id = $id;
      $this->render("configs/update_success");
    } else {
      $this->data->errors = $config->errors->fetch();
      $this->render("configs/update_errors");
    }
  }
}