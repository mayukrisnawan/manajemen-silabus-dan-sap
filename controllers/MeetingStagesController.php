<?php

class MeetingStagesController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Kurikulum"=>u("curriculums#index"))
    );
  }

  private function cek_pertemuan($id) {
    try {
      $this->data->meeting = Meeting::find($id);
      $this->data->meeting_order = $this->data->meeting->get_order();
      $this->data->subject = $this->data->meeting->subject;
      cek_punya_akses_untuk_mata_kuliah($this->data->subject->id);
    } catch (Exception $e) {
      App::notFound();
    }

    $subject_id = $this->data->subject->id;
    if ($this->data->subject->type == 0) {
      $this->data->push("navigations", array(
        $this->data->subject->curriculum->name=>u("matkul_umum", array(
          "curriculum_id"=>$this->data->subject->parent_id
        ))
      ));
    } else {
      $this->data->push("navigations", array(
        $this->data->subject->program->curriculum->name=>u("curriculum_programs#index", array(
          "curriculum_id"=>$this->data->subject->program->curriculum->id
        ))
      ));
      $this->data->push("navigations", array(
        $this->data->subject->program->name=>u("curriculum_programs#show", array(
          "curriculum_id"=>$this->data->subject->program->curriculum_id,
          "id"=>$this->data->subject->program->id
        ))
      ));
    }
    $this->data->push("navigations", array(
      $this->data->subject->name=>u("subjects#show", array("id"=>$subject_id))
    ));

    $this->data->push("navigations", array(
      "Pertemuan ke-" . $this->data->meeting_order => u("subject_meetings#show", array("id"=>$id, "subject_id"=>$subject_id))
    ));
  }

  # PATCH /meeting/:meeting_id/stage/set/orders
  public function set_orders($meeting_id) {
    foreach (params("meeting_stage_orders") as $order => $meeting_stage_id) {
      try {
        $meeting_stage = MeetingStage::find($meeting_stage_id);
        cek_punya_akses_untuk_mata_kuliah($meeting_stage->meeting->subject_id);
        if ($meeting_stage->meeting_id != $meeting_id) continue;
        $meeting_stage->order = $order;
        $meeting_stage->save();
      } catch (Exception $e) {}
    }
  }

  # GET /meeting/stage/:id/resources/table
  public function resources_table($id) {
    try {
      $this->data->meeting_stage = MeetingStage::find($id);
      cek_punya_akses_untuk_mata_kuliah($this->data->meeting_stage->meeting->subject_id);
    } catch (Exception $e) {
      App:notFound();
    }
    $this->data->meeting_stage_resources = $this->data->meeting_stage->meeting_stage_resources;
    $this->render("meetings/stages/resources/_meeting_stage_resources_table");
  }

  # GET /meetings/:meeting_id/meetingstages/add
  public function add($meeting_id) {
    $this->cek_pertemuan($meeting_id);
    $meeting_stage_ids = array();
    foreach ($this->data->meeting->meeting_stages as $meeting_stage) {
      $meeting_stage_ids[] = $meeting_stage->id;
    }
    $this->data->push("navigations", array(
      "Tambah Tahapan Pembelajaran"=>""
    ));

    if (count($meeting_stage_ids) != 0) {
      $this->data->stages = Stage::all(array(
        "order"=>"label",
        "conditions"=>array(
          "stages.id NOT IN (SELECT stage_id FROM meeting_stages WHERE meeting_stages.id IN (?))",
          $meeting_stage_ids
        )
      ));
    } else {
      $this->data->stages = Stage::all(array("order"=>"label"));
    }
    $this->render("meetings/stages/add");
  }

  # GET /meetings/:meeting_id/meetingstages/:id/edit
  public function edit($meeting_id, $id) {
    $this->cek_pertemuan($meeting_id);
    try {
      $this->data->meeting_stage = MeetingStage::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array(
      "Edit Tahapan Pembelajaran"=>""
    ));
    $this->data->stages = Stage::all(array("order"=>"label"));
    $this->data->meeting_stage_resources = $this->data->meeting_stage->meeting_stage_resources;
    $this->data->resources = Resource::all(array("order"=>"name"));
    $this->render("meetings/stages/edit");
  }

  # POST /meetings/:meeting_id/meetingstages/
  public function create($meeting_id) {
    $this->cek_pertemuan($meeting_id);
    $this->data->meeting_stage = new MeetingStage(params("meeting_stage"));
    if ($this->data->meeting_stage->save()) {
      $this->render("meetings/stages/create_success");
    } else {
      $this->render("meetings/stages/create_errors");
    }
  }

  # PATCH /meetingstages/:id
  public function update($id) {
    try {
      $this->data->meeting_stage = MeetingStage::find($id);
      $this->data->meeting = $this->data->meeting_stage->meeting;
      $this->cek_pertemuan($this->data->meeting->id);
    } catch (Exception $e) {
      $this->render("meetings/stages/update_errors");
    }
    foreach (params("meeting_stage") as $key => $value) {
      $this->data->meeting_stage->$key = $value;
    }
    if ($this->data->meeting_stage->save()) {
      $this->render("meetings/stages/update_success");
    } else {
      $this->render("meetings/stages/update_errors");
    }
  }

  # DELETE /meetingstages/:id
  public function destroy($id) {
    try {
      $meeting_stage = MeetingStage::find($id);
      cek_punya_akses_untuk_mata_kuliah($meeting_stage->meeting->subject_id);
    } catch (Exception $e) {
      $this->render("meetings/stages/destroy_errors");
    }
    if ($meeting_stage->delete()) {
      $this->render("meetings/stages/destroy_success");
    } else {
      $this->render("meetings/stages/destroy_errors");
    }
  }
}