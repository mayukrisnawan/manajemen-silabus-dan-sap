<?php

class ReferencesController extends Controller{
  function __construct(){
    parent::__construct();
    $this->data->navigations=array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Referensi"=>u("references#index"))
    );
  }

  # GET /references/json
  public function json() {
    $content = params("content");
    if (!$content) exit();
    $references = Reference::all(
      array(
        "conditions"=>array("content LIKE ?", "%".$content."%"),
        "order"=>"content"
      )
    );
    echo Model::json($references);
  }

  public function search() {
    UM::requiredLevels(array("admin", "dosen"));
    $this->data->query = params("referensi");
    $query = "%%";
    if (params("referensi")) $query = "%". params("referensi") . "%";
    $this->data->pagination = Reference::paginate(array(
      "order"=>"content",
      "conditions"=>array("content LIKE ?", $query)
    ));
    $this->render("references/_references_table");
  }

  public function index(){
    $this->data->pagination=Reference::paginate(array("order"=>"content"));
    $this->render("references/index");
  }

  public function add(){
    $this->data->push("navigations", array("Tambah Referensi"=>""));
    $this->data->action="references#create";
    $this->data->method="post";
    $this->render("references/add");
  }

  public function create(){
    $referensi = new Reference();
    foreach (params ("reference") as $key => $value) {
      $referensi->$key = $value;
    }
    if ($referensi->save()){
      $this->render("references/create_success");
    }else{
      $this->data->errors=$referensi->errors->fetch();
      $this->render("references/create_errors");
    }
  }

  public function edit($id){
    $this->data->push("navigations", array("Edit referensi"=>""));
    try {
      $this->data->reference=Reference::find($id);
    } catch(Exception $e) {
      App::notFound();
    }

    $this->data->action = "references#update";
    $this->data->method="patch";
    $this->render("references/edit");
  }

  public function update($id) {
    try {
      $referensi = Reference::find($id);
    } catch (Exception $e) {
      $this->render("references/update_success");
    }
    foreach (params("reference") as $key => $value) {
      $referensi->$key = $value;
    }
    if ($referensi->save()) {
      $this->render("references/update_success");
    } else {
      $this->data->errors = $referensi->errors->fetch();
      $this->render("references/update_errors");
    }
  }

  # DELETE /resource/:id
  public function destroy($id) {
    try {
      $referensi = Reference::find($id);
    } catch (Exception $e) {
      $this->render("references/destroy_success");
    }
    if ($referensi->delete()) {
      $this->render("references/destroy_success");
    } else {
      $this->render("references/destroy_errors");
    }
  }
}