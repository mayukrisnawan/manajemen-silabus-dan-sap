<?php

class StandardCompetenciesController extends Controller {
  private function cek_matkul($id) {
    cek_punya_akses_untuk_mata_kuliah($id);
  }

  # POST /standardcompetencies
  public function create() {
    $standard_competency = new StandardCompetency();
    foreach (params("standard_competency") as $key => $value) {
      $standard_competency->$key = $value;
    }
    $this->cek_matkul($standard_competency->subject_id);
    if ($standard_competency->save()) {
      $this->render("subjects/standard_competencies/create_success");
    } else {
      $this->data->errors = $standard_competency->errors->fetch();
      $this->render("subjects/standard_competencies/create_errors");
    }
  }

  # PATCH /standardcompetencies/:id
  public function update($id) {
    try {
      $standard_competency = StandardCompetency::find($id);
      $this->cek_matkul($standard_competency->subject_id);
    } catch (Exception $e) {
      App::notFound();
    }
    foreach (params("standard_competency") as $key => $value) {
      $standard_competency->$key = $value;
    }
    if ($standard_competency->save()) {
      $this->render("subjects/standard_competencies/update_success");
    } else {
      $this->render("subjects/standard_competencies/update_errors");
    }
  }

  # DELETE /standardcompetencies/:id
  public function destroy($id) {
    try {
      $standard_competency = StandardCompetency::find($id);
      $this->cek_matkul($standard_competency->subject_id);
    } catch (Exception $e) {
      App::notFound();
    }
    if ($standard_competency->delete()) {
      $this->render("subjects/standard_competencies/destroy_success");
    } else {
      $this->render("subjects/standard_competencies/destroy_errors");
    }
  }
}
