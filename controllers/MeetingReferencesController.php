<?php

class MeetingReferencesController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>u("admin#index")),
      array("Daftar Kurikulum"=>u("curriculums#index"))
    );
  }

  private function cek_meeting($id) {
    $meeting = Meeting::find($id);
    cek_punya_akses_untuk_mata_kuliah($meeting->subject_id);
  }

  # POST /meetingreferences
  public function create() {
    $input = params("meeting_reference");
    if (!$input["meeting_id"]) {
      $this->data->error = "Terjadi kesalahan, referensi tidak berhasil ditambahkan";
      $this->render("meetings/references/create_errors");
    } else if (!$input["new_content"] && !$input["existing_content"]) {
      $this->data->error = "Referensi harus diisi sebelum disimpan";
      $this->render("meetings/references/create_errors");      
    }
    if ($input["new_content"] != "") {
      $reference = new Reference();
      $reference->content = $input["new_content"];
      if (!$reference->save()) {
        $this->data->error = "Terjadi kesalahan, referensi tidak berhasil ditambahkan";
        $this->render("meetings/references/create_errors");
      }
    } else if ($input["existing_content"] != "") {
      try {
        $reference = Reference::find_by_content($input["existing_content"]);
      } catch (Exception $e) {
        $this->data->error = "Terjadi kesalahan, referensi '{$input['existing_content']}' tidak ditemukan";
        $this->render("meetings/references/create_errors");
      }
    }
    $this->cek_meeting($input["meeting_id"]);
    $meeting_reference = new MeetingReference();
    $meeting_reference->meeting_id = $input["meeting_id"];
    $meeting_reference->reference_id = $reference->id;
    if ($meeting_reference->save()) {
      $this->render("meetings/references/create_success");
    } else {
      $this->data->error = "Terjadi kesalahan, referensi tidak berhasil ditambahkan";
      $this->render("meetings/references/create_errors");
    }
  }

  # DELETE /meetingreferences/:id
  public function destroy($id) {
    try {
      $meeting_reference = MeetingReference::find($id);
      $this->cek_meeting($meeting_reference->meeting_id);
    } catch (Exception $e) {
      $this->render("meetings/references/destroy_errors");
    }
    if ($meeting_reference->delete()) {
      $this->render("meetings/references/destroy_success");
    } else {
      $this->render("meetings/references/destroy_errors");
    }
  }
}