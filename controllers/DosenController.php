<?php

class DosenController extends Controller {
  function __construct() {
    parent::__construct();
    UM::requiredLevel("dosen");
    $this->data->navigations = array(
      array("Dashboard"=>u("dosen#index"))
    );
  }
  public function index() {
    $this->data->user = UM::currentUser();
    $this->render("dosen/index");
  }

  public function akun() {
    $this->data->push("navigations", array(
      "Informasi Akun"=>""
    ));
    $this->data->user = UM::currentUser();
    $this->render("users/edit");
  }
  
  public function kurikulum() {
    $this->data->push("navigations", array("Daftar Kurikulum"=>""));
    $this->data->pagination = Curriculum::paginate(array("order"=>"name"));
    $this->render("curriculums/index");
  }

  public function ampu_mata_kuliah() {
    $this->data->push("navigations", array("Daftar Pengampuan Mata Kuliah"=>""));
    $this->data->lecturer = Lecturer::find(UM::currentUser()->id);
    $this->data->subjects = Subject::all(array(
      "order"=>"name",
      "conditions"=>array("lecturer_id = ?", $this->data->lecturer->id)
    ));
    $this->render("dosen/ampu_mata_kuliah");
  }

  public function dosen(){
    $this->data->push("navigations", array("Daftar Dosen"=>""));
    $this->data->pagination=Lecturer::paginate(array("order"=>"name"));
    $this->render("lecturers/index");
  }

  public function tahapan_pembelajaran(){
    $this->data->push("navigations", array("Daftar Tahapan Pembelajaran"=>""));
    $this->data->stages = Stage::all(array("order"=>"label"));
    $this->render("stages/index");
  }
   public function alat_dan_bahan() {
    $this->data->push("navigations", array("Daftar Alat dan Bahan"=>""));
    $this->data->resources = $this->data->pagination = Resource::paginate();
    $this->render("resources/index");
  }

  public function referensi(){
    $this->data->push("navigations", array("Daftar Referensi"=>""));
    $this->data->references= $this->data->pagination=Reference::paginate();
    $this->render("references/index");
  }
}