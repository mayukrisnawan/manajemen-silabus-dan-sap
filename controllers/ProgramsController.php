<?php

class ProgramsController extends Controller {
  function __construct() {
    parent::__construct();
    $this->data->navigations = array(
      array("Dashboard"=>UM::currentLevel() == "admin" ? u("admin#index") : u("dosen#index")),
      array("Daftar Kurikulum"=>UM::currentLevel() == "admin" ? u("curriculums#index") : u("dosen#kurikulum"))
    );
  }

  // Mengecek kurikulum yang valid
  // Lalu menambahkan navigasi ke kurikulum
  private function cek_kurikulum($id) {
    try {
      $this->data->curriculum = Curriculum::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->push("navigations", array(
      $this->data->curriculum->name=>u("curriculum_programs#index", array("curriculum_id"=>$id))
    ));
  }

  # GET /curriculum/:curriculum_id/program/options/
  public function options($curriculum_id) {
    UM::requiredLevels(array("dosen", "admin"));
    try {
      $curriculum = Curriculum::find($curriculum_id);
    } catch (Exception $e) {
      exit();
    }
    $programs = $curriculum->programs;
    echo Model::options($programs, "name");
  }

  # GET /curriculums/:curriculum_id/programs
  public function index($curriculum_id) {
    UM::requiredLevels(array("dosen", "admin"));
    $this->cek_kurikulum($curriculum_id);
    $this->data->pagination = Program::paginate(array(
      "order"=>"name", "conditions"=>array("curriculum_id = ?", $curriculum_id)
    ));
    $this->render("programs/index");
  }

  # GET /curriculums/:curriculum_id/program/:id/search
  public function search_matkul($curriculum_id, $id) {
    UM::requiredLevels(array("admin", "dosen", "guest"));
    try {
      $this->data->program = Program::find($id);
    } catch (Exception $e) {
      App::stop();
    }
    $this->data->pagination = Subject::searchPaginate(array(
      "curriculum_id"=>$curriculum_id,
      "program_id"=>$id
    ));
    $this->render("subjects/_subjects_table");
  }

  # GET /curriculums/:curriculum_id/program/:id
  public function show($curriculum_id, $id) {
    UM::requiredLevels(array("dosen", "admin"));
    $this->cek_kurikulum($curriculum_id);
    try {
      $this->data->program = Program::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->pagination = Subject::paginate(array(
      "conditions"=>array("type <> 0 AND parent_id =?", $id),
      "order"=>"name"
    ));
    $this->data->semesters = Subject::all(array(
      "conditions"=>array("semester <> 0 AND type <> 0 AND parent_id =?", $id),
      "select"=>"DISTINCT semester",
      "order"=>"semester",
    ));
    $this->data->program_defined = true;
    $this->data->push("navigations", array($this->data->program->name=>""));
    $this->render("programs/show");
  }

  # GET /curriculums/:curriculum_id/programs/add
  public function add($curriculum_id) {
    $this->cek_kurikulum($curriculum_id);
    $this->data->push("navigations", array("Tambah Program Studi Baru"=>""));
    $this->data->action = u("curriculum_programs#create", array("curriculum_id"=>$curriculum_id)); 
    $this->data->method = "post"; 
    $this->render("programs/add");
  }

  # GET /curriculums/:curriculum_id/programs/:id/edit
  public function edit($curriculum_id, $id) {
    $this->cek_kurikulum($curriculum_id);
    $this->data->push("navigations", array("Edit Program Studi"=>""));
    try {
      $this->data->program = Program::find($id);
    } catch (Exception $e) {
      App::notFound();
    }
    $this->data->action = u("programs#update", array("id"=> $id));  
    $this->data->method = "patch"; 
    $this->render("programs/edit");
  }

  # POST /curriculums/:curriculum_id/programs
  public function create($curriculum_id) {
    $this->cek_kurikulum($curriculum_id);
    $program = new Program();
    foreach (params("program") as $key => $value) {
      $program->$key = $value;
    }
    $program->curriculum_id = $curriculum_id;
    if ($program->save()) {
      $this->render("programs/create_success");
    } else {
      $this->data->errors = $program->errors->fetch();
      $this->render("programs/create_errors");
    }
  }

  # PATCH /programs/:id
  public function update($id) {
    try {
      $this->data->program = Program::find($id);
    } catch (Exception $e) {
      $this->render("programs/update_success");
    }
    foreach (params("program") as $key => $value) {
      $this->data->program->$key = $value;
    }
    if ($this->data->program->save()) {
      $this->render("programs/update_success");
    } else {
      $this->data->errors = $this->data->program->errors->fetch();
      $this->render("programs/update_errors");
    }
  }

  # DELETE /programs/:id
  public function destroy($id) {
    try {
      $program = Program::find($id);
    } catch (Exception $e) {
      $this->render("programs/destroy_success");
    }
    if ($program->delete()) {
      $this->render("programs/destroy_success");
    } else {
      $this->render("programs/destroy_errors");
    }
  }
}